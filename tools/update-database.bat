@echo off
chcp 1251>nul
rem set start_red=0C
rem%start_red% "ERROR"

cd ..
set PathRoot=%cd%
echo PathRoot=%PathRoot%
set ok=true

set EntityFrameworkCore="%PathRoot%\aspnet-core\src\MyExchange.EntityFrameworkCore"
set MyExchange="%PathRoot%\aspnet-core\src\core\MyExchange"
set BotTacticMonitoring="%PathRoot%\aspnet-core\src\modules\BotTactics\MyExchange.BotTacticMonitoring"

set WebHost="%PathRoot%\aspnet-core\src\MyExchange.Web.Host\MyExchange.Web.Host.csproj"

call:doMigrate

:breake

IF %ok%==false  (
echo ���-�� ����� �� ���, ����� ����������� ���� ������, ������� ������...
dropdb -h localhost -U postgres -W MyExchange
set ok=true
call:doMigrate
 ) ELSE (
echo good!!!!!!!) 


echo.&pause&goto:eof
------------------------------------------------

:doMigrate
cd %EntityFrameworkCore%
echo ����������� �������� %EntityFrameworkCore%
dotnet ef database update --startup-project %WebHost%
IF %errorlevel% == 0  (
echo. command return: %errorlevel%
) ELSE (
set ok=false
goto:breake)  

cd %MyExchange%
echo ����������� �������� %MyExchange%
dotnet ef database update --startup-project %WebHost%
IF %errorlevel% == 0  (
echo. command return: %errorlevel%
 ) ELSE (
set ok=false
goto:breake) 

cd %BotTacticMonitoring%
echo ����������� �������� %BotTacticMonitoring%
dotnet ef database update --startup-project %WebHost%
IF %errorlevel% == 0  (
echo. command return: %errorlevel%
 ) ELSE (
set ok=false
goto:breake) 


goto:eof