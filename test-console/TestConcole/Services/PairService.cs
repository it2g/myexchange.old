﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyExchange.Api.Dto;
using MyExchange.Api.Dto.Pair;
using Newtonsoft.Json;
using RestSharp;
using TestConcole.Dto._DtoBase;

namespace TestConcole.Services
{
    static class PairService
    {
        private static string _baseUrl = "http://localhost:21021";
        private static string _resource = Path.Combine("api", "Pair");
        static RestClient _restClient = new RestClient(_baseUrl);

        public static string LoadPairs(Guid exchangeId)
        {
            var request = new RestRequest(Path.Combine(_resource, "LoadFromExchange"), Method.POST);
            request.AddJsonBody(exchangeId);

            var response = _restClient.Execute(request);

            if (response.IsSuccessful)
            {
                return "Процесс загрузки запущен успешно.";
            }
           
            return $"{response.ErrorMessage} \\n {response.ErrorException}";
        }

        
        public static IList<PairDto> GetPairs()
        {
            var request = new RestRequest(Path.Combine(_resource, "?MaxResultCount=10000000"), Method.GET);

            var responce = _restClient.Execute(request);

            var responseContent = JsonConvert.DeserializeObject<ResponseContent<PagedResultBaseDto<PairDto>>>(responce.Content);
            
            return responseContent.result.Items; 
        }
    }
}
