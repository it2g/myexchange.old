﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyExchange.Api.Dto;
using MyExchange.Api.Entity;
using Newtonsoft.Json;
using RestSharp;
using TestConcole.Dto._DtoBase;

namespace TestConcole.Services
{
    static class TradeService
    {
        private static string _baseUrl = "http://localhost:21021";
        
        static RestClient _restClient = new RestClient(_baseUrl);

        private static DateTime? lastRequestTime = DateTime.Now;

        /// <summary>
        /// Запускаем тактику мониторинга для загрузки пары с биржи
        /// </summary>
        /// <param name="exchangeId"></param>
        /// <param name="pairId"></param>
        /// <returns></returns>
        public static string LoadTradesJob(Guid exchangeId, Guid pairId)
        {
            var resource = Path.Combine("api", "BotTactic");

            var request = new RestRequest(Path.Combine(resource, "RunTradesMonitoring"), Method.POST);

            request.AddJsonBody(new TradeMonitoringInput { ExchangeId = exchangeId, PairId = pairId });

            var response = _restClient.Execute(request);

            if (response.IsSuccessful)
            {
                return "Процесс загрузки сделок запущен успешно.";
            }

            return $"{response.ErrorMessage} \\n {response.ErrorException}";
        }
        public class TradeMonitoringInput
        {
            public Guid ExchangeId { get; set; }

            public Guid PairId { get; set; }
        }

        public static IList<TradeDto> GetLastTrades(Guid exchangeId)
        {
            var resource = Path.Combine("api", "Trade");

            var request = new RestRequest(
                Path.Combine(resource, $"?MaxResultCount=20&ExchangeId={exchangeId}&lastRequestTime={lastRequestTime}"),
                Method.GET);

            var response = _restClient.Execute(request);
            var responseContent = JsonConvert.DeserializeObject<ResponseContent<PagedResultBaseDto<TradeDto>>>(response.Content);

            if (response.IsSuccessful)
            {
                lastRequestTime = DateTime.Now;

                return responseContent.result.Items;
            }

            throw new Exception("Запрос не прошел");

        }

    }
}
