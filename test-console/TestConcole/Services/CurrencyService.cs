﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace TestConcole.Services
{
    static class CurrencyService
    {
        private static string _baseUrl = "http://localhost:21021";
        private static string _resource = Path.Combine("api", "Currency");
        static RestClient _restClient = new RestClient(_baseUrl);

        public static string LoadCurrenies(Guid exchangeId)
        {
            var request = new RestRequest(Path.Combine(_resource, "LoadFromExchange"), Method.POST);
            request.AddJsonBody(exchangeId);

            var response = _restClient.Execute(request);

            if (response.IsSuccessful)
            {
                return "Процесс загрузки запущен успешно.";
            }
           
            return $"{response.ErrorMessage} \\n {response.ErrorException}";

        }
    }
}
