﻿namespace TestConcole.Presentation
{
    partial class frmTradeLoad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnLoadTrades = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.listBoxPairs = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxTrades = new System.Windows.Forms.TextBox();
            this.textBoxLoadedCurrencies = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btnLoadTrades);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.listBoxPairs);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.textBoxTrades);
            this.splitContainer1.Panel2.Controls.Add(this.textBoxLoadedCurrencies);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Size = new System.Drawing.Size(800, 450);
            this.splitContainer1.SplitterDistance = 159;
            this.splitContainer1.TabIndex = 0;
            // 
            // btnLoadTrades
            // 
            this.btnLoadTrades.Location = new System.Drawing.Point(12, 415);
            this.btnLoadTrades.Name = "btnLoadTrades";
            this.btnLoadTrades.Size = new System.Drawing.Size(118, 23);
            this.btnLoadTrades.TabIndex = 2;
            this.btnLoadTrades.Text = "Загрузить";
            this.btnLoadTrades.UseVisualStyleBackColor = true;
            this.btnLoadTrades.Click += new System.EventHandler(this.btnLoadTrades_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Валютные пары";
            // 
            // listBoxPairs
            // 
            this.listBoxPairs.FormattingEnabled = true;
            this.listBoxPairs.Location = new System.Drawing.Point(13, 29);
            this.listBoxPairs.Name = "listBoxPairs";
            this.listBoxPairs.Size = new System.Drawing.Size(121, 381);
            this.listBoxPairs.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Загруженные сделки";
            // 
            // textBoxTrades
            // 
            this.textBoxTrades.Location = new System.Drawing.Point(18, 134);
            this.textBoxTrades.Multiline = true;
            this.textBoxTrades.Name = "textBoxTrades";
            this.textBoxTrades.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxTrades.Size = new System.Drawing.Size(603, 276);
            this.textBoxTrades.TabIndex = 5;
            // 
            // textBoxLoadedCurrencies
            // 
            this.textBoxLoadedCurrencies.Location = new System.Drawing.Point(18, 31);
            this.textBoxLoadedCurrencies.Multiline = true;
            this.textBoxLoadedCurrencies.Name = "textBoxLoadedCurrencies";
            this.textBoxLoadedCurrencies.Size = new System.Drawing.Size(603, 69);
            this.textBoxLoadedCurrencies.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(263, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Валютные пары  по которым загружаются сделки";
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
            // 
            // frmTradeLoad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "frmTradeLoad";
            this.Text = "Загрузка сделок по валютным парам";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmTradeLoad_FormClosing);
            this.Load += new System.EventHandler(this.frmTradeLoad_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnLoadTrades;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBoxPairs;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxTrades;
        private System.Windows.Forms.TextBox textBoxLoadedCurrencies;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
    }
}