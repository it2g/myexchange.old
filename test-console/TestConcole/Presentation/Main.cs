﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestConcole.Constants;
using TestConcole.Presentation;
using TestConcole.Services;

namespace TestConcole
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Загрузка валют с указанной биржи в БД
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrencyLoadMenuItem_Click(object sender, EventArgs e)
        {
            var resultMessage = CurrencyService.LoadCurrenies(Exchange.Id);

            MessageBox.Show(resultMessage, "Результат запроса", MessageBoxButtons.OK );
        }

        /// <summary>
        /// Загрузка валют из сервиса для просмотра и редактирования в форме
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrencySeekMenuItem_Click(object sender, EventArgs e)
        {
            //Откроем форму для их просмотра и редактирования
            var form = new frmCurrencyList();
            form.Show();
        }

        private void PairLoadMenuItem_Click(object sender, EventArgs e)
        {
            var resultMessage = PairService.LoadPairs(Exchange.Id);

            MessageBox.Show(resultMessage, "Результат запроса", MessageBoxButtons.OK);
        }

        private void всеСделкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new frmTradeLoad();
            form.Show();
        }
    }
}
