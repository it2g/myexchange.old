﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyExchange.Api.Dto;
using TestConcole.Constants;
using TestConcole.Services;

namespace TestConcole.Presentation
{
    public partial class frmTradeLoad : Form
    {
        private IList<PairDto> pairs;
        public frmTradeLoad()
        {
            InitializeComponent();
            backgroundWorker.WorkerReportsProgress = true;
            backgroundWorker.WorkerSupportsCancellation = true;

        }

        private void frmTradeLoad_Load(object sender, EventArgs e)
        {
            pairs = PairService.GetPairs();
            listBoxPairs.DataSource = pairs;
            listBoxPairs.DisplayMember = "Name";
            listBoxPairs.ValueMember = "Id";

            if (backgroundWorker.IsBusy != true)
            {
                // Start the asynchronous operation.
                backgroundWorker.RunWorkerAsync();
            }
        }

        /// <summary>
        /// Загрузка сделок
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLoadTrades_Click(object sender, EventArgs e)
        {
            var selectedItem = (PairDto)listBoxPairs.SelectedItem;
            //Добавить пару в список пар загрузаемых
            textBoxLoadedCurrencies.Text += (selectedItem).Name + ", ";

            //Подать команду на загрузку сделок
            TradeService.LoadTradesJob(Exchange.Id, selectedItem.Id);
          
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            while (!worker.CancellationPending )
            {
                var trades = TradeService.GetLastTrades(Exchange.Id);

                foreach (var trade in trades)
                {
                   e.Result = $"{pairs.First(x=> x.Id == trade.PairId).Name} - {trade.Date.ToShortDateString()} - {trade.Amount}\n\r";
                }
            }
        }

        private void frmTradeLoad_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.backgroundWorker.CancelAsync();
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
        }
    }
}
