﻿namespace TestConcole
{
    partial class Main
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.посмотрыДанныхToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CurrencyMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.валютныеПарыToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.всеСделкиToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.всеОрдераToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.своиСделкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.своиОрдераToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.загрузкиДанныхToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CurrencyLoadMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PairLoadMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.всеСделкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.всеОрдераToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.всоиСделкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.своиОрдераToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.посмотрыДанныхToolStripMenuItem,
            this.загрузкиДанныхToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // посмотрыДанныхToolStripMenuItem
            // 
            this.посмотрыДанныхToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CurrencyMenuItem,
            this.валютныеПарыToolStripMenuItem1,
            this.всеСделкиToolStripMenuItem1,
            this.всеОрдераToolStripMenuItem,
            this.своиСделкиToolStripMenuItem,
            this.своиОрдераToolStripMenuItem});
            this.посмотрыДанныхToolStripMenuItem.Name = "посмотрыДанныхToolStripMenuItem";
            this.посмотрыДанныхToolStripMenuItem.Size = new System.Drawing.Size(121, 20);
            this.посмотрыДанныхToolStripMenuItem.Text = "Посмотры данных";
            // 
            // CurrencyMenuItem
            // 
            this.CurrencyMenuItem.Name = "CurrencyMenuItem";
            this.CurrencyMenuItem.Size = new System.Drawing.Size(163, 22);
            this.CurrencyMenuItem.Text = "Валюты";
            this.CurrencyMenuItem.Click += new System.EventHandler(this.CurrencySeekMenuItem_Click);
            // 
            // валютныеПарыToolStripMenuItem1
            // 
            this.валютныеПарыToolStripMenuItem1.Name = "валютныеПарыToolStripMenuItem1";
            this.валютныеПарыToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.валютныеПарыToolStripMenuItem1.Text = "Валютные пары";
            // 
            // всеСделкиToolStripMenuItem1
            // 
            this.всеСделкиToolStripMenuItem1.Name = "всеСделкиToolStripMenuItem1";
            this.всеСделкиToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.всеСделкиToolStripMenuItem1.Text = "Все сделки";
            // 
            // всеОрдераToolStripMenuItem
            // 
            this.всеОрдераToolStripMenuItem.Name = "всеОрдераToolStripMenuItem";
            this.всеОрдераToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.всеОрдераToolStripMenuItem.Text = "Все ордера";
            // 
            // своиСделкиToolStripMenuItem
            // 
            this.своиСделкиToolStripMenuItem.Name = "своиСделкиToolStripMenuItem";
            this.своиСделкиToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.своиСделкиToolStripMenuItem.Text = "Свои сделки";
            // 
            // своиОрдераToolStripMenuItem
            // 
            this.своиОрдераToolStripMenuItem.Name = "своиОрдераToolStripMenuItem";
            this.своиОрдераToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.своиОрдераToolStripMenuItem.Text = "Свои ордера";
            // 
            // загрузкиДанныхToolStripMenuItem
            // 
            this.загрузкиДанныхToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CurrencyLoadMenuItem,
            this.PairLoadMenuItem,
            this.всеСделкиToolStripMenuItem,
            this.всеОрдераToolStripMenuItem1,
            this.всоиСделкиToolStripMenuItem,
            this.своиОрдераToolStripMenuItem1});
            this.загрузкиДанныхToolStripMenuItem.Name = "загрузкиДанныхToolStripMenuItem";
            this.загрузкиДанныхToolStripMenuItem.Size = new System.Drawing.Size(111, 20);
            this.загрузкиДанныхToolStripMenuItem.Text = "Загрузки данных";
            // 
            // CurrencyLoadMenuItem
            // 
            this.CurrencyLoadMenuItem.Name = "CurrencyLoadMenuItem";
            this.CurrencyLoadMenuItem.Size = new System.Drawing.Size(163, 22);
            this.CurrencyLoadMenuItem.Text = "Валюты";
            this.CurrencyLoadMenuItem.Click += new System.EventHandler(this.CurrencyLoadMenuItem_Click);
            // 
            // PairLoadMenuItem
            // 
            this.PairLoadMenuItem.Name = "PairLoadMenuItem";
            this.PairLoadMenuItem.Size = new System.Drawing.Size(163, 22);
            this.PairLoadMenuItem.Text = "Валютные пары";
            this.PairLoadMenuItem.Click += new System.EventHandler(this.PairLoadMenuItem_Click);
            // 
            // всеСделкиToolStripMenuItem
            // 
            this.всеСделкиToolStripMenuItem.Name = "всеСделкиToolStripMenuItem";
            this.всеСделкиToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.всеСделкиToolStripMenuItem.Text = "Все сделки";
            this.всеСделкиToolStripMenuItem.Click += new System.EventHandler(this.всеСделкиToolStripMenuItem_Click);
            // 
            // всеОрдераToolStripMenuItem1
            // 
            this.всеОрдераToolStripMenuItem1.Name = "всеОрдераToolStripMenuItem1";
            this.всеОрдераToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.всеОрдераToolStripMenuItem1.Text = "Все ордера";
            // 
            // всоиСделкиToolStripMenuItem
            // 
            this.всоиСделкиToolStripMenuItem.Name = "всоиСделкиToolStripMenuItem";
            this.всоиСделкиToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.всоиСделкиToolStripMenuItem.Text = "Свои сделки";
            // 
            // своиОрдераToolStripMenuItem1
            // 
            this.своиОрдераToolStripMenuItem1.Name = "своиОрдераToolStripMenuItem1";
            this.своиОрдераToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.своиОрдераToolStripMenuItem1.Text = "Свои ордера";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.Text = "Тестовая консоль";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem загрузкиДанныхToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CurrencyLoadMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PairLoadMenuItem;
        private System.Windows.Forms.ToolStripMenuItem всеСделкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem посмотрыДанныхToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CurrencyMenuItem;
        private System.Windows.Forms.ToolStripMenuItem валютныеПарыToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem всеСделкиToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem всеОрдераToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem своиСделкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem своиОрдераToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem всеОрдераToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem всоиСделкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem своиОрдераToolStripMenuItem1;
    }
}

