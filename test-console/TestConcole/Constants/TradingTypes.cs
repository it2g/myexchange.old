﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConcole
{
    public enum TradeTypes
    {
        /// <summary>
        /// Продажа
        /// </summary>
        Sell = 0,

        /// <summary>
        /// Покупка
        /// </summary>
        Bay = 1,

    }
}
