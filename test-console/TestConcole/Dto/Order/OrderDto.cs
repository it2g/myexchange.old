﻿using MyExchange.Api.Constants;
using MyExchange.Api.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyExchange.Api.Dto
{
    public class OrderDto : DtoBase
    {
        public Guid PairId { get; set; }

        public Guid ExchangeId { get; set; }

        public DateTime Date { get; set; }

        /// <summary>
        /// Цена покупки/продажи
        /// </summary>
        public decimal Price { get; set; }


        public decimal Quantity { get; set; }

        /// <summary>
        /// Тип сделки
        /// </summary>
        public TradeTypes TradeTypes { get; set; }

        /// <summary>
        /// Состояние выставленного ордера
        /// </summary>
        public OrderStatuses OrderStatus { get; set; }
    }
}
