﻿using System;
using System.Collections.Generic;
using System.Text;
using MyExchange.Api.Entity;

namespace MyExchange.Api.Dto
{
   public class TradeExpertLiquidityDto : DtoBase
    {
       public Guid TradeExpertId { get; set; }

       public double Liquidity { get; set; }

       public DateTime Date { get; set; }
    }
}
