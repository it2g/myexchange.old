﻿using System;
using System.Collections.Generic;
using System.Text;


namespace MyExchange.Api.Dto.Pair
{
   public class CreatePairDto 
    {
        public Guid Currency1Id { get; set; }

        public Guid Currency2Id { get; set; }

    }
}
