﻿using MyExchange.Api.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyExchange.Api.Dto
{
    [Serializable]
    public class PairDto
    {
        public Guid Id { get; set; }

        public CurrencyDto Currency1 { get; set; }
        public Guid Currency1Id { get; set; }

        public CurrencyDto Currency2 { get; set; }
        public Guid Currency2Id { get; set; }

        public string Name { get; set; }
       
    }
}
