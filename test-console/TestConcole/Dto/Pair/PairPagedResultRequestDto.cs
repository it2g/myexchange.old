﻿using System;
using System.Collections.Generic;
using System.Text;
using TestConcole.Dto._DtoBase;


namespace MyExchange.Api.Dto.Pair
{
    [Serializable]
    public class PairPagedResultRequestDto : PagedResultBaseDto<PairDto>
    {
    }
}
