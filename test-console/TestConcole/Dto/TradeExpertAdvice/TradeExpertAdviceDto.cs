﻿using System;
using System.Collections.Generic;
using System.Text;
using MyExchange.Api.Entity;
using MyExchange.Api.TradeExpert;

namespace MyExchange.Api.Dto
{
   public class TradeExpertAdviceDto : DtoBase
    {
       public Advices Advice { get; set; }

       /// <summary>
       /// Вероятность сделанной оценки по мнению эксперта
       /// </summary>
       public double Possibility { get; set; }
    }
}
