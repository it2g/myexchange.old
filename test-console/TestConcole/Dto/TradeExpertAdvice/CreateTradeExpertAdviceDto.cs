﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Runtime.Validation;
using MyExchange.Api.TradeExpert;

namespace MyExchange.Api.Dto.TradeExpertAdvice
{
   public class CreateTradeExpertAdviceDto : IShouldNormalize
    {
       public Advices Advice { get; set; }

       /// <summary>
       /// Вероятность сделанной оценки по мнению эксперта
       /// </summary>
       public double Possibility { get; set; }
        public void Normalize()
        {
            
        }
    }
}
