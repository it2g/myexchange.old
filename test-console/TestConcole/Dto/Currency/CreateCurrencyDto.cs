﻿using MyExchange.Api.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyExchange.Api.Dto
{
    public class CreateCurrencyDto 
    {
        public string BrifName { get; set; }

        public string FullName { get; set; }
    }
}
