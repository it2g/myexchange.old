﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConcole.Dto._DtoBase
{
    public interface IEntityDto<TPrimaryKey>
    {
        //
        // Сводка:
        //     Id of the entity.
        TPrimaryKey Id { get; set; }
    }

    public interface IEntityDto : IEntityDto<int>
    {

    }
}
