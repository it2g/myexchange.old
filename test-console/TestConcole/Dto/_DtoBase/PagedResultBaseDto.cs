﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TestConcole.Dto._DtoBase
{
    public class PagedResultBaseDto<T>
    {
        public PagedResultBaseDto()
        {
            Items = new List<T>();
        }

        public  int TotalCount { get; set; }
        public IList<T> Items { get; set; }
        public string TargetUrl { get; set; }
        public bool Success { get; set; }
        public string Error { get; set; }
        public bool UnAuthorizedRequest { get; set; }
        public bool __abp { get; set; }
    }
}
