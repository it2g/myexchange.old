﻿using System;
using System.Collections.Generic;
using System.Text;
using TestConcole.Dto._DtoBase;


namespace MyExchange.Api.Entity
{
    /// <summary>
    /// Базовый класс для всех объектов передачи данных
    /// </summary>
    [Serializable]
    public class DtoBase : EntityDto<Guid>
    {
        
    }
}
