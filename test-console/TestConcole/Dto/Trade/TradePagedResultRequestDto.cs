﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyExchange.Api.Dto.Trade
{
   public class TradePagedResultRequestDto 
    {
        /// <summary>
        /// Время, после которого нужно выгрузить все сделки до текущего момента
        /// </summary>
        public DateTime? LastRequestTime { get; set; }

        public virtual string Sorting { get; set; }
    }
}
