﻿using System;
using Abp.Application.Services.Dto;
using MyExchange.Api.Bot;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Constants;
using MyExchange.Api.MyExchange;

namespace MyExchange.Api.Entity
{
    /// <summary>
    /// Реализует стратегию торговли по отношении в какой либо бирже.
    /// </summary>
    public class BotDto : DtoBase
    {
        /// <summary>
        /// Unique identifier of a background job
        /// </summary>
        public string BackgroundJobId { get; set; }

        /// <summary>
        /// Тактика, котрая будет использована для торговли на бирже.
        /// Через бота можно управлять этой тактикой
        /// </summary>
        public IBotTactic BotTactic { get; set; }

        /// <summary>
        /// Контекстное окружение для тактики бота
        /// </summary>
        public TacticsContextBase TacticsContext { get; set; }

        /// <summary>
        /// Биржа, в отношении которой будет производится торговля
        /// </summary>
        public Entity.Exchange Exchange { get; set; }

        /// <summary>
        /// Стратегия, к которой прикреплен бот
        /// </summary>
        public IMyExchangeStrategy MyExchangeStrategy { get; set; }

        /// <summary>
        /// Используется менеджером ботов для мониторинга состояния бота
        /// </summary>
        public  BotStatuses Status { get; set; }

        /// <summary>
        /// Дата и время запуска бота
        /// Если null но запускать сразу же как появиться возможность
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// Дата и время остановки бота
        /// Если null то будет работать всегда, пока не остановится принудительно 
        /// оператором или использующей его стратегией торговли
        /// /summary>
        public DateTime? StopTime { get; set; }


    }
}
