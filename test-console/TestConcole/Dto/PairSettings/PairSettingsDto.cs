﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using MyExchange.Api.Entity;

namespace MyExchange.Api.Dto
{
    /// <summary>
    /// Настройки валютной пары на бирже. Эти настройки нужно 
    /// знать при выставлении ордера. На разных 
    /// биржех установлены разные условия
    /// </summary>
    public class PairSettingsDto : DtoBase
    {
        public Guid PairId { get; set; }

        /// <summary>
        /// минимальное кол-во по ордеру
        /// </summary>
        public decimal MinQuantity { get; set; }

        /// <summary>
        /// максимальное кол-во по ордеру
        /// </summary>
        public decimal MaxQuantity { get; set; }

        /// <summary>
        /// минимальная цена по ордеру
        /// </summary>
        public decimal MinPrice { get; set; }

        /// <summary>
        /// максимальная цена по ордеру
        /// </summary>
        public decimal MaxPrice { get; set; }

        /// <summary>
        /// минимальная сумма по ордеру
        /// </summary>
        public decimal MinAmount { get; set; }

        /// <summary>
        /// максимальная сумма по ордеру
        /// </summary>
        public decimal MaxAmount { get; set; }
    }
}
