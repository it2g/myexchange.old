﻿using System;
using MyExchange.Api.Entity;

namespace MyExchange.Api.Services
{
    public class CurrencyUserBalanceDto : DtoBase
    {
        public Guid CurrencyId { get; set; }

        public Guid WalletId { get; set; }

        /// <summary>
        /// Баланс валюты на кошельке
        /// </summary>
        public decimal Value { get; set; }
    }
}