﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Runtime.Validation;

namespace MyExchange.Api.Dto.PairInfoOnExchange
{
   public class CreatePairInfoOnExchangeDto : IShouldNormalize
    {
        public Guid PairId { get; set; }

       public Guid ExchangeId { get; set; }
        /// <summary>
        /// Vаксимальная цена сделки за 24 часа
        /// </summary>
        public decimal High { get; set; }
       /// <summary>
       /// Минимальная цена сделки за 24 часа
       /// </summary>
       public decimal Low { get; set; }

       /// <summary>
       /// Средняя цена сделки за 24 часа
       /// </summary>
       public decimal Avarage { get; set; }

       /// <summary>
       /// Объем всех сделок за 24 часа
       /// </summary>
       public ulong Value { get; set; }

       /// <summary>
       /// Сумма всех сделок за 24 часа
       /// </summary>
       public decimal ValueCurr { get; set; }

       /// <summary>
       /// Цена последней сделки
       /// </summary>
       public decimal LastTrade { get; set; }

       /// <summary>
       /// Текущая максимальная цена покупки
       /// </summary>
       public decimal BuyPrice { get; set; }

       /// <summary>
       /// Текущая минимальная цена продажи
       /// </summary>
       public decimal SellPrice { get; set; }

       /// <summary>
       /// Дата и время обновления данных
       /// </summary>
       public DateTime Updated { get; set; }

       public void Normalize()
       {
           
       }
   }
    
}

