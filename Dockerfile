#��������� ������ �������� �� ���������������� ������ �� Microsoft
FROM mcr.microsoft.com/dotnet/core/sdk

ARG COMMIT_ID
ENV COMMIT_ID=${COMMIT_ID}

#���������� ������� � ������ ������ ������ (������ ����� � ����������)
RUN mkdir /app

#�� �������� ��������� �������, ����� ������, ���� ��������
#RUN apt-get update && apt-get install -y nano && apt-get install --assume-yes apt-utils

#������� WORKDIR ��������� ����������, �� ������� ����� ����������� ������� CMD
WORKDIR /app

#�������� ����� � ��������� (���� ������������� ����� dockerfile!!!)
COPY ./aspnet-core/src/MyExchange.Web.Host/bin/Test/netcoreapp2.2/publish /app

#���������� ���� �� ���� ����������
EXPOSE 21021

#������������� ���������� ���������� �� ���������, ������� ������������ ������ ��� � ������ ���������� ���������� � ������� ������
ENTRYPOINT ["dotnet", "MyExchange.Web.Host.dll"]