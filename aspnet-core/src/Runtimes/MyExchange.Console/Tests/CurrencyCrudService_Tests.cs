﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Abp.Application.Services;
using Abp.Dependency;
using MyExchange.Api.Dto;
using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Services;
using MyExchange.Api.Services.Crud;

namespace MyExchange.Console.Tests
{
    class CurrencyCrudService_Tests : IApplicationService
    {
        private static ICurrencyCrudAppService service;

        public static void CreateCurrency(IIocManager ioc)
        {

            //foreach (var handler in ioc.IocContainer.Kernel.GetAssignableHandlers(typeof(object)))
            //{
            //    Debug.WriteLine("{0} {1}",
            //        String.Join(",", handler.ComponentModel.Services.Select(s=>s.FullName)),
            //        handler.ComponentModel.Implementation);
            //}

            service = ioc.IocContainer.Resolve<ICurrencyCrudAppService>();
            var currency = new CurrencyDto()
            {
                Id = new Guid(),
                BrifName = "BTC",
                FullName = "Bitcoin",
            };

            currency = service.Create(currency).Result;

            Debug.WriteLine(currency.ToString() + "/n/r");

        }
    }
}
