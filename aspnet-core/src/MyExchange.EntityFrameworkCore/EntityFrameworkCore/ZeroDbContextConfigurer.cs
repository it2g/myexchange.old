using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyExchange.EntityFrameworkCore
{
    public static class ZeroDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<ZeroDbContext> builder, string connectionString)
        {
            builder.UseNpgsql(connectionString,
                //https://stackoverflow.com/questions/38507861/entity-framework-core-change-schema-of-efmigrationshistory-table
                x => x.MigrationsHistoryTable(HistoryRepository.DefaultTableName, "abp_zero"))
                .UseLazyLoadingProxies(); 
        }

        public static void Configure(DbContextOptionsBuilder<ZeroDbContext> builder, DbConnection connection)
        {
            builder.UseNpgsql(connection,
                //https://stackoverflow.com/questions/38507861/entity-framework-core-change-schema-of-efmigrationshistory-table
                x => x.MigrationsHistoryTable(HistoryRepository.DefaultTableName, "abp_zero"))
                   .UseLazyLoadingProxies();
        }
    }
}
