﻿namespace MyExchange
{
    public class MyExchangeConsts
    {
        public const string LocalizationSourceName = "MyExchange";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
