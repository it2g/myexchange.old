﻿using System;
using MyExchange.Api.MyExchange;

namespace MyExchange.Api.Entity
{
    /// <summary>
    /// Контекст, которым оперирует тактика, выполняемая ботом операции на бирже
    /// Этим контекстом оперируют большая часть тактик, но он может быть расширен наследованием
    ///  </summary>
    public abstract class TradingBotTacticContextBase : BotTacticContextBase
    {
        /// <summary>
        /// Объем денежных средств выделеных тактике для ведения торговли.
        /// Он резервирует с одной стороны денежные средства на кошельке, 
        /// и ограничивает бота, с другой стороны, в использовании общедоступных денежных средств
        /// </summary>
        public CurrencyValue CurrencyValue { get; set; }

        /// <summary>
        /// С этой пары нужно начинать торги,
        /// </summary>
        public Guid PairId { get; set; }

        public Guid ExchangeId { get; set; }

        public Guid TradingStratagyId { get; set; }
    }

   
}
