﻿using System;
using MyExchange.Api.Constants;
using MyExchange.Api.MyExchange;

namespace MyExchange.Api.Entity
{
    /// <summary>
    /// Контекст, которым оперирует бот, выполняемая какую либо работу в фоновом режиме
    /// Этим контекстом оперируют большая часть тактик, но он может быть расширен наследованием
    ///  </summary>
    public abstract class BotTacticContextBase : EntityBase
    {
        /// <summary>
        /// Тактика, котрая будет использована для торговли на бирже.
        /// Через бота можно управлять этой тактикой
        /// TODO Т.к. контекст в конечном итоге являясь наследником этого
        /// базового типапредставляет собой уникальный тип, то скорее всего
        /// этот id может не понадобиться
        /// </summary>
        public Guid BotTacticId { get; set; }

        /// <summary>
        /// Используется менеджером ботов для мониторинга состояния бота
        /// </summary>
        public BotStatuses Status { get; set; }

        /// <summary>
        /// Дата и время запуска бота
        /// Если null но запускать сразу же как появиться возможность
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// Дата и время остановки бота
        /// Если null то будет работать всегда, пока не остановится принудительно 
        /// оператором или использующей его стратегией торговли
        /// /summary>
        public DateTime? StopTime { get; set; }
    }

   
}
