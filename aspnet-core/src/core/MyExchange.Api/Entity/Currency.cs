﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyExchange.Api.Entity
{
    /// <summary>
    /// Валюта  безоносительно к каой либо бирже. 
    /// На ее онове формируеся справочник валют
    /// </summary>
    public class Currency : EntityBase
    {
        public string BrifName { get; set; }

        public string FullName { get; set; }
    }
}
