﻿using System;

namespace MyExchange.Api.Entity
{
    /// <summary>
    /// Информация по валютной паре на конкретной бирже
    /// </summary>
    public class PairInfoOnExchange : EntityBase
    {
        public Guid PairId { get; set; }
        public virtual Pair Pair { get; set; }

        /// <summary>
        /// Биржа, с которой получена информация о валютной паре
        /// </summary>
        public Guid ExchangeId { get; set; }

        /// <summary>
        /// Максимальная цена сделки за 24 часа
        /// </summary>
        public decimal High { get; set; }
        /// <summary>
        /// Минимальная цена сделки за 24 часа
        /// </summary>
        public decimal Low { get; set; }

        /// <summary>
        /// Средняя цена сделки за 24 часа
        /// </summary>
        public decimal Avarage { get; set; }
        
        /// <summary>
        /// Объем всех сделок за 24 часа
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        /// Сумма всех сделок за 24 часа
        /// </summary>
        public decimal ValueCurr { get; set; }

        /// <summary>
        /// Цена последней сделки
        /// </summary>
        public decimal LastTrade { get; set; }

        /// <summary>
        /// Текущая максимальная цена покупки
        /// </summary>
        public decimal BuyPrice { get; set; }

        /// <summary>
        /// Текущая минимальная цена продажи
        /// </summary>
        public decimal SellPrice { get; set; }

        /// <summary>
        /// Дата и время обновления данных
        /// </summary>
        public DateTime Updated { get; set; }
    }
}
