﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Dynamic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using MyExchange.Api.Constants;

namespace MyExchange.Api.Entity
{
    /// <summary>
    /// Сделка по валютной паре
    /// </summary>
    public class Trade : EntityBase
    {
        /// <summary>
        /// Идентификатор сделки на бирже
        /// </summary>
        public string TradeId { get; set; }

        public Guid ExchangeId { get; set; }

        public virtual Order Order { get; set; }
        public Guid? OrderId { get; set; }
        
        /// <summary>
        /// Курс валюты по которому произошла сделка
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// Количество закупленной/проданной валюты
        /// </summary>
        public decimal Quantity { get; set; }
        /// <summary>
        /// Суммарная цена сделки
        /// </summary>
        public decimal Amount { get; set; }

        public  Guid PairId { get; set; }
        public virtual Pair Pair { get; set; }

        public DateTime Date { get; set; }

        public TradeTypes TradeTypes { get; set; }

    }

   
}
