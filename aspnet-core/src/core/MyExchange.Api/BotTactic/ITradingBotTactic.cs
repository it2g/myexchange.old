﻿using System;
using MyExchange.Api.Constants;
using MyExchange.Api.Entity;

namespace MyExchange.Api.BotTactic
{
    /// <summary>
    /// Интерфейс для торгующих тактик
    /// </summary>
    public interface ITradingBotTactic : IBotTactic
    {
        /// <summary>
        /// Изменяет объем валюты, которым может оперировать тактика в процессе реализации
        /// торгового алгоритма. 
        /// Тот, кто будет вызывать данный метод, должен перехватывать Exceptions
        /// для анализа пиричны отказа в изменении значений и информаировании об этом трейдера
        /// TODO нужно подумать, что не всем тактикам бота нужна эта валюта
        /// и как следствие - само значение и метод его измененияя.
        /// </summary>
        /// <param name="currency"></param>
        /// <param name="value"></param>
        /// <param name="purpose"></param>
        void ChangeCurrencyValue(Currency currency, decimal value, string purpose);

        /// <summary>
        /// Директивная установка оператором ордера на куплепродажу валютной пары
        /// </summary>
        /// <param name="pair">валютная пара</param>
        /// <param name="value">объем покупаемой/продаваемой валюты</param>
        /// <param name="rate">курс, по которому нужно покупать/ продавать валюту</param>
        /// <param name="tradeTypes">тип сделки: покупка или продажа</param>
        /// <returns>Идентификатор ордера </returns>
        string SetOrder( Pair pair, decimal value, decimal rate, TradeTypes tradeTypes);

        /// <summary>
        /// Директивная отмена оператором выставленного ордера на покупку/продажу
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        bool CancelOrder(Guid orderId);
    }
   
}
