﻿using System;
using Abp.Application.Services;
using MyExchange.Api.Entity;

namespace MyExchange.Api.BotTactic
{
    /// <summary>
    /// Базовый интерфейс для всех тактик ботов, даже тех, котрые не торгуют,
    /// а, например, собирают информацию или информируют трейдера и пр.
    /// </summary>
    public interface IBotTactic : IApplicationService, IDisposable
    {
        Guid Id { get; }
    
        /// <summary>D:\Projects\00_zefslab\MyExchange\aspnet-core\src\core\MyExchange.Base\BotTactic\BotTacticBase.cs
        /// Название тактики, по которому она будет доступна в административной консоли
        /// Наименование хардкодиться при реализации интерфейса
        /// </summary>
        string Name { get; }
       
        /// <summary>
        /// Метод, котрый будет запускать алгоритм торговли, реализуемый
        /// каждой тактикой
        /// </summary>
        Action ExecuteTactic { get; }
    }

    /// <summary>
    /// Тактика бота которая оперирует контекстом данных
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    public interface IBotTactic<TContext> : IBotTactic
        where TContext : BotTacticContextBase
    {
        /// <summary>
        /// Контекст тактики содержит входные параметры для тактики,
        /// и данные пораждаемые в процессе работы, котрыми оперирует тактика
        /// и котрые могут пригодиться ей для продолжения работы
        /// в случае ее прерывания.
        /// </summary>
        TContext Context { get; set; }
    }

}
