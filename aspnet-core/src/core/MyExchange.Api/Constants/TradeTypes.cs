﻿namespace MyExchange.Api.Constants
{
    public enum TradeTypes 
    {
        /// <summary>
        /// Продажа
        /// </summary>
        Sell = 0,

        /// <summary>
        /// Покупка
        /// </summary>
        Bay = 1,

    }
}
