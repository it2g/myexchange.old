﻿namespace MyExchange.Api.Constants
{
    /// <summary>
    /// Статус агента
    /// </summary>
    public enum AgentStatuses
    {
        
        OnLine = 1,

        OnPause = 2,

        NotResponce = 3
    }
}
