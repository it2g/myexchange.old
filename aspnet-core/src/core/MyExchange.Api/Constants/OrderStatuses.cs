﻿namespace MyExchange.Api.Constants
{
    /// <summary>
    /// Статус ордера
    /// </summary>
    public enum OrderStatuses
    {
        
        New = 1,

        Canceled = 2,

        Closed = 3,

        Opened = 4
    }
}
