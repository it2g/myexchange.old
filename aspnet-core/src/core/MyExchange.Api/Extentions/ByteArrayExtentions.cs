﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyExchange.Api.Extentions
{
     public static class ByteArrayExtentions
    {
        public static string ConvertToString(this byte[] bytes)
        {
            string sbinary = "";

            for (int i = 0; i < bytes.Length; i++)
            {
                sbinary += bytes[i].ToString("X2"); // hex format
            }
            return (sbinary).ToLowerInvariant();
        }
    }
}
