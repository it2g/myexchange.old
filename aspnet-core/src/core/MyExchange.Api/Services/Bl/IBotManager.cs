﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Entity;
using MyExchange.Api.MyExchange;

namespace MyExchange.Api.Services.Bl
{
    /// <summary>
    /// Менеджер, котрый управляет ботами
    /// Он используется в Административной консоли для управления и мониторинга ботами.
    /// В автоматизированом режиме работы системы этот менеджер может использоваться и стратегией 
    /// самой торговой системы
    /// Под управлением понимается: запуск, остановка и установка на паузу ботов
    /// На самом деле все эти действия применяются к тактикам, а бот действует как оболочка
    /// для управления всеми этими действиями
    /// </summary>
    public interface IBotManager<TBotTactic, TBotTacticContext> : IApplicationService
        where TBotTacticContext : BotTacticContextBase
        where TBotTactic : IBotTactic<TBotTacticContext>
    {

        /// <summary>
        /// Запускает процесс торговли на бирже, реализующий ботом указанную тактику
        /// Подготавливает для бота тактику и контекст и передает его боту при его запуске
        /// </summary>
        /// <param name="exchange">биржа на которой нужно производить торговые операции</param>
        /// <param name="botStrategy">стратегия торговли, которую реализует бот</param>
        /// <param name="myExchangeStrategy">стратегия трговли в распоряжение которой передается бот</param>
        /// <param name="pair">валютная пара с которой нужно начинать торговать</param>
        /// <param name="value">объем денежных средств, котрые следует использовать при реализации стратегии</param>
        Task StartBot(TBotTactic bot);

        /// <summary>
        /// Останавливает указанный бот
        /// </summary>
        Task StopBot(TBotTactic bot);

        /// <summary>
        /// Приостанавливает торговые операции ботом
        /// При этом политику в отношении уже выставленных заявок определяет стратегия бота
        /// </summary>
        /// <param name="bot"></param>
        Task PauseBot(TBotTactic bot);

        /// <summary>
        /// Продолжение выполнения ранее остановленного бота
        /// </summary>
        /// <param name="bot"></param>
        Task ResumeBot(TBotTactic bot);
        
        /// <summary>
        /// Восстанавливает прерванные по непредвиденной причине работающие боты
        /// При этом информация о запущенных ботах берется из БД
        /// </summary>
        /// <param name="bot"></param>
        IList<TBotTactic> RestoreBots();

        /// <summary>
        /// Изменение количества средств, используемых ботом для торговли активами
        /// Требуется дополнительное обдумывание этого метода как с помощью него влиять на 
        /// сумму, выделенную ранее боту
        /// </summary>
        /// <param name="value"></param>
        Task ChangeValue(TBotTactic bot, decimal value);

     
    }
}
