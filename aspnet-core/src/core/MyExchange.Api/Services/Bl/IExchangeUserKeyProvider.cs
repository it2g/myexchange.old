﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MyExchange.Api.Data;

namespace MyExchange.Api.Services.Bl
{
    /// <summary>
    /// Предоставляет ключи пользователя для указанной биржи,
    /// чтобы обратиться к защищенным методам.
    /// </summary>
    public interface IExchangeUserKeyProvider
    {
        Task<ExchangeUserKeyPair> GetKeyAsync(Guid exchangeId);
    }
}
