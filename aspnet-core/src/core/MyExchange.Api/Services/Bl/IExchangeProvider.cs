﻿using System;
using System.Collections;
using System.Collections.Generic;
using Abp.Application.Services;
using MyExchange.Api.MyExchange;

namespace MyExchange.Api.Services.Bl
{
    public interface IExchangeProvider : IApplicationService
    {
        Exchange Get(Guid id);

        IList<Exchange> GetAll();
    }
}
