﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using MyExchange.Api.Data;

namespace MyExchange.Api.Services.Bl
{
    /// <summary>
    /// Предоставляет сервис управления свечами
    /// </summary>
    public interface IBarService : IApplicationService
    {
        /// <summary>
        /// Предоставляет список свечей указанной размерности для валюты
        /// не конкретной бирже 
        /// </summary>
        /// <returns></returns>
        Task<IList<Data.Bar>> GetList(Guid pairId, Guid exchangeId, TimeSpan scale, DateTime begin, DateTime end, int countBars = 0);
        /// <summary>
        /// Возвращает все свечи из указанного диапазона
        /// </summary>
        /// <param name="pairId"></param>
        /// <param name="exchangeId"></param>
        /// <param name="scale"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        Task<IList<Bar>> GetList(Guid pairId, Guid exchangeId, TimeSpan scale, DateTime begin, DateTime end);
        Task<IList<Bar>> GetList(Guid pairId, Guid exchangeId, TimeSpan scale, int countBars = 0);


    }
}
