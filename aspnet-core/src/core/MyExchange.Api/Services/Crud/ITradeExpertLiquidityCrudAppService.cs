﻿using System;
using MyExchange.Api.Dto.TradeExpertLiquidity;

namespace MyExchange.Api.Services.Crud
{
    public interface ITradeExpertLiquidityCrudAppService :
        IExhchangeCrudAppServices
            <TradeExpertLiquidityDto, Guid, TradeExpertLiquidityPagedResultRequestDto, TradeExpertLiquidityDto,
                TradeExpertLiquidityDto, TradeExpertLiquidityDto, TradeExpertLiquidityDto>
    {
    }


}
