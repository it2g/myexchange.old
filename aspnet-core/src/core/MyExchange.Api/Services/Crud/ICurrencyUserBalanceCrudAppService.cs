﻿using System;
using MyExchange.Api.Dto.CurrencyUserBalance;

namespace MyExchange.Api.Services.Crud
{
    public interface ICurrencyUserBalanceCrudAppService :
        IExhchangeCrudAppServices<CurrencyUserBalanceDto, Guid, CurrencyUserBalancePagedResultRequestDto, CurrencyUserBalanceDto, CurrencyUserBalanceDto, CurrencyUserBalanceDto, CurrencyUserBalanceDto>

    {
    }
    
}
