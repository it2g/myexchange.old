﻿using System;
using MyExchange.Api.Dto.TradeExpertAdvice;

namespace MyExchange.Api.Services.Crud
{
    public interface ITradeExpertAdviceCrudAppService : IExhchangeCrudAppServices<TradeExpertAdviceDto, Guid, TradeExpertAdvicePagedResultRequestDto,  TradeExpertAdviceDto,  TradeExpertAdviceDto,  TradeExpertAdviceDto, TradeExpertAdviceDto>
    {
    }
}
