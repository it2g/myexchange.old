﻿using System;
using System.Threading.Tasks;
using MyExchange.Api.Dto.ExchangeUser;

namespace MyExchange.Api.Services.Crud
{
    public interface IExchangeUserCrudAppService : IExhchangeCrudAppServices<ExchangeUserDto, Guid, ExchangeUserPagedResultRequestDto, ExchangeUserDto, ExchangeUserDto, ExchangeUserDto, ExchangeUserDto>
 
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="exchangeId">идентификатор биржи</param>
        /// <param name="userId">пользователь зарегистрированный в системе (не на бирже)</param>
        /// <returns></returns>
       Task<ExchangeUserDto> Get(Guid exchangeId, long userId);
    }
}
