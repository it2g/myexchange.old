﻿using System;
using MyExchange.Api.Dto.Order;

namespace MyExchange.Api.Services.Crud
{
    public interface IOrderCrudAppService : IExhchangeCrudAppServices<OrderDto, Guid, OrderPagedResultRequestDto, OrderDto, OrderDto, OrderDto,OrderDto>
    {
    }
    
}
