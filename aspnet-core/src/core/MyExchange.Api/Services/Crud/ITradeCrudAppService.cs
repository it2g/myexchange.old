﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyExchange.Api.Dto.Trade;

namespace MyExchange.Api.Services.Crud
{
    public interface ITradeCrudAppService : IExhchangeCrudAppServices<TradeDto, Guid, TradePagedResultRequestDto, TradeDto, TradeDto, TradeDto, TradeDto>
    
    {
        /// <summary>
        /// Принимает список сделок считанных с биржи и находит среди них те, котрых еще нет в БД
        /// сравнимая по внешним идентификаторам и добавляет их в БД
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
       Task<IList<TradeDto>> AddList(IList<TradeDto> list);
    }
}
