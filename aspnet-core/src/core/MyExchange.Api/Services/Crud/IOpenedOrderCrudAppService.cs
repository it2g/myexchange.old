﻿using System;
using MyExchange.Api.Dto.OpenedOrder;

namespace MyExchange.Api.Services.Crud
{
    public interface IOpenedOrderCrudAppService : IExhchangeCrudAppServices<OpenedOrderDto, Guid, OpenedOrderPagedResultRequestDto, OpenedOrderDto, OpenedOrderDto, OpenedOrderDto, OpenedOrderDto>
    {
    }
    
}
