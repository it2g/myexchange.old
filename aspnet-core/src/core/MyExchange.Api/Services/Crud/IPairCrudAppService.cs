﻿using System;
using MyExchange.Api.Dto.Pair;

namespace MyExchange.Api.Services.Crud
{
    public interface IPairCrudAppService : IExhchangeCrudAppServices<PairDto, Guid, PairPagedResultRequestDto, PairDto, PairDto, PairDto, PairDto>
    {
       
    }
}
