﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyExchange.Api.Data
{
    /// <summary>
    /// Пара ключей для доступа к защищенным методам
    /// на бирже
    /// </summary>
    public class ExchangeUserKeyPair
    {
        public Guid ExchangeUserId { get; set; }

        public string ApiOpenKey { get; set; }

        public string ApiSecretKey { get; set; }
    }
}
