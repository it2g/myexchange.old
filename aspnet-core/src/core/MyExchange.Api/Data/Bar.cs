﻿
using System;
using MyExchange.Api.Constants;

namespace MyExchange.Api.Data
{
    /// <summary>
    /// Свеча 
    /// На ее онове формируеся таймфрем: 1мин/5мин/15/30/1час/4часа/день/неделя/месяц/год
    /// Изредка бывает так, что Open=Close и такое состояние называют Дожи
    /// </summary>
    public class Bar 
    {
        /// <summary>
        /// Самая выскоая цена сделки в рамках свечи
        /// </summary>
        public decimal High { get; set; }

        /// <summary>
        /// Цена последней сделки в интервале свечи
        /// </summary>
        public decimal Close { get; set; }
        
        /// <summary>
        /// Цена первой сделки в интервале свечи
        /// </summary>
        public decimal Open { get; set; }
        
        /// <summary>
        /// Самая низкая цена сделки в рамках свечи
        /// </summary>
        public decimal Low { get; set; }

        /// <summary>
        /// Рамерность свечи по продолжительности времени
        /// TODO Подумать о том, зачем нужно это свойство и удалить его если оно не пригодиться.
        /// </summary>
        public TimeSpan Scale { get; set; }

        /// <summary>
        /// Тренд свечи
        /// </summary>
        public Trends Trend { get; set; }
    }
}
