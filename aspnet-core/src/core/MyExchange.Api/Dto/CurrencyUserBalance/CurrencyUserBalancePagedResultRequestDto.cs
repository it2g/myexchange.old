﻿using Abp.Application.Services.Dto;

namespace MyExchange.Api.Dto.CurrencyUserBalance
{
    public class CurrencyUserBalancePagedResultRequestDto : PagedAndSortedResultRequestDto
    {
    }
}
