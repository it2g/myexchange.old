﻿using System;

namespace MyExchange.Api.Dto.OpenedOrder
{
    public class CreateOpenedOrderDto
    {
        public Guid OrderId { get; set; }

        public Guid UserId { get; set; }

        public decimal StopLoss { get; set; }

        public decimal TakeProfit { get; set; }

        public string Comment { get; set; }

    }
}
