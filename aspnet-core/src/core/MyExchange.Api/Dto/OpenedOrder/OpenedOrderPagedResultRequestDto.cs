﻿using MyExchange.Api.MyExchange;

namespace MyExchange.Api.Dto.OpenedOrder
{
    public class OpenedOrderPagedResultRequestDto
    {
        public Exchange Exchange { get; set; }

        /// <summary>
        /// Объем выручки, котрая получена в процессе работы бота
        /// </summary>
        public decimal Value { get; set; }

        public Entity.Pair Pair { get; set; }
    }
}
