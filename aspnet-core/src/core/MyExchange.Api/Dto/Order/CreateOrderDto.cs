﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Runtime.Validation;
using MyExchange.Api.Constants;

namespace MyExchange.Api.Dto.Order
{
   public class CreateOrderDto : IShouldNormalize
    {
        public Guid PairId { get; set; }

       public Guid ExchangeId { get; set; }

       public DateTime Date { get; set; }

       /// <summary>
       /// Цена покупки/продажи
       /// </summary>
       public decimal Price { get; set; }


       public decimal Quantity { get; set; }

       /// <summary>
       /// Тип сделки
       /// </summary>
       public TradeTypes TradeTypes { get; set; }

       /// <summary>
       /// Состояние выставленного ордера
       /// </summary>
       public OrderStatuses OrderStatus { get; set; }
        public void Normalize()
        {
           
        }
    }
}
