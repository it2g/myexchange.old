﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;

namespace MyExchange.Api.Dto.Trade
{
    public class TradePagedResultRequestDto : PagedAndSortedResultRequestDto
    {
        /// <summary>
        /// Время, после которого нужно выгрузить все сделки до текущего момента
        /// </summary>
        public DateTime? LastRequestTime { get; set; }

        public Guid ExchangeId { get; set; }

        public Guid PairId { get; set; }
    }
}
