﻿using System;
using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Dto._DtoBase;

namespace MyExchange.Api.Dto.Pair
{
    public class PairDto: DtoBase
    {
        public CurrencyDto Currency1 { get; set; }
        public Guid Currency1Id { get; set; }

        public CurrencyDto Currency2 { get; set; }
        public Guid Currency2Id { get; set; }

        public string Name { get; set; }

        public override string ToString()
        {
            return $"{Currency1.BrifName}-{Currency2.BrifName}";
        }
        public string GetSystemName(string splitter)
        {
            return Currency1.BrifName + splitter + Currency2.BrifName;
        }
    }
}
