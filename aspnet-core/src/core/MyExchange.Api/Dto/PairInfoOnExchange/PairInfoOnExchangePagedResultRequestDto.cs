﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;

namespace MyExchange.Api.Dto.PairInfoOnExchange
{
   public class PairInfoOnExchangePagedResultRequestDto : PagedAndSortedResultRequestDto
    {
        /// <summary>
        /// Выбирает все валютные пары для указанной биржи
        /// </summary>
        public Guid? ExchangeId;
    }
}
