﻿namespace MyExchange.Api.Data
{
    /// <summary>
    /// Сигнал на вход в рынок
    /// </summary>
    public class Signal
    {
        public decimal Price { get; set; }

        public decimal Stoploss { get; set; }

        public decimal TakeProfit { get; set; }
    }
}
