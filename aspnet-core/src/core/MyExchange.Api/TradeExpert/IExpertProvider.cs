﻿using System.Collections.Generic;
using Abp.Application.Services;

namespace MyExchange.Api.TradeExpert
{
    public interface IExpertProvider : IApplicationService
    {
        IList<ITradeAdviser> GetExperts(IDictionary<string, object> parametrs);

    }
}
