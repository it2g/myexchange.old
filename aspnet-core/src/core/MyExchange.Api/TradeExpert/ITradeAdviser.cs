﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MyExchange.Api.Constants;
using MyExchange.Api.Entity;

namespace MyExchange.Api.TradeExpert
{
    /// <summary>
    /// Эксперт в области торговли основанный на одном из устойчивых паттернов
    /// </summary>
    public interface ITradeAdviser : ITradeExpert
    {
        /// <summary>
        /// Перечень типов стратегий, для которых наиболее приспособлен
        /// эксперт и может давать советы для торговых тактик
        /// </summary>
        IList<TradingTypes> TradingTypeList { get; }

        Task<TradeExpertAdvice> GetAdviceAsync();
    }
}
