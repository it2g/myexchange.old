﻿using System;

namespace MyExchange.Api.TradeExpert
{
    /// <summary>
    /// Эксперт в области торговли основанный на одном из устойчивых паттернов
    /// </summary>
    public interface ITradeIndicator: ITradeExpert
    {
        Action ExecuteIndicator { get; }
    }
}
