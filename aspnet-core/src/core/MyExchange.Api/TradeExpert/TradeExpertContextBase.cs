﻿using System;
using MyExchange.Api.Entity;

namespace MyExchange.Api.TradeExpert
{
    /// <summary>
    /// Контекст, которым оперирует эксперт
    ///  </summary>
    public abstract class TradeExpertContextBase : BotTacticContextBase
    {
        public Guid PairId { get; set; }

        public Guid ExchangeId { get; set; }
    }
}
