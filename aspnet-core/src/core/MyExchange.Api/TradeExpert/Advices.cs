﻿namespace MyExchange.Api.TradeExpert
{
    /// <summary>
    /// Совет в отношении сложившегося тренда на рынке
    /// </summary>
    public enum Advices
    {
        Growin = 1,
        Falls = 2,
        Stable = 3
    }
}