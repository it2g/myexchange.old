﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Entity;

namespace MyExchange.Api.Dal.EfConfigurations
{
    public abstract class BotTacticsContextBaseMap<TEntity> : BaseEntityMap<TEntity>
        where TEntity : BotTacticContextBase
    {
        public override void Configure(EntityTypeBuilder<TEntity> builder)
        {
            base.Configure(builder);
          
            builder.Property(x => x.BotTacticId).HasColumnName("bot_tactic_id");
            builder.Property(x => x.Status).HasColumnName("status_id");
            builder.Property(x => x.StartTime).HasColumnName("start_time");
            builder.Property(x => x.StopTime).HasColumnName("stop_time");
        }
    }
}
