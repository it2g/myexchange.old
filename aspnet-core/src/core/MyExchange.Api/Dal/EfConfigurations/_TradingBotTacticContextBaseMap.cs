﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Entity;

namespace MyExchange.Api.Dal.EfConfigurations
{
    public abstract class TradingBotTacticContextBaseMap<TEntity> : BotTacticsContextBaseMap<TEntity>
        where TEntity : TradingBotTacticContextBase
    {
        public override void Configure(EntityTypeBuilder<TEntity> builder)
        {
            base.Configure(builder);
          
            builder.Property(x => x.TradingStratagyId).HasColumnName("trading_stratagy_id");
            builder.Property(x => x.ExchangeId).HasColumnName("exchange_id");
            builder.Property(x => x.PairId).HasColumnName("pair_id");
        }
    }
}
