﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Repositories;
using MyExchange.Api.Entity;

namespace MyExchange.Api.Dal
{
    public interface ITradeExpertLiquidityRepository : IRepository<TradeExpertLiquidity, Guid>
    {
    
    }
}
