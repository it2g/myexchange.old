﻿using System;
using System.Collections.Generic;
using MyExchange.Api.Dto.ExchangeUser;
using MyExchange.Api.Entity;

namespace MyExchange.Api.MyExchange
{
    /// <summary>
    /// Баланс кошелька пользователя 
    /// </summary>
    public class ExchangeUserInfo
    {
        public ExchangeUserInfo()
        {
            Balances = new Dictionary<Currency, decimal>();
            Reserved = new Dictionary<Currency, decimal>();
        }
        public Guid ExchangeUserId { get; set; }

        public DateTime InfoReciveDate { get; set; }
        
        /// <summary>
        /// Доступный баланс на счету пользователя по валютам
        /// TODO Не понятно как предполагается записывать в БД это и следующее всойство?
        /// </summary>
        public IDictionary<Currency, Decimal> Balances { get; }

        /// <summary>
        /// Баланс пользователя в ордерах
        /// </summary>
        public IDictionary<Currency, Decimal> Reserved { get; }
    
    }
}
