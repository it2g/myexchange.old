﻿using System;
using System.Collections.Generic;
using System.Text;
using MyExchange.Api.Entity;

namespace MyExchange.Api.MyExchange
{
    /// <summary>
    /// Объем денежных средств в определенной валюте, котрый может быть
    /// передан/возврашен ботом
    /// </summary>
    public class CurrencyValue
    {
        public Currency Currency {get; set; }

        public decimal Value { get; set; }
    }
}
