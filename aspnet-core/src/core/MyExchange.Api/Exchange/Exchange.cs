﻿using System;

namespace MyExchange.Api.MyExchange
{
   

        /// <summary>
        /// Зарегистрированная в системе биржа, по отношении к которой можно применять стратегии 
        /// торговли активами/
        /// Инициализация биржи производится обязательным назначением ей драйвера для api
        /// <typeparam name="TDriver">Тип драйвера</typeparam>
        /// </summary>
        public abstract class Exchange
        {
        /// <summary>
        /// Это ключевое поле, по которому будет
        /// определяться приндлежность сущностей к бирже
        /// </summary>
        public virtual Guid Id { get; }

        public virtual string BriefName { get; }

        public virtual string FullName { get;}

        public virtual string Url { get;}

        public virtual string Description { get;}

        public IExchangeDriver ExchangeDriver { get; protected set; }
    }

    public abstract class Exchange<TExchangeDriver> : Exchange
        where TExchangeDriver : IExchangeDriver
    {
        protected Exchange(TExchangeDriver driver)
        {
            ///Инициализируется драйвер конкретного типа для конкретной биржи
            ExchangeDriver = driver;
            ExchangeDriver.Exchange = this;
        }
       
    }
}
