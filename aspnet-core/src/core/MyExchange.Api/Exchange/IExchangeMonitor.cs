﻿using Abp.Application.Services;

namespace MyExchange.Api.MyExchange
{
    /// <summary>
    /// Инструментарий для отслеживания работы систмы и представления
    /// метрических показателей эффективности
    /// </summary>
    public interface IExchangeMonitor : IApplicationService
    {
    }
}
