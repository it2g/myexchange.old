﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using MyExchange.Api.Constants;
using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Dto.Order;
using MyExchange.Api.Dto.Pair;
using MyExchange.Api.Dto.PairInfoOnExchange;
using MyExchange.Api.Dto.PairSettings;
using MyExchange.Api.Dto.Trade;

namespace MyExchange.Api.MyExchange
{
    /// <summary>
    /// Интерфейс драйвера для взаимодействия с  биржей.
    /// При запуске бота ему передается стратегия, в которой указывается биржа.
    /// Каждая биржа имеет реализацию драйвера, посредством которого 
    /// идет обращение к бирже на основе ее api
    /// </summary>
    public interface IExchangeDriver : IApplicationService
    {
        Exchange Exchange { set; }

        #region Public methods
        /// <summary>
        /// Запрос на список сделок по валютной паре
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<TradeDto>> TradesAsync(PairDto pair);

        /// <summary>
        /// Запрос на списки сделок по валютным нескольким парам
        /// </summary>
        /// <param name="pairs"></param>
        /// <returns></returns>
        Task<IDictionary<PairDto, IEnumerable<TradeDto>>> TradesAsync(IList<PairDto> pairs);

        /// <summary>
        /// Ордера по валютной паре
        /// </summary>
        /// <param name="pair"></param>
        /// <param name="limit">кол-во отображаемых позиций (по умолчанию 100, максимум 1000)</param>
        /// <returns></returns>
        Task<IEnumerable<OrderDto>> OrdersAsync(PairDto pair, uint limit = 100);

        /// <summary>
        /// Cтатистика цен и объемов торгов по валютным парам
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<PairInfoOnExchangeDto>> TickerAsync();

        /// <summary>
        /// Cписок валют биржи
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<CurrencyDto>> CurrenciesAsync();

        /// <summary>
        /// Получение настроек валютных пар пары
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<PairSettings>> PairSettingsAsync();

        #endregion

        #region Authenticated methods

        Task<ExchangeUserInfo> UserInfoAsync();

        ///// <summary>
        ///// Получение информации о балансе кошелька на бирже
        ///// </summary>
        ///// <returns></returns>
        //IEnumerable<CurrencyUserBalance> GetWalletBalance(Guid exchangeId, Guid exchangeUserId);

        /// <summary>
        /// Получение списка открытых ордеров пользователя
        /// </summary>
        /// <param name="exchangeId"></param>
        /// <param name="exchangeUserId"></param>
        /// <returns></returns>
        Task<IEnumerable<OrderDto>> OpenOrdersAsync();

        Task OrderCreateAsync(OrderDto order, OrderCreateTypes orderCreateType);

        #endregion
    }
}
