﻿using System;
using System.Collections.Generic;
using System.Text;
using MyExchange.Api.Entity;

namespace MyExchange.Api.MyExchange
{
    /// <summary>
    /// Бухгалтер - контролирует совместное использование ботами денежных средств и 
    /// выделение этих средств ботам перед запуском или перераспределение их
    /// какой либо стратегией между ботами.
    /// </summary>
    public interface IAccountant
    {
        /// <summary>
        /// Выделяет денежные средства запрошенные в какой либо валюте на указанные цели
        /// </summary>
        /// <param name="currency"></param>
        /// <param name="value"></param>
        CurrencyValue GetMoney(Currency currency, decimal value, string purpose);

        /// <summary>
        /// Возвращение денежных средств в кошелек, чтобы они стали доступны другим ботам
        /// </summary>
        /// <param name="currency"></param>
        /// <param name="value"></param>
        /// <param name="reason"></param>
        void ReturnMoney(Currency currency, decimal value, string reason);
    }
}
