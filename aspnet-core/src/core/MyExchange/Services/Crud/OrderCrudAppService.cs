﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto;
using MyExchange.Api.Dto.Order;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Crud;
using MyExchange.Web.Services;

namespace MyExchange.Services
{
   public class OrderCrudAppService : MyExchangeCrudServiceBase<Order, OrderDto, OrderPagedResultRequestDto, Guid>, IOrderCrudAppService
    {
        public OrderCrudAppService(IOrderRepository repository) : base(repository)
        {
        }
    }
}
