﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Castle.DynamicProxy.Internal;
using JetBrains.Annotations;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto;
using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Dto.Pair;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Crud;
using MyExchange.Web.Services;

namespace MyExchange.Services
{
    public class PairCrudAppService : MyExchangeCrudServiceBase<Pair, PairDto, PairPagedResultRequestDto, Guid>, IPairCrudAppService
    {
        private readonly ICurrencyRepository _currencyRepository;
        public PairCrudAppService(IPairRepository repository,
            [NotNull] ICurrencyRepository currencyRepository) : base(repository)
        {
            _currencyRepository = currencyRepository ?? throw new ArgumentNullException(nameof(currencyRepository));
        }

        public override async Task<PairDto> Create(PairDto input)
        {
            var dto = await base.Create(input);

            dto.Currency1 = (await _currencyRepository.GetAsync(dto.Currency1Id)).MapTo<CurrencyDto>();
            dto.Currency2 = (await _currencyRepository.GetAsync(dto.Currency2Id)).MapTo<CurrencyDto>();
            dto.Name = $"{dto.Currency1.BrifName}/{dto.Currency2.BrifName}";
            return dto;
        }

       
    }
}
