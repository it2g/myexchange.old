﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto;
using MyExchange.Api.Dto.TradeExpertAdvice;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Crud;
using MyExchange.Web.Services;

namespace MyExchange.Services
{
    public class TradeExpertAdviceCrudAppService : MyExchangeCrudServiceBase<TradeExpertAdvice, TradeExpertAdviceDto, TradeExpertAdvicePagedResultRequestDto, Guid>, ITradeExpertAdviceCrudAppService
    {
        public TradeExpertAdviceCrudAppService(ITradeExpertAdviceRepository repository) : base(repository)
        {
        }

    }
}
