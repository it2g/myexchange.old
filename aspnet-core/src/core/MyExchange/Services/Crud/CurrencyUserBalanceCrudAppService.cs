﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services;
using Abp.Domain.Repositories;
using MyExchange.Api.Entity;
using System.Threading.Tasks;
using MyExchange.Api.Dal;
using Abp.Application.Services.Dto;
using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Dto.CurrencyUserBalance;
using MyExchange.Api.Services.Crud;
using MyExchange.Web.Services;

namespace MyExchange.Services
{
    public class CurrencyUserBalanceCrudAppService : MyExchangeCrudServiceBase<CurrencyUserBalance, CurrencyUserBalanceDto, CurrencyUserBalancePagedResultRequestDto, Guid>, ICurrencyUserBalanceCrudAppService
    {
        public CurrencyUserBalanceCrudAppService(ICurrencyUserBalanceRepository repository) : base(repository)
        {
        }
    }
}
