﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto.ExchangeUser;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Crud;
using MyExchange.Web.Services;

namespace MyExchange.Services
{
    public class ExchangeUserCrudAppService : MyExchangeCrudServiceBase< ExchangeUser, ExchangeUserDto, ExchangeUserPagedResultRequestDto, Guid>, IExchangeUserCrudAppService
    {
        public ExchangeUserCrudAppService(IExchangeUserRepository repository) : base(repository)
        {
        }

        public async Task<ExchangeUserDto> Get(Guid exchangeId, long userId)
        {
            var user = await Repository.FirstOrDefaultAsync(
                u => u.ExchangeId == exchangeId && u.UserId == userId);

            return user?.MapTo<ExchangeUserDto>();
        }

    }
}
