﻿using Abp.Application.Services;
using MyExchange.Api.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto;
using MyExchange.Api.Dto.TradeExpertLiquidity;
using MyExchange.Api.Services.Crud;
using MyExchange.Web.Services;

namespace MyExchange.Services
{
    public class TradeExpertLiquidityCrudAppService :
        MyExchangeCrudServiceBase<TradeExpertLiquidity, TradeExpertLiquidityDto, TradeExpertLiquidityPagedResultRequestDto, Guid>, ITradeExpertLiquidityCrudAppService
    {
        public TradeExpertLiquidityCrudAppService(ITradeExpertLiquidityRepository repository) : base(repository)
        {

        }

    }
}
