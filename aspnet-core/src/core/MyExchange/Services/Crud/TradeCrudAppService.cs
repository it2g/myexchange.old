﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto.Trade;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Crud;
using MyExchange.Web.Services;

namespace MyExchange.Services
{
    public class TradeCrudAppService : MyExchangeCrudServiceBase<Trade, TradeDto, TradePagedResultRequestDto, Guid>, ITradeCrudAppService
    {
        public TradeCrudAppService(ITradeRepository repository) : base(repository)
        {
        }

        public async Task<IList<TradeDto>> AddList(IList<TradeDto> list)
        {
            IList<TradeDto> outputList = new List<TradeDto>();

            foreach (var dto in list)
            {
                var trade = await Create(dto);

                outputList.Add(trade);
            }


            return outputList;
        }

        protected override IQueryable<Trade> CreateFilteredQuery(TradePagedResultRequestDto input)
        {
            var query = base.CreateFilteredQuery(input).Where(x=> x.ExchangeId == input.ExchangeId);

            if (input.LastRequestTime != null)
            {
                query = query.Where(x => x.CreationTime >= input.LastRequestTime);
            }

            return query;
        }
    }
}
