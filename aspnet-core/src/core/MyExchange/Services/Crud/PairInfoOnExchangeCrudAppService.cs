﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto;
using MyExchange.Api.Dto.PairInfoOnExchange;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Crud;
using MyExchange.Web.Services;

namespace MyExchange.Services
{
    public class PairInfoOnExchangeCrudAppService :
        MyExchangeCrudServiceBase<PairInfoOnExchange, PairInfoOnExchangeDto, PairInfoOnExchangePagedResultRequestDto, Guid>, IPairInfoOnExchangeCrudAppService
    {
        public PairInfoOnExchangeCrudAppService(IPairInfoOnExchangeRepository repository) : base(repository)
        {
        }

        protected override IQueryable<PairInfoOnExchange> CreateFilteredQuery(PairInfoOnExchangePagedResultRequestDto input)
        {
            var query = base.CreateFilteredQuery(input);

            if (input.ExchangeId != null && input.ExchangeId != Guid.Empty)
            {
                query = query.Where(pair => pair.ExchangeId == input.ExchangeId);
            }

            return query;
        }
    }
}
