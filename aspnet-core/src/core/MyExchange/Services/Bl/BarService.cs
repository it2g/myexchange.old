﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using JetBrains.Annotations;
using MyExchange.Api.Constants;
using MyExchange.Api.Data;
using MyExchange.Api.Dto.Trade;
using MyExchange.Api.Services.Bl;
using MyExchange.Api.Services.Crud;


namespace MyExchange.Services.Bl
{
    /// <summary>
    /// Предоставляет сервис управления свечами
    /// </summary>
    public class BarService : ApplicationService, IBarService
    {
        private ITradeCrudAppService _tradeCrudAppService;

        public BarService([NotNull] ITradeCrudAppService tradeCrudAppService)
        {
            _tradeCrudAppService = tradeCrudAppService ?? throw new ArgumentNullException(nameof(tradeCrudAppService));
        }

        public async Task<IList<Bar>>  GetList(Guid pairId, Guid exchangeId, TimeSpan barScale, DateTime begin, DateTime end, int countBars = 0)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pairId"></param>
        /// <param name="exchangeId"></param>
        /// <param name="barScale"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public async Task<IList<Bar>> GetList(Guid pairId, Guid exchangeId, TimeSpan barScale, DateTime begin, DateTime end)
        {
            var filter = new TradePagedResultRequestDto()
            {
                ExchangeId = exchangeId,
                PairId = pairId,
                LastRequestTime = begin
            };

            var trades = (await _tradeCrudAppService.GetAll(filter)).Items;

            var bars = new List<Bar>();

            DateTime slider = begin;

            for (int barNumber = 1; slider < end-TimeSpan.FromTicks(20); barNumber++)
            {
                var barTrades = trades
                                .Where(t => t.Date >= slider && t.Date < slider + barScale )
                                .OrderBy(t => t.Date).ToList();
                var bar = new Bar();

                if (!barTrades.Any())
                {
                    var prevoseBar = bars.LastOrDefault();
                    if (prevoseBar != null)
                    {
                        bar = new Bar()
                        {
                            Scale = barScale,
                            Open = prevoseBar.Close,
                            Close = prevoseBar.Close,
                            High = prevoseBar.Close,
                            Low = prevoseBar.Close,
                        };
                    }
                    else
                    {
                        //TODO нужна свеча с предыдущего диапазаона
                        throw new Exception("В первой свече нет ни одной сделки, поэтому определить ее не возможно.");
                    }
                }
                else
                {
                    bar = new Bar()
                    {
                        Scale = barScale,
                        Open = barTrades.First().Price,
                        Close = barTrades.Last().Price,
                        High = barTrades.OrderBy(t => t.Price).Last().Price,
                        Low = barTrades.OrderByDescending(t => t.Price).Last().Price,
                    };

                    bar.Trend = TrendDefine(bar);
                }
                bars.Add(bar);

                slider += barScale;
            }
            return bars;
        }

        /// <summary>
        /// TODO нужен модульный тест для этого метода
        /// </summary>
        public async Task<IList<Bar>> GetList(Guid pairId, Guid exchangeId, TimeSpan barScale, int countBars = 0)
        {
            var trades = await GetList(pairId, exchangeId, barScale,  DateTime.UtcNow - (countBars * barScale), DateTime.UtcNow);

            return trades;
        }

        /// <summary>
        /// Определение тренда свечи по некоторым характеристикам
        /// </summary>
        /// <returns></returns>
        private Trends TrendDefine(Bar bar)
        {
            if (bar.Open > bar.Close)
            {
                return Trends.Downtrend;

            }
            else if(bar.Open < bar.Close)
            {
                return Trends.Uptrend;
            }
            
            return Trends.Sideways;
        }
    }
}
