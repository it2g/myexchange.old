﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using MyExchange.Api.Data;
using MyExchange.Api.MyExchange;
using MyExchange.Api.Services.Crud;

namespace MyExchange.Api.Services.Bl
{
    public class ExchangeUserKeyProvider : ApplicationService, IExchangeUserKeyProvider
    {
        private IExchangeUserCrudAppService _exchangeUserCrudAppService;

        public ExchangeUserKeyProvider(IExchangeUserCrudAppService exchangeUserCrudAppService)
        {
            _exchangeUserCrudAppService = exchangeUserCrudAppService;
        }

        public async Task<ExchangeUserKeyPair> GetKeyAsync(Guid exchangeId)
        {                      
            var userId = (long)AbpSession.UserId;

            var exchangeUser = await  _exchangeUserCrudAppService.Get(exchangeId, userId);

            var keyPair = new ExchangeUserKeyPair
            {
                ExchangeUserId = exchangeUser.Id,
                ApiOpenKey = exchangeUser.ApiOpenKey,
                ApiSecretKey = exchangeUser.ApiSecretKey
            };
            
            return keyPair;
        }
    }
}
