﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services;
using Abp.Dependency;
using JetBrains.Annotations;
using MyExchange.Api.Entity;
using MyExchange.Api.MyExchange;
using MyExchange.Api.Services.Bl;
using NodaTime.TimeZones;

namespace MyExchange.Services.Bl
{
    public class ExchangeProvider : ApplicationService, IExchangeProvider
    {
        private readonly IIocManager _iocManager;

        public ExchangeProvider(IIocManager iocManager)
        {
            _iocManager = iocManager;
        }

        public Exchange Get(Guid id)
        {
            ///На тип Exchange зарегистрировано много бирж с назличными именами.
            /// В качестве имени использовался идентификатор биржи
            var exchange = _iocManager.IocContainer.Resolve<Exchange>(id.ToString());

            return exchange;
        }

        public IList<Exchange> GetAll()
        {
            var exchanges = _iocManager.IocContainer.ResolveAll<Exchange>();

            return exchanges;
        }
    }
}
