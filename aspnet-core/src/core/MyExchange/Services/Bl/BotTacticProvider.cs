﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services;
using Abp.Dependency;
using JetBrains.Annotations;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Entity;
using MyExchange.Api.MyExchange;
using MyExchange.Api.Services.Bl;
using NodaTime.TimeZones;

namespace MyExchange.Services.Bl
{
    /// <summary>
    /// Предоставляет тактику торговли по идентификатору
    /// </summary>
    public class BotTacticProvider : ApplicationService, IBotTacticProvider
    {
        private readonly IIocManager _iocManager;

        public BotTacticProvider(IIocManager iocManager)
        {
            _iocManager = iocManager;
        }

        /// <summary>
        /// Находит в контейнере зависимостей тактику, в соответсвии с идентификатором
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IBotTactic Get(Guid id)
        {
            var tactic = _iocManager.IocContainer.Resolve<IBotTactic>(id.ToString());

            return tactic;
        }

        public TBotTactic CreateBotTactic<TBotTactic>(TBotTactic botTactic, BotTacticContextBase botTacticContext)
        {
            throw new NotImplementedException();
        }
    }
}
