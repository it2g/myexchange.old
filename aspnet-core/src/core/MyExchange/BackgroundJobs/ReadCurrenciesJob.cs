﻿using System;
using System.Collections.Generic;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Dependency;
using JetBrains.Annotations;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Services.Bl;
using MyExchange.Api.Services.Crud;

namespace MyExchange.BotTacticMonitoring.BackgroundJobs
{
    /// <summary>
    /// Фоновая задача для считывания валют с указанной биржи
    /// </summary>
    public class ReadCurrenciesJob : BackgroundJob<Guid>, ITransientDependency
    {
        private IExchangeProvider _exchangeProvider;
        private ICurrencyRepository _currencyRepository;
        private readonly ICurrencyCrudAppService _currencyCrudService;

        private object locker = new object();

        public ReadCurrenciesJob([NotNull] IExchangeProvider exchangeProvider,
            [NotNull] ICurrencyRepository currencyRepository,
            [NotNull] ICurrencyCrudAppService currencyCrudService)
        {
            _exchangeProvider = exchangeProvider ?? throw new ArgumentNullException(nameof(exchangeProvider));
            _currencyRepository = currencyRepository ?? throw new ArgumentNullException(nameof(currencyRepository));
            _currencyCrudService = currencyCrudService ?? throw new ArgumentNullException(nameof(currencyCrudService));
        }

       
        public override void Execute( Guid exchangeId)
        {
            var exchange = _exchangeProvider.Get(exchangeId);

            IEnumerable<CurrencyDto> currencies = exchange.ExchangeDriver.CurrenciesAsync().Result;

            foreach (var currency in currencies)
            {
                using (var unitOfWork = UnitOfWorkManager.Begin())
                {
                    lock (locker)
                    {
                    var curr = _currencyRepository
                        .FirstOrDefault(x => x.BrifName.ToUpper() == currency.BrifName.ToUpper());
                    if (curr == null)
                    {
                        currency.BrifName = currency.BrifName.ToUpper();

                        var currencyDto = _currencyCrudService.Create(currency.MapTo<CurrencyDto>()).Result;

                        Console.WriteLine($"Добавлена  валюта {currencyDto.BrifName} в БД");
                    }
                    else
                    {
                        Console.WriteLine($"Валюта {currency.BrifName} уже имеется в БД");
                    }

                    unitOfWork.Complete();
                    }
                }
            }

        }
    }
}
