﻿using System;
using System.Collections.Generic;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Dependency;
using JetBrains.Annotations;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Dto.Pair;
using MyExchange.Api.Dto.PairInfoOnExchange;
using MyExchange.Api.Dto.PairSettings;
using MyExchange.Api.Services.Bl;
using MyExchange.Api.Services.Crud;

namespace MyExchange.BackgroundJobs
{
    /// <summary>
    /// Фоновая задача на считывание из биржи новых валютных пар
    /// </summary>
    public class ReadPairsJob : BackgroundJob<Guid>, ITransientDependency
    {
        private readonly IPairCrudAppService _pairCrudService;
        private readonly IPairRepository _pairRepository;
        private readonly IPairInfoOnExchangeRepository _pairInfoOnExchangeRepository;
        private readonly ICurrencyRepository _currencyRepository;
        private readonly IExchangeProvider _exchangeProvider;
        private readonly IPairInfoOnExchangeCrudAppService _pairInfoOnExchangeCrudAppService;
        private object _locker = new object();


        public ReadPairsJob([NotNull] IPairCrudAppService pairCrudService,
                            [NotNull] IPairRepository pairRepository,
                            [NotNull] IExchangeProvider exchangeProvider,
                            [NotNull] IPairInfoOnExchangeCrudAppService pairInfoOnExchangeCrudAppService,
                            [NotNull] ICurrencyRepository currencyRepository,
                            [NotNull] IPairInfoOnExchangeRepository pairInfoOnExchangeRepository)
        {
            _exchangeProvider = exchangeProvider ?? throw new ArgumentNullException(nameof(exchangeProvider));
            _pairInfoOnExchangeCrudAppService = pairInfoOnExchangeCrudAppService ?? throw new ArgumentNullException(nameof(pairInfoOnExchangeCrudAppService));
            _currencyRepository = currencyRepository ?? throw new ArgumentNullException(nameof(currencyRepository));
            _pairInfoOnExchangeRepository = pairInfoOnExchangeRepository ?? throw new ArgumentNullException(nameof(pairInfoOnExchangeRepository));
            _pairCrudService = pairCrudService ?? throw new ArgumentNullException(nameof(pairCrudService));
            _pairRepository = pairRepository ?? throw new ArgumentNullException(nameof(pairRepository));
        }

        public override void Execute(Guid exchangeId)
        {
            var exchange =  _exchangeProvider.Get(exchangeId);

            IEnumerable<PairSettings> pairSettings = exchange.ExchangeDriver.PairSettingsAsync().Result;

            foreach (var pairSetting in pairSettings)
            {
                using (var unitOfWork = UnitOfWorkManager.Begin())
                {
                    //Изменить условие поиска пары по наименованиям валют
                    var pair = _pairRepository.FirstOrDefault(p => p.Id == pairSetting.PairId);
                    

                    if (pair == null)
                    {//Создается валютная пара если она отсутсвует в БД

                        var currency1 = _currencyRepository.FirstOrDefault(
                            cur => cur.BrifName.ToUpper() == pairSetting.Pair.Currency1.BrifName.ToUpper());
                        var currency2 = _currencyRepository.FirstOrDefault(
                            cur => cur.BrifName.ToUpper() == pairSetting.Pair.Currency2.BrifName.ToUpper());

                        if (currency1 == null || currency2 == null)
                        {
                            Logger.Debug("При сохранении валютной пары не обнаружилась валюта, сохраненная в БД");
                            continue;
                        }

                        pairSetting.Pair.Currency1 = currency1.MapTo<CurrencyDto>();
                        pairSetting.Pair.Currency2 = currency2.MapTo<CurrencyDto>();

                        PairDto pairDto;

                        lock (_locker)
                        {
                             pairDto = _pairCrudService.Create(pairSetting.Pair.MapTo<PairDto>()).Result;
                        }
                       
                        pair = pairDto.MapTo<Api.Entity.Pair>();

                        Console.WriteLine($"Добавлена новая валютная пара {pairDto.ToString()} в БД");
                    }

                    var pairInfoOnExchange = _pairInfoOnExchangeRepository
                            .FirstOrDefault(p => p.PairId == pair.Id && p.ExchangeId == exchangeId);
                    
                    if (pairInfoOnExchange == null)
                    {
                        lock (_locker)
                        {
                            var pairOnEchangeDto = _pairInfoOnExchangeCrudAppService.Create(new PairInfoOnExchangeDto
                            {
                                PairId = pair.Id,
                                ExchangeId = exchangeId
                            }).Result;
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Валютная пара {pair.ToString()} уже имеется в БД");
                    }

                    unitOfWork.Complete();
                }
            }

        }
    }
}
