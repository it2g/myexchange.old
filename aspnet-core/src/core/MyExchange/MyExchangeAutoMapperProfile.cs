﻿using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Dto.CurrencyUserBalance;
using MyExchange.Api.Dto.ExchangeUser;
using MyExchange.Api.Dto.OpenedOrder;
using MyExchange.Api.Dto.Order;
using MyExchange.Api.Dto.Pair;
using MyExchange.Api.Dto.PairInfoOnExchange;
using MyExchange.Api.Dto.PairSettings;
using MyExchange.Api.Dto.Trade;
using MyExchange.Api.Dto.TradeExpertAdvice;
using MyExchange.Api.Dto.TradeExpertLiquidity;
using MyExchange.Api.Entity;

namespace MyExchange
{
    public class MyExchangeAutoMapperProfile : AutoMapper.Profile
    {
        public MyExchangeAutoMapperProfile()
        {
            CreateMap<CreateCurrencyDto, CurrencyDto>();
            CreateMap<CurrencyDto, Currency>().ReverseMap();

            CreateMap<CurrencyUserBalance, CurrencyUserBalanceDto>().ReverseMap();

            CreateMap<CreateExchangeUserDto, ExchangeUserDto>();
            CreateMap<ExchangeUserDto, ExchangeUser>().ReverseMap();

            CreateMap<CreateOrderDto, OrderDto>();
            CreateMap<OrderDto, Order>().ReverseMap();

            CreateMap<CreateOpenedOrderDto, OpenedOrderDto>().ReverseMap();
            CreateMap<UpdateOpenedOrderDto, OpenedOrderDto>();
            CreateMap<OpenedOrderDto, OpenedOrder>().ReverseMap();

            
            CreateMap<CreatePairDto, PairDto>()
                .ForMember(dto => dto.Name, x =>x.MapFrom(e=> ""));
            CreateMap<PairDto, Pair>();
            CreateMap<Pair, PairDto>()
                .ForMember(dto => dto.Name, x=> x.MapFrom(entity => $"{entity.Currency1.BrifName}/{entity.Currency2.BrifName}") );

            CreateMap<CreatePairInfoOnExchangeDto, PairInfoOnExchangeDto>();
            CreateMap<PairInfoOnExchangeDto, PairInfoOnExchange>().ReverseMap();

            CreateMap<PairSettings, PairSettings>().ReverseMap();

            CreateMap<TradeDto, Trade>().ReverseMap();

            CreateMap<CreateTradeExpertAdviceDto, TradeExpertAdviceDto>();
            CreateMap<TradeExpertAdviceDto, TradeExpertAdvice>().ReverseMap();

            CreateMap<CreateTradeExpertLiquidityDto, TradeExpertLiquidityDto>();
            CreateMap<TradeExpertLiquidityDto, TradeExpertLiquidity>().ReverseMap();
        }
    }
}
