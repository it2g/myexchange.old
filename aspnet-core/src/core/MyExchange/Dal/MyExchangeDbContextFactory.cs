﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using MyExchange.Configuration;
using MyExchange.Web;

namespace MyExchange.Dal
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class MyExchangeDbContextFactory : IDesignTimeDbContextFactory<MyExchangeDbContext>
    {
        public MyExchangeDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<MyExchangeDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            MyExchangeDbContextConfigurer.Configure(builder, configuration.GetConnectionString(MyExchangeConsts.ConnectionStringName));

            return new MyExchangeDbContext(builder.Options);
        }
    }
}
