﻿using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.Configuration;
using Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Entity;
using MyExchange.Api.MyExchange;
using MyExchange.Authorization.Users;
using MyExchange.Dal.EfConfigurations;
using MyWallet.Dal.EfConfigurations;

namespace MyExchange.Dal
{
    public class MyExchangeDbContext : AbpDbContext
    {
        public virtual DbSet<Pair> Pairs { get; set; }
        public virtual DbSet<Currency> Currencies { get; set; }
        public virtual DbSet<ExchangeUser> ExchangeUsers { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OpenedOrder> OpenedOrders { get; set; }
        public virtual DbSet<PairInfoOnExchange> PairInfoOnExchanges { get; set; }
        public virtual DbSet<Trade> Trades { get; set; }
        public virtual DbSet<TradeExpertAdvice> TradeExpertAdvices { get; set; }
        public virtual DbSet<TradeExpertLiquidity> TradeExpertLiquidities { get; set; }
        public virtual DbSet<Wallet> Wallets { get; set; }

        public MyExchangeDbContext(DbContextOptions<MyExchangeDbContext> options)
            : base(options)
        {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<ApplicationLanguageText>()
            //    .Property(p => p.Value)
            //    .HasMaxLength(10485759); // any integer that is smaller than 10485760
            base.OnModelCreating(modelBuilder);
            //https://www.npgsql.org/efcore/value-generation.html - для автогенерации Id типа Guid
            modelBuilder.HasPostgresExtension("uuid-ossp");

            modelBuilder.ApplyConfiguration(new ExchangeUserMap());
            modelBuilder.ApplyConfiguration(new CurrencyMap());
            modelBuilder.ApplyConfiguration(new OrderMap());
            modelBuilder.ApplyConfiguration(new OpenedOrderMap());
            modelBuilder.ApplyConfiguration(new PairInfoOnExchangeMap());
            modelBuilder.ApplyConfiguration(new PairMap());
            modelBuilder.ApplyConfiguration(new TradeExpertAdviceMap());
            modelBuilder.ApplyConfiguration(new TradeExpertLiquidityMap());
            modelBuilder.ApplyConfiguration(new TradeMap());
            modelBuilder.ApplyConfiguration(new WalletMap());


            modelBuilder.Ignore<User>();
            modelBuilder.Ignore<PermissionSetting>();
            modelBuilder.Ignore<UserPermissionSetting>();
            modelBuilder.Ignore<RolePermissionSetting>();
            modelBuilder.Ignore<Setting>();
            modelBuilder.Ignore<UserClaim>();
            modelBuilder.Ignore<UserLogin>();
            modelBuilder.Ignore<UserRole>();
            modelBuilder.Ignore<UserToken>();

            modelBuilder.Ignore<Exchange>();
            modelBuilder.Ignore<IExchangeDriver>();
            modelBuilder.Ignore<ITradingStrategy>();
            modelBuilder.Ignore<IBotTactic>();
            modelBuilder.Ignore<BotTacticContextBase>();
            







        }
    }
}
