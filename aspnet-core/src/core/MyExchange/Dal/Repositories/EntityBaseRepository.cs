﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.EntityFrameworkCore;
using Abp.EntityFrameworkCore.Repositories;
using MyExchange.Api.Dal;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.Repositories
{
   public class EntityBaseRepository : EfCoreRepositoryBase<MyExchangeDbContext, EntityBase, Guid>, IEntityBaseRepository
    {
        public EntityBaseRepository(IDbContextProvider<MyExchangeDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
