﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.EntityFrameworkCore;
using Abp.EntityFrameworkCore.Repositories;
using MyExchange.Api.Dal;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.Repositories
{
   public class TradeExpertLiquidityRepository : EfCoreRepositoryBase<MyExchangeDbContext, TradeExpertLiquidity, Guid>, ITradeExpertLiquidityRepository
    {
        public TradeExpertLiquidityRepository(IDbContextProvider<MyExchangeDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
