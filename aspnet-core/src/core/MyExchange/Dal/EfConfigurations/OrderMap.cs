﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.Dal.EfConfigurations;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.EfConfigurations
{
    public class OrderMap : BaseEntityMap<Order>
    {
        public override void Configure(EntityTypeBuilder<Order> builder)
        {
            base.Configure(builder);
            builder.Property(u => u.Date).HasColumnName("date").IsRequired();
            builder.Property(u => u.OrderStatus).IsRequired().HasColumnName("order_status");
            builder.Property(u => u.Price).IsRequired().HasColumnName("price");
            builder.Property(u => u.Quantity).HasColumnName("quantity").IsRequired();
            builder.Property(u => u.TradeTypes).HasColumnName("trade_type").IsRequired();
            builder.Property(u => u.PairId).HasColumnName("pair_id").IsRequired();
            builder.Property(x => x.ExchangeId).HasColumnName("exchange_id");
            builder.Property(x => x.IsDeleted).HasColumnName("is_deleted");

            builder.HasOne(x => x.Pair).WithMany().HasForeignKey(x => x.PairId).IsRequired();

            builder.ToTable("orders", "core");
        }
    }
}
