﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.Dal.EfConfigurations;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.EfConfigurations
{
    public class TradeMap : BaseEntityMap<Trade>
    {
        public override void Configure(EntityTypeBuilder<Trade> builder)
        {
            base.Configure(builder);
            builder.Property(u => u.Amount).HasColumnName("amount").IsRequired();
            builder.Property(u => u.Date).HasColumnName("date").IsRequired();
            builder.Property(u => u.Price).HasColumnName("price").IsRequired();
            builder.Property(u => u.Quantity).HasColumnName("quantity").IsRequired();
            builder.Property(u => u.TradeId).HasColumnName("trade_id").IsRequired();
            builder.Property(u => u.OrderId).IsRequired(false).HasColumnName("order_id");
            builder.Property(u => u.TradeTypes).HasColumnName("trade_type").IsRequired();
            builder.Property(u => u.ExchangeId).HasColumnName("exchange_id").IsRequired();
            builder.HasOne(x => x.Order).WithMany().HasForeignKey(x => x.OrderId).IsRequired(false);

            builder.Property(u => u.PairId).HasColumnName("pair_id").IsRequired();
            builder.HasOne(u => u.Pair).WithMany().HasForeignKey(x => x.PairId).IsRequired();

            builder.HasIndex(u => u.TradeId);
            builder.HasIndex(u => u.ExchangeId);

            builder.ToTable("trades", "core");
        }
    }
}
