﻿using System;
using JetBrains.Annotations;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Constants;
using MyExchange.Api.Entity;
using MyExchange.Api.MyExchange;

namespace MyExchange.Base.BotTactic
{
    /// <summary>
    /// Описание класса и его публичных методов смотри в интерфейсе
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    public abstract class TradeBotTacticBase<TContext> : BotTacticBase<TContext>, ITradingBotTactic
        where TContext : TradingBotTacticContextBase
    {
        /// <summary>
        /// Контролер расходования и возврата денежных средств
        /// </summary>
        protected IAccountant Accountant { get; }

        public TradeBotTacticBase([NotNull] IAccountant accountant) 
        {
            if (accountant == null) throw new ArgumentNullException(nameof(accountant));
            Accountant = accountant;
        }
        
        public virtual void ChangeCurrencyValue(Currency currency, decimal value, string purpose)
        {
            Context.CurrencyValue = Accountant.GetMoney(currency, value, purpose);
            ///TODO Зафиксировать в БД изменения в распределении средств.
            ///Возможно что это лучше делать "Бухгалтеру"
        }

        public virtual string SetOrder(Pair pair, decimal value, decimal rate, TradeTypes tradeTypes)
        {
            throw new NotImplementedException();
        }

        public virtual  bool CancelOrder(Guid orderId)
        {
            throw new NotImplementedException();
        }
    }
}
