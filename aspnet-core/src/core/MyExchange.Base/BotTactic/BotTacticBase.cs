﻿using System;
using Abp.Application.Services;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Entity;

namespace MyExchange.Base.BotTactic
{
    /// <summary>
    /// Описание класса и его публичных методов смотри в интерфейсе
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    public abstract class BotTacticBase<TContext> : ApplicationService, IBotTactic<TContext>
        where TContext : BotTacticContextBase
    {
        public abstract Guid Id { get; }

        public TContext Context { get; set; }

        public abstract string Name { get; }

        public abstract Action ExecuteTactic { get; }

        public abstract void Dispose();
    }
}
