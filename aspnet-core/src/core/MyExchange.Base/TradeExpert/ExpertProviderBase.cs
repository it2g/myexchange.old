﻿using System.Collections.Generic;
using Abp.Application.Services;
using MyExchange.Api.TradeExpert;

namespace MyExchange.Web.TradeExpert
{
   public abstract class ExpertProviderBase : ApplicationService, IExpertProvider
   {
       public abstract IList<ITradeAdviser> GetExperts(IDictionary<string, object> parametrs);

   }
}
