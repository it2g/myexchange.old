﻿using System.Collections.Generic;
using System.Linq;
using Abp.Dependency;
using MyExchange.Api.Constants;
using MyExchange.Api.TradeExpert;

namespace MyExchange.Web.TradeExpert
{
    /// <summary>
    /// Простейший поставщик экспертов тля торговых тактик
    /// </summary>
   public class SimpleExpertProvider : ExpertProviderBase
   {
       private readonly IIocResolver _iocResolver;

       public SimpleExpertProvider(IIocResolver iocResolver)
       {
           _iocResolver = iocResolver;
       }
        /// <summary>
        /// По переданным параметрам производится подбор и фильтрация экспертов.
        /// </summary>
        /// <param name="parametrs"></param>
        /// <returns></returns>
       public override IList<ITradeAdviser> GetExperts(IDictionary<string, object> parametrs)
       {
            var experts = _iocResolver.ResolveAll<ITradeAdviser>().ToList();

             object tradingTypeParam = null;

            parametrs.TryGetValue("TradingType", out tradingTypeParam);

           if (tradingTypeParam != null)
           {
               var tradingType =  (TradingTypes)tradingTypeParam;

                //здесь нужно подобрать подходящих экспертов для текущей тактики
                experts = experts.Where(e => e.TradingTypeList.Any(x => x == tradingType)).ToList();

           }
   
            return experts;
        }
    }
}
