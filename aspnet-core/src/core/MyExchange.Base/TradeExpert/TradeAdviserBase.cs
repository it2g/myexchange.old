﻿using System.Collections.Generic;
using System.Threading.Tasks;
using JetBrains.Annotations;
using MyExchange.Api.Constants;
using MyExchange.Api.Entity;
using MyExchange.Api.TradeExpert;
using MyExchange.Web.TradeExpert;

namespace MyExchange.Base.TradeExpert
{
    /// <summary>
    /// Базовый класс для экспертов дающих советы по запросам тактик торговли
    /// </summary>
    public abstract class TradeAdviserBase<TContext> : TradeExpertBase<TContext>, ITradeAdviser
        where TContext : TradeExpertContextBase
    {
        public virtual IList<TradingTypes> TradingTypeList { get; }

        protected TradeAdviserBase([NotNull] TContext context) : base(context)
        {
            TradingTypeList = new List<TradingTypes>();
        }
        public abstract Task<TradeExpertAdvice> GetAdviceAsync();
    }
}
