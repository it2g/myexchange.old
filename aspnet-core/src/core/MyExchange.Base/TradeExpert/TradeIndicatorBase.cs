﻿using System;
using JetBrains.Annotations;
using MyExchange.Api.TradeExpert;
using MyExchange.Web.TradeExpert;

namespace MyExchange.Base.TradeExpert
{
    /// <summary>
    /// Базовый класс для экспертов работающих постоянно в фоновом режиме
    /// </summary>
    public abstract class TradeIndicatorBase<TContext> : TradeExpertBase<TContext>, ITradeIndicator
        where TContext : TradeExpertContextBase
    {
        protected TradeIndicatorBase([NotNull] TContext context) : base(context)
        {
        }

        public abstract Action ExecuteIndicator { get; }
    }
}
