﻿using System;
using Abp.Application.Services;
using JetBrains.Annotations;
using MyExchange.Api.TradeExpert;

namespace MyExchange.Web.TradeExpert
{
    public abstract class TradeExpertBase<TContext> : ApplicationService, ITradeExpert
        where TContext : TradeExpertContextBase
    {
        protected TContext Context;

        public abstract Guid Id { get; protected set; }

        protected TradeExpertBase([NotNull] TContext context)
        {
            Context = context ?? throw new ArgumentNullException(nameof(context));
        }

      

    }

   
}
