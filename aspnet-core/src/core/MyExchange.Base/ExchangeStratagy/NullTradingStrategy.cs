﻿using System;
using JetBrains.Annotations;
using MyExchange.Api.Services.Bl;

namespace MyExchange.Web.ExchangeStratagy
{
    /// <summary>
    /// К этой стратегии прикрепляются боты, котрые работают автономно без управления
    /// какой либо значимой стратегией.
    /// </summary>
    public class NullTradingStrategy : TradingStrategyBase
    {
        //public NullTradingStrategy([NotNull] IBotManager botManager) : base(botManager)
        //{ }

        public override Action ExecuteStratagy { get; protected set; } = Implementation;

        private static void Implementation()
        {
            ///Логика управления ботами для достижения цели стратегии
            ///Она должна быть очень проста: запускаются все боты и работают автономно

        }
    }
}
