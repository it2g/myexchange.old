﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Entity;
using MyExchange.Api.MyExchange;
using MyExchange.Api.Services.Bl;

namespace MyExchange.Web.ExchangeStratagy
{
    /// <summary>
    /// Базовая стратегия автоматизированной торговли на биржах, применяемая системой
    /// </summary>
    public abstract class TradingStrategyBase : EntityBase, ITradingStrategy
    {
        /// <summary>
        /// Используется в стратегии торговли для управления ботами на основе анализа 
        /// продуктивности их работы
        /// </summary>
        //protected IBotManager _botManager;

        //public TradingStrategyBase([NotNull] IBotManager botManager)
        //{
        //    _botManager = botManager ?? throw new ArgumentNullException(nameof(botManager));
        //}

        public abstract Action ExecuteStratagy { get; protected set; }

        public IEnumerable<ITradingBotTactic> Bots { get; }
        //{
        //    get { return _botManager.GetBots(new BotFilter(){MyExchangeStrategy = this}); }
        //}

        
    }
}
