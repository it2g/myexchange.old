﻿using System;
using Abp.Application.Services;
using JetBrains.Annotations;
using MyExchange.Api.Services.Bl;

namespace MyExchange.Web.Exchange
{
    public abstract class ExchangeDriverBase : ApplicationService
    {
        protected IExchangeUserKeyProvider ExchangeUserKeyProvider { get; }

        protected ExchangeDriverBase([NotNull] IExchangeUserKeyProvider exchangeUserKeyProvider)
        {
            ExchangeUserKeyProvider = exchangeUserKeyProvider ?? throw new ArgumentNullException(nameof(exchangeUserKeyProvider));
        }

      
        /// <summary>
        /// С бирж приходят сведения о датах в различных исчислениях и их нужно приводить
        /// к DateTime. На разных биржах, это могут быть разные приведения.
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        protected virtual DateTime ConvertDateTime(long datetime)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(datetime);
        }
    }
}
