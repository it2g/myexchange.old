﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using JetBrains.Annotations;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Constants;
using MyExchange.Api.Entity;
using MyExchange.Api.MyExchange;
using MyExchange.Api.Services.Bl;
using MyExchange.Api.Services.Crud;

namespace MyExchange.Base.Services
{
    /// <summary>
    /// Управляет ботами: запускает, останавливает, продолжает работы и т.д.
    /// </summary>
    public abstract  class BotManagerBase<TBotTactic, TBotTacticContext> : ApplicationService, IBotManager<TBotTactic, TBotTacticContext>
        where TBotTacticContext : BotTacticContextBase
        where TBotTactic : IBotTactic<TBotTacticContext>
    {
        protected IBotTacticProvider _botTacticProvider;
        protected IExchangeProvider _exchangeProvider;
        protected IBackgroundJobManager BackgroundJobManager;
        protected IRepository<TBotTacticContext, Guid> _contextRepository;

        protected BotManagerBase([NotNull]  IBotTacticProvider botTacticProvider,
            [NotNull] IExchangeProvider exchangeProvider,
            [NotNull] IBackgroundJobManager backgroundJobManager,
            [NotNull] IRepository<TBotTacticContext, Guid> contextRepository)
        {
            _exchangeProvider = exchangeProvider ?? throw new ArgumentNullException(nameof(exchangeProvider));
            _botTacticProvider = botTacticProvider ?? throw new ArgumentNullException(nameof(botTacticProvider));
            BackgroundJobManager = backgroundJobManager ?? throw new ArgumentNullException(nameof(backgroundJobManager));
            _contextRepository = contextRepository ?? throw new ArgumentNullException(nameof(contextRepository));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="botTactic"></param>
        /// <param name="exchangeStrategy">стратегия, котрая запускает бота</param>
        public virtual async Task StartBot(TBotTactic botTactic)
        {
            botTactic.Context.Status = BotStatuses.Work;
            botTactic.Context.StartTime = DateTime.UtcNow;

            await _contextRepository.InsertOrUpdateAsync(botTactic.Context);

            new Task(() => botTactic.ExecuteTactic()).Start();
        }

        
        public virtual async Task StopBot(TBotTactic bot)
        {
            bot.Dispose();

            bot.Context.Status = BotStatuses.Stoped;

            await _contextRepository.UpdateAsync(bot.Context);
        }

        public virtual async Task PauseBot(TBotTactic bot)
        {
            /// 1. Все установленные заказы на покупку продажу на бирже остаются
            ///но бот уже не сможет делать новые заказы
 

           //2. Удалить бота из очереди исполнения задач

            /// 3. Изменить статус бота и произвести запись этих изменений в БД
            bot.Context.Status = BotStatuses.OnPause;

            await _contextRepository.UpdateAsync(bot.Context);
        }

        /// <summary>
        /// Продолжение работы приостановленного ранее бота
        /// </summary>
        /// <param name="bot"></param>
        public virtual async Task ResumeBot(TBotTactic bot)
        {
            /// 1. Все установленные заказы на покупку продажу на бирже остаются
            ///но бот уже не сможет делать новые заказы


            //2. Постановка в очередь исполнения задач
            var botTactics = _botTacticProvider.CreateBotTactic(bot.GetType(), bot.Context);

            if (bot.Context.StartTime == null)
            {
               //  _queryManager.Enqueue(() => botTactics.ExecuteTactic());
            }
            else
            {
               // _queryManager.Schedule(() => botTactics.ExecuteTactic(), DateTime.Now - (DateTime)botTactic.StartTime);
            }

            /// 3. Изменить статус бота и произвести запись этих изменений в БД
            bot.Context.Status = BotStatuses.Work;

            await _contextRepository.UpdateAsync(bot.Context);
        }

        public IList<TBotTactic> RestoreBots()
        {
            throw new NotImplementedException();
        }

        public virtual Task ChangeValue(TBotTactic bot, decimal value)
        {
            throw new NotImplementedException();
        }

    }
}
