﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using System.Threading.Tasks;
using MyExchange.Api.Services.Crud;

namespace MyExchange.Web.Services
{
    public class MyExchangeCrudServiceBase<TEntity, TEntityDto, TGetAllInput, TPrimaryKey> : AsyncCrudAppService<TEntity, TEntityDto, TPrimaryKey, TGetAllInput, TEntityDto, TEntityDto, TEntityDto, TEntityDto>,
        IExhchangeCrudAppServices<TEntityDto, TPrimaryKey,  TGetAllInput, TEntityDto, TEntityDto, TEntityDto, TEntityDto>
        where TEntity : class, IEntity<TPrimaryKey>
        where TEntityDto : IEntityDto<TPrimaryKey>, new()

    {
        public MyExchangeCrudServiceBase(IRepository<TEntity, TPrimaryKey> repository) : base(repository)
        {
        }

        public virtual async  Task<TEntityDto> Get(TPrimaryKey id)
        {

           return await  Get(new TEntityDto() {Id = id});
        }
        public virtual async Task Delete(TPrimaryKey id)
        {
            await Delete(new TEntityDto() {Id = id});
        }

    }
}
