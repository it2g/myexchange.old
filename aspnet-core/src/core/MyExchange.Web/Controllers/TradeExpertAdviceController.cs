﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using MyExchange.Services;
using System;
using System.Threading.Tasks;
using Abp.AutoMapper;
using MyExchange.Api.Dto.TradeExpertAdvice;

namespace MyExchange.Web.Controllers
{
    [Route("api/[controller]")]
    public class TradeExpertAdviceController : MyExchangeControllerBase<TradeExpertAdviceCrudAppService>
    {
        public TradeExpertAdviceController(TradeExpertAdviceCrudAppService crudService) : base(crudService)
        {
        }

        [HttpGet]
        public async Task<PagedResultDto<TradeExpertAdviceDto>> Get()
        {

            return await CrudService.GetAll(new TradeExpertAdvicePagedResultRequestDto());

        }

        [HttpGet("{id}")]
        public async Task<TradeExpertAdviceDto> Get([FromRoute]Guid id)
        {
            return await CrudService.Get(id);
        }

        [HttpPost]
        public async Task<TradeExpertAdviceDto> Post([FromBody]CreateTradeExpertAdviceDto input)
        {
            var dto = await CrudService.Create(input.MapTo<TradeExpertAdviceDto>());

            return dto;
        }

        [HttpPut("{id}")]
        public async Task<TradeExpertAdviceDto> Put([FromRoute] Guid id, [FromBody]  TradeExpertAdviceDto input)
        {

            var dto = await CrudService.Get(id);

            dto = await CrudService.Update(ObjectMapper.Map(input, dto));

            return dto;
        }

        [HttpDelete("{id}")]
        public async Task Delete([FromRoute] Guid id)
        {
            await CrudService.Delete(id);
        }
    }
}
