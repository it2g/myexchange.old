﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using JetBrains.Annotations;
using MyExchange.Api.Dto.Pair;
using MyExchange.Api.Services.Crud;
using MyExchange.BackgroundJobs;

namespace MyExchange.Web.Controllers
{
    [Route("api/[controller]")]
    public class PairController : MyExchangeControllerBase<IPairCrudAppService>
    {
        private IBackgroundJobManager _backgroundJobManager;

        public PairController(IPairCrudAppService service,
            [NotNull] IBackgroundJobManager backgroundJobManager) : base(service)
        {
            _backgroundJobManager = backgroundJobManager ?? throw new ArgumentNullException(nameof(backgroundJobManager));
        }

        [HttpGet]
        public async Task<PagedResultDto<PairDto>> Get(PairPagedResultRequestDto input)
        {
            return await CrudService.GetAll(input);
        }

        [HttpGet("{id}")]
        public async Task<PairDto> Get([FromRoute]Guid id)
        {
            return await CrudService.Get(id);
        }

        [HttpPost]
        public async Task<PairDto> Post([FromBody] CreatePairDto input)
        {
            var dto = await CrudService.Create(input.MapTo<PairDto>());

            return dto;
        }

       
        [HttpDelete("{id}")]
        public async Task Delete([FromRoute] Guid id)
        {
            await CrudService.Delete(id);
        }

        /// <summary>
        /// Загрузка валютных пар с биржи в БД
        /// </summary>
        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> LoadFromExchange([FromBody] Guid exchangeId)
        {
            if (exchangeId == Guid.Empty) return NotFound(new Exception("Ошибочный идентификатор биржи"));

            await _backgroundJobManager.EnqueueAsync<ReadPairsJob, Guid>(exchangeId);

            return Ok();
        }
    }
}
