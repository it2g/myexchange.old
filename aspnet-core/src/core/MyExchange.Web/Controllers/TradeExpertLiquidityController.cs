﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using MyExchange.Services;
using System;
using System.Threading.Tasks;
using Abp.AutoMapper;
using MyExchange.Api.Dto.TradeExpertLiquidity;

namespace MyExchange.Web.Controllers
{
    [Route("api/[controller]")]
    public class TradeExpertLiquidityController : MyExchangeControllerBase<TradeExpertLiquidityCrudAppService>
    {
        public TradeExpertLiquidityController(TradeExpertLiquidityCrudAppService crudService) : base(crudService)
        {
        }

        [HttpGet]
        public async Task<PagedResultDto<TradeExpertLiquidityDto>> Get()
        {

            return await CrudService.GetAll(new TradeExpertLiquidityPagedResultRequestDto());

        }

        [HttpGet("{id}")]
        public async Task<TradeExpertLiquidityDto> Get([FromRoute]Guid id)
        {
            return await CrudService.Get(id);
        }

        [HttpPost]
        public async Task<TradeExpertLiquidityDto> Post([FromBody]CreateTradeExpertLiquidityDto input)
        {
            var dto = await CrudService.Create(input.MapTo<TradeExpertLiquidityDto>());

            return dto;
        }

        [HttpPut("{id}")]
        public async Task<TradeExpertLiquidityDto> Put([FromRoute] Guid id,  [FromBody]TradeExpertLiquidityDto input)
        {

            var dto = await CrudService.Get(id);

            dto = await CrudService.Update(ObjectMapper.Map(input, dto));

            return dto;
        }

        [HttpDelete("{id}")]
        public async Task Delete([FromRoute] Guid id)
        {
            await CrudService.Delete(id);
        }
    }
}
