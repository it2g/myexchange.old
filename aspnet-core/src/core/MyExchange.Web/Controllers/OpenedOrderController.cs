﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using MyExchange.Web.Controllers;
using System;
using System.Threading.Tasks;
using Abp.AutoMapper;
using MyExchange.Api.Dto.OpenedOrder;
using MyExchange.Api.Services.Crud;

namespace MyExchange.Web
{
    [Route("api/[controller]")]
    public class OpenedOrderController : MyExchangeControllerBase<IOpenedOrderCrudAppService>
    {
        public OpenedOrderController(IOpenedOrderCrudAppService service) : base(service)
        {
        }

        [HttpGet]
        public async Task<PagedResultDto<OpenedOrderDto>> Get()
        {
            var dto = await CrudService.GetAll(new OpenedOrderPagedResultRequestDto());

            return dto;
        }

        [HttpGet("{id}")]
        public async Task<OpenedOrderDto> Get([FromRoute]Guid id)
        {
           // var dto = 

            return await CrudService.Get(id);
        }

        [HttpPost]
        public async Task<OpenedOrderDto> Post([FromBody] CreateOpenedOrderDto input)
        {
            var dto = await CrudService.Create(input.MapTo<OpenedOrderDto>());

            return dto;
        }

        [HttpPut("{id}")]
        public async Task<OpenedOrderDto> Put([FromRoute] Guid id, [FromBody] UpdateOpenedOrderDto input)
        {
            var dto = await CrudService.Get(id);

            dto = await CrudService.Update(ObjectMapper.Map(input, dto));

            return dto;
        }

        [HttpDelete("{id}")]
        public async Task Delete([FromRoute] Guid id)
        {

            await CrudService.Delete(id);
        }

        

    }
}
