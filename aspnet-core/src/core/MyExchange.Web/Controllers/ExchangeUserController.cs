﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Abp.AutoMapper;
using MyExchange.Api.Dto.ExchangeUser;
using MyExchange.Api.Services.Crud;

namespace MyExchange.Web.Controllers
{
    [Route("api/[controller]")]
    public class ExchangeUserController : MyExchangeControllerBase<IExchangeUserCrudAppService>
    {
        public ExchangeUserController(IExchangeUserCrudAppService service) : base(service)
        {
        }

        [HttpGet]
        public async Task<PagedResultDto<ExchangeUserDto>> Get([FromRoute]ExchangeUserPagedResultRequestDto input)
        {

            return await CrudService.GetAll(input);

        }

        [HttpGet("{id}")]
        public async Task<ExchangeUserDto> Get([FromRoute]Guid id)
        {
            return await CrudService.Get(id);
        }

        [HttpPost]
        public async Task<ExchangeUserDto> Post([FromBody]CreateExchangeUserDto input)
        {
            var dto = await CrudService.Create(input.MapTo<ExchangeUserDto>());

            return dto;
        }

        [HttpPut("{id}")]
        public async Task<ExchangeUserDto> Put([FromRoute] Guid id, [FromBody] ExchangeUserDto input)
        {

            var dto = await CrudService.Get(id);

            dto = await CrudService.Update(ObjectMapper.Map(input, dto));

            return dto;
        }

        [HttpDelete("{id}")]
        public async Task Delete([FromRoute] Guid id)
        {
            await CrudService.Delete(id);
        }
    }
}
