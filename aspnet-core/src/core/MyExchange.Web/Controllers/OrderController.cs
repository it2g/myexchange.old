﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Abp.AutoMapper;
using MyExchange.Api.Dto.Order;
using MyExchange.Api.Services.Crud;

namespace MyExchange.Web.Controllers
{
    [Route("api/[controller]")]
    public class OrderController : MyExchangeControllerBase<IOrderCrudAppService>
    {
        public OrderController(IOrderCrudAppService service) : base(service)
        {
        }

        [HttpGet()]
        public async Task<PagedResultDto<OrderDto>> Get([FromRoute]OrderPagedResultRequestDto input)
        {

            return await CrudService.GetAll(input);

        }

        [HttpGet("{id}")]
        public async Task<OrderDto> Get([FromRoute]Guid id)
        {
            return await CrudService.Get(id);
        }

        [HttpPost]
        public async Task<OrderDto> Post([FromBody]CreateOrderDto input)
        {
            var dto = await CrudService.Create(input.MapTo<OrderDto>());

            return dto;
        }

        [HttpPut("{id}")]
        public async Task<OrderDto> Put([FromRoute] Guid id, [FromBody] OrderDto input)
        {

            var dto = await CrudService.Get(id);

            dto = await CrudService.Update(ObjectMapper.Map(input, dto));

            return dto;
        }

        [HttpDelete("{id}")]
        public async Task Delete([FromRoute] Guid id)
        {
            await CrudService.Delete(id);
        }
    }
}
