﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using MyExchange.Services;
using System;
using System.Threading.Tasks;
using MyExchange.Api.Dto.Trade;

namespace MyExchange.Web.Controllers
{
    [Route("api/[controller]")]
    public class TradeController : MyExchangeControllerBase<TradeCrudAppService>
    {
        public TradeController(TradeCrudAppService crudService) : base(crudService)
        {
        }

        [HttpGet]
        public async Task<PagedResultDto<TradeDto>> Get(TradePagedResultRequestDto input)
        {

            return await CrudService.GetAll(input);
        }

        [HttpGet("{id}")]
        public async Task<TradeDto> Get([FromRoute]Guid id)
        {
            return await CrudService.Get(id);
        }

       
    }
}
