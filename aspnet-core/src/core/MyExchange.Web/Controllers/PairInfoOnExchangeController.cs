﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using MyExchange.Services;
using System;
using System.Threading.Tasks;
using Abp.AutoMapper;
using MyExchange.Api.Dto.PairInfoOnExchange;

namespace MyExchange.Web.Controllers
{
    [Route("api/[controller]")]
    public class PairInfoOnExchangeController : MyExchangeControllerBase<PairInfoOnExchangeCrudAppService>
    {
        public PairInfoOnExchangeController(PairInfoOnExchangeCrudAppService crudService) : base(crudService)
        {

        }

        [HttpGet]
        public async Task<PagedResultDto<PairInfoOnExchangeDto>> Get()
        {

            return await CrudService.GetAll(new PairInfoOnExchangePagedResultRequestDto());

        }

        [HttpGet("{id}")]
        public async Task<PairInfoOnExchangeDto> Get([FromRoute]Guid id)
        {
            return await CrudService.Get(id);
        }

        [HttpPost]
        public async Task<PairInfoOnExchangeDto> Post([FromBody] CreatePairInfoOnExchangeDto input)
        {
            var dto = await CrudService.Create(input.MapTo<PairInfoOnExchangeDto>());

            return dto;
        }

        [HttpPut("{id}")]
        public async Task<PairInfoOnExchangeDto> Put([FromRoute] Guid id,[FromBody]  PairInfoOnExchangeDto input)
        {

            var dto = await CrudService.Get(id);

            dto = await CrudService.Update(ObjectMapper.Map(input, dto));

            return dto;
        }

        [HttpDelete("{id}")]
        public async Task Delete([FromRoute] Guid id)
        {
            await CrudService.Delete(id);
        }
    }
}
