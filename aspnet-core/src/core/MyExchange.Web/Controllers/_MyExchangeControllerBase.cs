﻿using Abp.Application.Services;
using MyExchange.Controllers;
using MyExchange.Services;
using System;

namespace MyExchange.Web.Controllers
{
    public class MyExchangeControllerBase<TCrudService> : MyExchangeControllerBase
         where TCrudService : class, IApplicationService
    {
        protected readonly TCrudService CrudService;
        private CurrencyCrudAppService service;

        public MyExchangeControllerBase(TCrudService crudService)
        {
            CrudService = crudService ?? throw new ArgumentNullException(nameof(crudService));
        }

        public MyExchangeControllerBase(CurrencyCrudAppService service)
        {
            this.service = service;
        }
    }
}
