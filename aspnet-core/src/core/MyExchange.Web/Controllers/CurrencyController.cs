﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;
using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Services.Crud;
using MyExchange.BotTacticMonitoring.BackgroundJobs;

namespace MyExchange.Web.Controllers
{
    [Route("api/[controller]")]
    public class CurrencyController : MyExchangeControllerBase<ICurrencyCrudAppService>
    {
        private IBackgroundJobManager _backgroundJobManager;

        public CurrencyController(ICurrencyCrudAppService service, 
            [NotNull] IBackgroundJobManager backgroundJobManager) : base(service)
        {
            _backgroundJobManager = backgroundJobManager ?? throw new ArgumentNullException(nameof(backgroundJobManager));
        }

        [HttpGet]
        public async Task<PagedResultDto<CurrencyDto>> Get()
        {

            return await CrudService.GetAll(new CurrencyPagedResultRequestDto());

        }

        [HttpGet("{id}")]
        public async Task<CurrencyDto> Get([FromRoute]Guid id)
        {
            return await CrudService.Get(id);
        }

        [HttpPost]
        public async Task<CurrencyDto> Post(CreateCurrencyDto input)
        {
            var dto = await CrudService.Create(input.MapTo<CurrencyDto>());

            return dto;
        }

        [HttpPut("{id}")]
        public async Task<CurrencyDto> Put([FromRoute] Guid id, [FromBody] CurrencyDto input)
        {

            var dto = await CrudService.Get(id);

            dto = await CrudService.Update(ObjectMapper.Map(input, dto));

            return dto;
        }

        [HttpDelete("{id}")]
        public async Task Delete([FromRoute] Guid id)
        {
            await CrudService.Delete(id);

        }

        /// <summary>
        /// Загрузка валют с биржи в БД
        /// </summary>
        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> LoadFromExchange([FromBody] Guid exchangeId )
        {
            if (exchangeId == Guid.Empty) return NotFound(new Exception("Ошибочный идентификатор биржи")) ;

            await _backgroundJobManager.EnqueueAsync<ReadCurrenciesJob, Guid >(exchangeId);

            return Ok();
        }

    }
}
