﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MyExchange.MultiTenancy.Dto;

namespace MyExchange.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

