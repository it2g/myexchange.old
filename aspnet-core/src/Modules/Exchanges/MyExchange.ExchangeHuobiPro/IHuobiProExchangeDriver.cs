﻿using MyExchange.Api.MyExchange;

namespace MyExchange.ExchangeHuobiPro
{
    /// <summary>
    /// Пример расширения интерфейса драйвера, в том случае если биржа предоставляет
    /// дополнительный функционал, который не охвачен унифицированным интерфейсов
    /// для всех бирж
    /// </summary>
    public interface IHuobiProExchangeDriver : IExchangeDriver
    {
        string SpecificHuobiMethod();
    }
}
