﻿using System;
using MyExchange.Api.MyExchange;

namespace MyExchange.ExchangeHuobiPro
{
    public class HuobiProExchange: Exchange<IHuobiProExchangeDriver>
    {
        public override Guid Id { get; } = new Guid("CAC4273F-088F-4E78-9255-15F1A93C830A");

        public override string BriefName { get; } = "Huobi";

        public override string FullName { get; } = "Huobi";

        public override string Description { get; } = "";

        public HuobiProExchange(IHuobiProExchangeDriver driver) : base(driver)
        {
        }
    }
}
