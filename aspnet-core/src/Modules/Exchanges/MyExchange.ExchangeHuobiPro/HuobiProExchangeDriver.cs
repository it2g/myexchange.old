﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using MyExchange.Api.Constants;
using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Dto.Order;
using MyExchange.Api.Dto.Pair;
using MyExchange.Api.Dto.PairInfoOnExchange;
using MyExchange.Api.Dto.PairSettings;
using MyExchange.Api.Dto.Trade;
using MyExchange.Api.Entity;
using MyExchange.Api.MyExchange;
using MyExchange.Api.Services.Bl;
using MyExchange.Web.Exchange;
using Newtonsoft.Json;

namespace MyExchange.ExchangeHuobiPro
{
    /// <summary>
    /// Драйвер для работы с биржей Huobi.pro
    /// https://github.com/huobiapi/API_Docs_en/wiki/REST_Reference - документация по api
    /// https://huobiapi.github.io/docs/spot/v1/en/ - обновленная
    /// </summary>
    public class HuobiProExchangeDriver : ExchangeDriverBase, IHuobiProExchangeDriver
    {
        /// <summary>
        /// Клиент, с помощью которого делаются REST запросы на Api биржи
        /// </summary>
        private static readonly HttpClient Client = new HttpClient();

        public Exchange Exchange { private get; set; }

        /// <summary>
        /// Принятый на этой бирже разделитель валют в наименовании валютной пары
        /// На каждой бирже он может быть свой
        /// </summary>
        private const string PairSplitter = "";

        public HuobiProExchangeDriver(IExchangeUserKeyProvider exchangeUserKeyProvider) : base(exchangeUserKeyProvider)
        {
        }

        /// <summary>
        /// Адрес API биржи
        /// </summary>
        public string UrlApi { get; } = "https://api.huobi.pro";

#region Методы не требующие авторизации

        /// <summary>
        /// Запрос на список сделок по валютной паре
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<TradeDto>> TradesAsync(PairDto pair)
        {
            //TODO  Нужно этот хардкод заменить
            const int limit = 100;

            var stringPair = pair.GetSystemName(PairSplitter);

            HttpResponseMessage response = await Client.GetAsync($"{UrlApi}/market/history/trade?symbol={stringPair}&size={limit}");

            CheckResponse(response);

            var jsonTrades = await response.Content.ReadAsStringAsync();

            //Десерелизация сделок из json в .net структуру
            //https://www.newtonsoft.com/json/help/html/SerializingCollections.htm
            dynamic tradesOnPair = JsonConvert.DeserializeObject(jsonTrades);

            var trades = new List<TradeDto>();
            if (tradesOnPair == null)
            {
                Console.WriteLine($"Нет ни одной сделки по валютной паре {stringPair}");
            }
            else
            {
                //Перебрать все сделки по валютной паре
                foreach (dynamic element in tradesOnPair)
                {
                    if (element.Name == "status" || element.Name == "ch" || element.Name == "ts") continue;

                    foreach (dynamic list in element)
                    {
                        foreach (dynamic trade in list)
                        {
                            trades.Add(new TradeDto
                            {
                                TradeId = (string)trade["id"],
                                Price = trade.data[0]["price"],
                                PairId = pair.Id,
                                Amount = trade.data[0]["amount"],
                                Quantity = 0,
                                Date = ConvertDateTime((long)trade["ts"]),
                                ExchangeId = Exchange.Id
                            });
                        }
                    }
                }
            }

            return trades;
        }

        /// <summary>
        /// Запрос на списки сделок по валютным нескольким парам
        /// </summary>
        /// <param name="pairs"></param>
        /// <returns></returns>
        public async Task<IDictionary<PairDto, IEnumerable<TradeDto>>> TradesAsync(IList<PairDto> pairs)
        {
            // TODO НУЖНО НАД ЭТОЙ ХРЕНЬЮ ПОДУМАТЬ
            const int limit = 100;

            var trades = new Dictionary<PairDto, IEnumerable<TradeDto>>();

            foreach (var pair in pairs)
            {
                var stringPair = pair.GetSystemName(PairSplitter);

                HttpResponseMessage response = await Client.GetAsync($"{UrlApi}/market/history/trade?symbol={stringPair}&size={limit}");

                CheckResponse(response);

                var jsonTrades = await response.Content.ReadAsStringAsync();

                //Десерелизация сделок из json в .net структуру
                //https://www.newtonsoft.com/json/help/html/SerializingCollections.htm
                dynamic tradesOnPair = JsonConvert.DeserializeObject(jsonTrades);

                var tradeList = new List<TradeDto>();
                if (tradesOnPair == null)
                {
                    Console.WriteLine($"Нет ни одной сделки по валютной паре {stringPair}");
                }
                else
                {
                    //Перебрать все сделки по валютной паре
                    foreach (dynamic element in tradesOnPair)
                    {
                        if (element.Name == "status" || element.Name == "ch" || element.Name == "ts") continue;

                        foreach (dynamic list in element)
                        {
                            foreach (dynamic trade in list)
                            {
                                tradeList.Add(new TradeDto
                                {
                                    TradeId = (string)trade["id"],
                                    Price = trade.data[0]["price"],
                                    PairId = pair.Id,
                                    Amount = trade.data[0]["amount"],
                                    Quantity = 0,
                                    Date = ConvertDateTime((long)trade["ts"]),
                                    ExchangeId = Exchange.Id
                                });
                            }
                        }
                    }
                    trades.Add(pair, tradeList);
                }
            }

            return trades;
        }

        /// <summary>
        /// Ордера по валютной паре
        /// </summary>
        /// <param name="pair"></param>
        /// <param name="limit">кол-во отображаемых позиций (по умолчанию 100, максимум 2000)</param>
        /// <returns></returns>
        public async Task<IEnumerable<OrderDto>> OrdersAsync(PairDto pair, uint limit = 100)
        {
            // TODO НУЖНО НАД ЭТОЙ ХРЕНЬЮ ПОДУМАТЬ
            const string type = "step0";

            var stringPair = pair.GetSystemName(PairSplitter);

            HttpResponseMessage response = await Client.GetAsync($"{UrlApi}/market/depth?symbol={stringPair}&type={type}");

            CheckResponse(response);

            var jsonSymbols = await response.Content.ReadAsStringAsync();

            dynamic objects = JsonConvert.DeserializeObject(jsonSymbols);

            var ordersOut = new List<OrderDto>();
            foreach (var el in objects.tick["bids"])
            {
                ordersOut.Add(new OrderDto()
                {
                    Price = (decimal)el[0],
                    Quantity = (decimal)el[1],
                    TradeTypes = TradeTypes.Bay,
                    ExchangeId = Exchange.Id,
                    PairId = pair.Id
                });
            }

            foreach (var el in objects.tick["asks"])
            {
                ordersOut.Add(new OrderDto()
                {
                    Price = (decimal)el[0],
                    Quantity = (decimal)el[1],
                    TradeTypes = TradeTypes.Bay,
                    ExchangeId = Exchange.Id,
                    PairId = pair.Id
                });
            }

            return ordersOut;
        }

        /// <summary>
        /// Cтатистика цен и объемов торгов по валютным парам
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<PairInfoOnExchangeDto>> TickerAsync()
        {
            HttpResponseMessage response = await Client.GetAsync($"{UrlApi}/market/tickers");

            CheckResponse(response);

            var jsonSymbols = await response.Content.ReadAsStringAsync();

            dynamic objects = JsonConvert.DeserializeObject(jsonSymbols);

            if (objects == null) throw new Exception($"Не удалось распарсить сделки полученные с биржи: {jsonSymbols}");

            var pairsInfo = new List<PairInfoOnExchangeDto>();
            foreach (dynamic keyValuePair in objects.data)
            {
                PairDto pair = GetPairFromString(keyValuePair);

                pairsInfo.Add(new PairInfoOnExchangeDto()
                {
                    ExchangeId = Exchange.Id,
                    Updated = ConvertDateTime(0),
                    High = keyValuePair["high"],
                    Low = keyValuePair["low"],
                    Avarage = 0,
                    ValueCurr = keyValuePair["amount"],
                    LastTrade = keyValuePair["close"],
                    BuyPrice = 0,
                    SellPrice = 0,
                    Value = keyValuePair["vol"],
                    Pair = pair
                });
            }

            return pairsInfo;
        }

        /// <summary>
        /// Cписок валют биржи
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<CurrencyDto>> CurrenciesAsync()
        {
            HttpResponseMessage response = await Client.GetAsync(UrlApi + "/v1/common/currencys");

            CheckResponse(response);

            var jsonCurrency = await response.Content.ReadAsStringAsync();

            dynamic symbols = JsonConvert.DeserializeObject(jsonCurrency);

            var list = new List<string>();
            foreach (var el in symbols.data)
            {
                list.Add((string)el);
            }

            var currencies = new List<CurrencyDto>();
            //Перебираем распарсеный лист валют, получая обеъкты "Валюты"
            foreach (var currency in list)
            {
                currencies.Add(new CurrencyDto() { BrifName = currency });
            }
            return currencies;
        }

        /// <summary>
        /// Получение настроек валютных пар
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<PairSettings>> PairSettingsAsync()
        {
            HttpResponseMessage response = await Client.GetAsync($"{UrlApi}/v1/common/symbols");

            CheckResponse(response);

            var jsonPairSettings = await response.Content.ReadAsStringAsync();

            dynamic parsedPairSettings = JsonConvert.DeserializeObject(jsonPairSettings);

            if (parsedPairSettings == null) throw new Exception("Не удалось загрузить данные");

            var pairsSettings = new List<PairSettings>();
            foreach (dynamic parsedPairSetting in parsedPairSettings.data)
            {
                PairDto pair = GetPairFromString(parsedPairSetting);

                pairsSettings.Add(new PairSettings()
                {
                    MaxAmount = (decimal)parsedPairSetting["max-order-amt"],
                    MaxPrice = 0,
                    MaxQuantity = 0,
                    MinAmount = (decimal)parsedPairSetting["min-order-amt"],
                    MinPrice = (decimal)parsedPairSetting["min-order-value"],
                    MinQuantity = 0,
                    Pair = pair
                });
            }

            return pairsSettings;
        }

#endregion

#region Методы с авторизацией

        /// <summary>
		/// Получение баланса кошелька пользователя
		/// </summary>
		/// <returns></returns>
        public async Task<ExchangeUserInfo> UserInfoAsync()
        {
            var resourcePath = $"/v1/account/accounts/{AbpSession.UserId}/balance";
            var keyPair = await ExchangeUserKeyProvider.GetKeyAsync(Exchange.Id);

            string parameters = GetCommonParameters(keyPair.ApiOpenKey); //Параметр запроса
            var sign = GetSignatureStr("GET", UrlApi, resourcePath, parameters, keyPair.ApiSecretKey);   //подпись
            parameters += $"&Signature={sign}";

            HttpResponseMessage response = await Client.GetAsync($"{UrlApi}{resourcePath}?{parameters}");

            CheckResponse(response);

            var jsonStringUserInfo = await response.Content.ReadAsStringAsync();

            dynamic huobiProUserInfo = JsonConvert.DeserializeObject(jsonStringUserInfo);

            var userInfo = new ExchangeUserInfo()
            {
                ExchangeUserId = keyPair.ExchangeUserId,
                InfoReciveDate = ConvertDateTime(0) // не получаем мы время
            };

            /*
             {
                "currency": "usdt",
                "type": "trade",
                "balance": "500009195917.4362872650"
             },
             */
            foreach (var currencyBalancePair in huobiProUserInfo.list)
            {
                userInfo.Balances.Add(new KeyValuePair<Currency, decimal>(
                    new Currency { BrifName = (string)currencyBalancePair["currency"] },
                    (decimal)currencyBalancePair["balance"])
                );
            }

            //foreach (var currencyReservePair in huobiProUserInfo.reserved)
            //{
            //    userInfo.Reserved.Add(new KeyValuePair<Currency, decimal>(
            //        _currencyRepository.GetAll().FirstOrDefault(c => c.BrifName == currencyReservePair.Key),
            //        currencyReservePair.Value)
            //    );
            //}
            return userInfo;
        }

        /// <summary>
		/// Получение списка открытых ордеров пользователя
		/// </summary>
		/// <returns></returns>
        public async Task<IEnumerable<OrderDto>> OpenOrdersAsync()
        {
            const string resourcePath = "/v1/order/openOrders";
            var keyPair = await ExchangeUserKeyProvider.GetKeyAsync(Exchange.Id);

            string parameters = GetCommonParameters(keyPair.ApiOpenKey); //Параметр запроса
            var sign = GetSignatureStr("GET", UrlApi, resourcePath, parameters, keyPair.ApiSecretKey);   //подпись
            parameters += $"&Signature={sign}";

            HttpResponseMessage response = await Client.GetAsync($"{UrlApi}{resourcePath}?{parameters}");

            // Проверяем результат на отсутствие ошибок
            CheckResponse(response);

            var jsonOrders = await response.Content.ReadAsStringAsync();

            dynamic ordersExchange = JsonConvert.DeserializeObject(jsonOrders);

            if (ordersExchange == null) throw new Exception($"Не удалось распарсить сделки полученные с биржи: {jsonOrders}");

            var orders = new List<OrderDto>();

            foreach (var order in ordersExchange.data)
            {
                foreach (var huobiProOpenOrder in order.Value)
                {
                    orders.Add(new OrderDto()
                    {
                        ExchangeId = Exchange.Id,
                        Quantity = huobiProOpenOrder["amount"],
                        Price = huobiProOpenOrder["price"],
                        Date = ConvertDateTime(0),
                        TradeTypes = IdentifyOrderType(huobiProOpenOrder["type"]),
                        OrderStatus = OrderStatuses.Opened,
                    });
                }
            }

            return orders;
        }

        /// <summary>
        /// Создание ордера
        /// </summary>
        /// <param name="order"></param>
        /// <param name="orderCreateType"></param>
        public async Task OrderCreateAsync(OrderDto order, OrderCreateTypes orderCreateType)
        {
            if (order == null) throw new Exception("Создаваемый ордер равен null");

            const string resourcePath = "/v1/order/orders/place";
            var keyPair = await ExchangeUserKeyProvider.GetKeyAsync(Exchange.Id);

            string parameters = GetCommonParameters(keyPair.ApiOpenKey); //Параметр запроса
            var sign = GetSignatureStr("GET", UrlApi, resourcePath, parameters, keyPair.ApiSecretKey);   //подпись
            parameters += $"&Signature={sign}";

            HttpResponseMessage response = await Client.GetAsync($"{UrlApi}{resourcePath}?{parameters}");

            CheckResponse(response);

            var jsonOrder = await response.Content.ReadAsStringAsync();
            // если все хорошо "data": "59378"
            // иначе { "status": "error", "err-code": "order-orderstate-error", "err-msg": "Incorrect order state", "data": null }
            dynamic orderExchange = JsonConvert.DeserializeObject(jsonOrder);

            if (orderExchange == null) throw new Exception($"Не удалось распарсить сделки полученные с биржи: {jsonOrder}");

            var orderData = orderExchange.data;
        }

#endregion

#region Вспомогательные методы

        /// <summary>
        /// Пример метода расширения функциональных возможностей драйвера биржи
        /// </summary>
        /// <returns></returns>
        public string SpecificHuobiMethod()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Проверяет успешность http запроса
        /// </summary>
        /// <param name="response"></param>
        private static void CheckResponse(HttpResponseMessage response)
        {
            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception($"Сервер биржи Huobi.pro возвращает ошибку: {response.StatusCode}");
            }
        }
		
		/// <summary>
		/// Метод определяющий тип Order'а
		/// </summary>
		/// <param name="orderType"> строка содержащая тип Order'а</param>
		/// <returns>Возвращает тип Order'а</returns>
		private static TradeTypes IdentifyOrderType(string orderType)
        {
            return orderType.ToLower().Contains("buy") ? TradeTypes.Bay : TradeTypes.Sell;
        }

        /// <summary>
        /// Получить универсальные параметры подписи
        /// </summary>
        /// <returns></returns>
        private static string GetCommonParameters(string accessKey)
        {
            return $"AccessKeyId={accessKey}&SignatureMethod=HmacSHA256&SignatureVersion=2&Timestamp={DateTime.UtcNow:yyyy-MM-ddTHH:mm:ss}";
        }

        /// <summary>
        /// Подпись параметра запроса
        /// </summary>
        /// <param name="method">Метод запроса</param>
        /// <param name="host">Доменное имя API</param>
        /// <param name="resourcePath">Адрес ресурса</param>
        /// <param name="parameters">Параметр запроса</param>
        /// <param name="secretKey">Секретный ключ</param>
        /// <returns></returns>
        private static string GetSignatureStr(string method, string host, string resourcePath, string parameters, string secretKey)
        {
            var sb = new StringBuilder();
            sb.Append(method.ToUpper()).Append("\n")
              .Append(host).Append("\n")
              .Append(resourcePath).Append("\n")
              .Append(parameters);

            // Рассчитайте сигнатуру и передайте следующие два параметра в криптографическую хеш-функцию
            string sign = CalculateSignature256(sb.ToString(), secretKey);

            return sign;
        }

        /// <summary>
        /// Шифрование Hmacsha256
        /// </summary>
        /// <param name="text">Строка для шифрования</param>
        /// <param name="secretKey">Секретный ключ</param>
        /// <returns></returns>
        private static string CalculateSignature256(string text, string secretKey)
        {
            using (var hmacsha256 = new HMACSHA256(Encoding.UTF8.GetBytes(secretKey)))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(Encoding.UTF8.GetBytes(text));
                return Convert.ToBase64String(hashmessage);
            }
        }

        /// <summary>
        /// Получение пары валют
        /// </summary>
        /// <param name="pairSetting">Название пары</param>
        /// <returns></returns>
        private static PairDto GetPairFromString(dynamic pairSetting)
        {
            var pair = new PairDto
            {
                Currency1 = new CurrencyDto {BrifName = (string)pairSetting["base-currency"]},
                Currency2 = new CurrencyDto { BrifName = (string)pairSetting["quote-currency"]}
            };

            return pair;
        }

#endregion
    }
}