﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using MyExchange.Api.Constants;
using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Dto.Order;
using MyExchange.Api.Dto.Pair;
using MyExchange.Api.Dto.PairInfoOnExchange;
using MyExchange.Api.Dto.PairSettings;
using MyExchange.Api.Dto.Trade;
using MyExchange.Api.Entity;
using MyExchange.Api.Extentions;
using MyExchange.Api.MyExchange;
using MyExchange.Api.Services.Bl;
using MyExchange.Web.Exchange;
using Newtonsoft.Json;

namespace MyExchange.ExchangeExmo
{
	/// <summary>
	/// Драйвер для работы с биржей Exmo.me
	/// https://exmo.me/ru/api - документация по api
	/// </summary>
	public class ExmoExchangeDriver : ExchangeDriverBase, IExmoExchangeDriver
	{
	    /// <summary>
		/// Клиент, с помощью которого делаются REST запросы на Api биржи
		/// </summary>(
		private static readonly HttpClient Client = new HttpClient();

		public Exchange Exchange { private get; set; }
		
		/// <summary>
		/// Принятый на этой бирже разделитель валют в наименовании валютной пары
		/// На каждой бирже он может быть свой
		/// </summary>
		private const string PairSplitter = "_";

        /// <summary>
        /// Адрес API биржи
        /// </summary>
        private const string UrlApi = "https://api.exmo.com/v1";

        public ExmoExchangeDriver(IExchangeUserKeyProvider exchangeUserKeyProvider) : base(exchangeUserKeyProvider)
		{
		}

#region Методы не требующие авторизации
		public async Task<IEnumerable<TradeDto>> TradesAsync(PairDto pair)
		{
		    var stringPair = pair.GetSystemName(PairSplitter);

            var request = ($"{UrlApi}/trades/?pair={stringPair}");
			HttpResponseMessage response = await Client.GetAsync(request);

			var jsonTrades = await response.Content.ReadAsStringAsync();

			//Десерелизация сделок из json в .net структуру
			//https://www.newtonsoft.com/json/help/html/SerializingCollections.htm
			var tradesOnPair = JsonConvert.DeserializeObject<IDictionary<string, List<ExmoTrade>>>(jsonTrades);

			var trades = new List<TradeDto>();

		    if (!tradesOnPair.Any())
		    {
		        Console.WriteLine($"Нет ни одной сделки по валютной паре {stringPair}");
		    }
		    else
		    {
		        //Перебрать все сделки по валютной паре
		        foreach (var trade in tradesOnPair.First().Value)
		        {
		            trades.Add(new TradeDto
                    {
		                TradeId = trade.trade_id,
		                Price = trade.price,
		                PairId = pair.Id,
		                Amount = trade.amount,
		                Quantity = trade.quantity,
		                Date = ConvertDateTime(trade.date),
                        ExchangeId = Exchange.Id
		            });
		        }
		    }

		    return trades;
		}

	    public async Task<IDictionary<PairDto,IEnumerable<TradeDto>>> TradesAsync(IList<PairDto> pairs)
		{
			var tradesByPairs = new Dictionary<PairDto, IEnumerable<TradeDto>>();

			string pairNameList = "";
			
			//Агрегируем список по всем валютным парам для формирования единого запроса
			foreach (var pair in pairs)
			{
				if (pairNameList != "")
				{//Уже не первая добавленная пара, поэтому нужен разделительмежду парами
					pairNameList += ",";
				}
				pairNameList += pair.GetSystemName(PairSplitter);

			}

			//Одним запросом получаем все валютные пары и сделки по ним
			HttpResponseMessage response = await Client.GetAsync(UrlApi + "/trades/?pair=" + pairNameList);

            var jsonTrades = await response.Content.ReadAsStringAsync();

			//Десерелизация сделок из json в .net структуру
			//https://www.newtonsoft.com/json/help/html/SerializingCollections.htm
			var deserializedTradesByPairs = JsonConvert.DeserializeObject<IDictionary<string, List<ExmoTrade>>>(jsonTrades);
			
			//перебираются все валютные пары
			foreach (var keyValuePair in deserializedTradesByPairs)
			{
				var trades = new List<TradeDto>();
				
				//находим валютную пару, соответсвующую ключу в распарсеной структуре
				var pair = pairs.FirstOrDefault(p => p.GetSystemName(PairSplitter) == keyValuePair.Key);

                if (pair == null) continue;

                //Перебрать все сделки по валютной паре
                foreach (var trade in keyValuePair.Value)
                {
                    trades.Add(new TradeDto
                    {
                        TradeId = trade.trade_id,
                        Price = trade.price,
                        PairId = pair.Id,
                        Amount = trade.amount,
                        Quantity = trade.quantity,
                        Date = ConvertDateTime(trade.date),
                        ExchangeId = Exchange.Id
                    });
                }

                tradesByPairs.Add(pair, trades);
            }

			return tradesByPairs;
		}

		/// <summary>
		/// Книга ордеров по валютной паре
		/// </summary>
		/// <param name="pair">Валютная пара</param>
		/// <param name="limit">кол-во отображаемых позиций (по умолчанию 100, максимум 1000)</param>
		/// <returns></returns>
		public async Task<IEnumerable<OrderDto>> OrdersAsync(PairDto pair, uint limit = 100)
		{
			HttpResponseMessage response = await Client.GetAsync(UrlApi + "/order_book/?pair=" + pair.GetSystemName(PairSplitter) + "&limit=" + limit);

			var jsonTrades = await response.Content.ReadAsStringAsync();

			//Десерелизация сделок из json в .net структуру
			//https://www.newtonsoft.com/json/help/html/SerializingCollections.htm

			var orders = JsonConvert.DeserializeObject<IDictionary<string, ExmoOrder>>(jsonTrades);

			var ordersOut = new List<OrderDto>();

			//Перебрать все сделки по валютной паре
			foreach (var order in orders)
			{
				foreach (var ask in order.Value.ask)
				{
					ordersOut.Add(new OrderDto()
					{
						Price = ask[0],
						Quantity = ask[1],
						TradeTypes = TradeTypes.Sell,
                        ExchangeId = Exchange.Id,
                        PairId = pair.Id
					});
				}

				foreach (var bid in order.Value.bid)
				{
					ordersOut.Add(new OrderDto()
					{
						Price = bid[0],
						Quantity = bid[1],
						TradeTypes = TradeTypes.Bay,
                        ExchangeId = Exchange.Id,
                        PairId = pair.Id
					});
				}
			}

			return ordersOut;
		}

		/// <summary>
		/// Метод возвращающий статистику цен и объемов торгов по валютным парам
		/// </summary>
		/// <returns></returns>
		public async Task<IEnumerable<PairInfoOnExchangeDto>> TickerAsync()
		{
			HttpResponseMessage response = await Client.GetAsync(UrlApi + "/ticker/");

			CheckResponse(response);

			var jsonTicker = await response.Content.ReadAsStringAsync();

			//Десерелизация сделок из json в .net структуру
			//https://www.newtonsoft.com/json/help/html/SerializingCollections.htm

			var ticker = JsonConvert.DeserializeObject<IDictionary<string, ExmoTicker>>(jsonTicker);

			if (ticker == null) throw new Exception($"Не удалось распарсить сделки полученные с биржи: {jsonTicker}");

			var pairsInfo = new List<PairInfoOnExchangeDto>();

			foreach(var keyValuePair in ticker)
			{
                var pair = GetPairFromString(keyValuePair.Key);
                
                    //TODO Эту логику вынесем наверх из драйвера
                    //_pairRepository
                    //.FirstOrDefault(p => p.Currency1.BrifName == currencyNames[0]
                    //                     && p.Currency2.BrifName == currencyNames[1])
                    //?? throw new Exception("Не удалось загрузить валютную пару из БД");
               
                pairsInfo.Add(new PairInfoOnExchangeDto()
				{
					ExchangeId = Exchange.Id,
                    Pair = pair,
					Updated = ConvertDateTime(keyValuePair.Value.updated),
					High = keyValuePair.Value.high,
					Low = keyValuePair.Value.low,
					Avarage = keyValuePair.Value.avg,
					ValueCurr = keyValuePair.Value.vol_curr,
					LastTrade = keyValuePair.Value.last_trade,
					BuyPrice = keyValuePair.Value.buy_price,
					SellPrice = keyValuePair.Value.sell_price,
					Value = (ulong)keyValuePair.Value.vol                    
				});
			}
			return pairsInfo;
		}


		/// <summary>
		/// Метод получения списка валют биржи
		/// </summary>
		/// <returns></returns>
		public async Task<IEnumerable<CurrencyDto>> CurrenciesAsync()
		{
			HttpResponseMessage response = await Client.GetAsync(Path.Combine(UrlApi,"currency"));

			CheckResponse(response);

			var jsonCurrencies = await response.Content.ReadAsStringAsync();

			//Десерелизация сделок из json в .net структуру
			//https://www.newtonsoft.com/json/help/html/SerializingCollections.htm

			// распарсенный список валют
			var deserializedCurrencies = JsonConvert.DeserializeObject<List<string>>(jsonCurrencies);

			var currenciesFromDb = new List<CurrencyDto>();

			//Перебираем распарсеный лист валют, получая обеъкты "Валюты"
			foreach (var currency in deserializedCurrencies)
			{
				// Если валюта уже имеется в нашей БД то поднимаем ее из БД и возвращаем
				// Если валюты нет, то добавляем в БД
				var currencyFromDb = new CurrencyDto() { BrifName = currency };
				currenciesFromDb.Add(currencyFromDb);
			}
			return currenciesFromDb;
		}

		/// <summary>
		/// Метод для получение информации о валютных парах
		/// </summary>
		/// <returns></returns>
		public async Task<IEnumerable<PairSettings>> PairSettingsAsync()
		{
			HttpResponseMessage response = await Client.GetAsync(Path.Combine(UrlApi, "pair_settings"));

			CheckResponse(response);

			var jsonPairSettings = await response.Content.ReadAsStringAsync();

			//Десерелизация сделок из json в .net структуру
			//https://www.newtonsoft.com/json/help/html/SerializingCollections.htm

			var parsedPairSettings = JsonConvert.DeserializeObject<IDictionary<string, ExmoPairSettings>>(jsonPairSettings);

			if (parsedPairSettings == null) throw new Exception("Не удалось загрузить данные");

			var pairsSettings = new List<PairSettings>();

			foreach (var parsedPairSetting in parsedPairSettings)
			{
                var pair = GetPairFromString(parsedPairSetting.Key);

                pairsSettings.Add(new PairSettings()
				{
					MaxAmount = parsedPairSetting.Value.max_amount,
					MaxPrice = parsedPairSetting.Value.max_price,
					MaxQuantity = parsedPairSetting.Value.max_quantity,
					MinAmount = parsedPairSetting.Value.min_amount,
					MinPrice = parsedPairSetting.Value.min_price,
					MinQuantity = parsedPairSetting.Value.min_quantity,
                    Pair = pair
				});
			}

			return pairsSettings;
		}
#endregion

#region Методы с авторизацией

		/// <summary>
		/// Получение баланса кошелька пользователя
		/// </summary>
		/// <returns></returns>
		public async Task<ExchangeUserInfo> UserInfoAsync()
		{
            var keyPair = await ExchangeUserKeyProvider.GetKeyAsync(Exchange.Id);
            var content = await PrepereContent(); //запрос без входных параметров
			 
			// делаем запрос, получаем результат
			HttpResponseMessage response = await Client.PostAsync(UrlApi + "/user_info", content);

			CheckResponse(response);

			var jsonStringUserInfo = await response.Content.ReadAsStringAsync();

			var exmoUserInfo = JsonConvert.DeserializeObject<ExmoUserInfo>(jsonStringUserInfo);

			// заполняем структуру данными полученными с биржи
			var userInfo = new ExchangeUserInfo()
			{
                //TODO изменить передачу параметра на более надежный вызов
                ExchangeUserId = keyPair.ExchangeUserId,
				InfoReciveDate = ConvertDateTime(exmoUserInfo.server_date),
               
			};
			
			foreach (var currencyBalancePair in exmoUserInfo.balances)
            {
                userInfo.Balances.Add(new KeyValuePair<Currency, decimal>(
                    new Currency {BrifName = currencyBalancePair.Key }, 
					currencyBalancePair.Value)
				);
			}

			foreach (var currencyReservePair in exmoUserInfo.reserved)
            {
                userInfo.Reserved.Add(new KeyValuePair<Currency, decimal>(
                        new Currency { BrifName = currencyReservePair.Key },
                        currencyReservePair.Value));
			}

			return userInfo;
		}

		/// <summary>
		/// Получение списка открытых ордеров пользователя
		/// </summary>
		/// <returns></returns>
		public async Task<IEnumerable<OrderDto>> OpenOrdersAsync()
		{
			// Контейнер для значений отправляемых на сервер
			var content = await PrepereContent();

			// делаем запрос, получаем результат
			HttpResponseMessage response = await Client.PostAsync(UrlApi + "/user_open_orders", content);

			// Проверяем результат на отсуствие ошибок
			CheckResponse(response);

			var json = await response.Content.ReadAsStringAsync();

			var ordersExchange = JsonConvert.DeserializeObject<Dictionary<string, List<ExmoOpenOrder>>>(json);

			var orders = new List<OrderDto>();

			foreach(var order in ordersExchange)
			{
				foreach (var exmoOpenOrder in order.Value)
				{
                    var pair = GetPairFromString(order.Key);

                    orders.Add(new OrderDto()
					{
                        ExchangeId = Exchange.Id,
						Quantity = exmoOpenOrder.quantity,
						Price = exmoOpenOrder.price,
						Date = ConvertDateTime(exmoOpenOrder.created),
						TradeTypes = IdentifyOrderType(exmoOpenOrder.type),
						OrderStatus = OrderStatuses.Opened,
                        Pair = pair
					});
				}                
			}

			return orders;
		}


        /// <summary>
        /// Создание ордера
        /// </summary>
        /// <param name="order"></param>
        /// <param name="orderCreateType"></param>
        /// TODO Метод должен возвращать результат (см api Exmo.me)
        public async Task OrderCreateAsync(OrderDto order, OrderCreateTypes orderCreateType)
		{
			if (order == null) throw new Exception("Создаваемый ордер равен null");

			#region comments
			//ExmoCreateOrder createOrder = new ExmoCreateOrder();
			//createOrder.pair = PairToString(order.Pair);
			//createOrder.price = order.Price;
			//createOrder.quantity = order.Quantity;
			//createOrder.type = OrderCreateTypeToString(orderCreateType);
			#endregion
			
			// Параметры для запроса на биржу
			var parameters = new Dictionary<string, string>()
			{
				{ "pair", order.Pair.GetSystemName(PairSplitter) },
				{ "quantity", order.Quantity.ToString() },
				{ "price", order.Price.ToString() },
				{ "type", OrderCreateTypeToString(orderCreateType) }
				//{ "nonce", Convert.ToString(GetTimestamp()) }
			};

			var content = await PrepereContent(parameters);

			HttpResponseMessage response = await Client.PostAsync(UrlApi + "/order_create", content);

			CheckResponse(response);

		    var json = await response.Content.ReadAsStringAsync();
        }

#endregion


#region Вспомогательные методы
		/// <summary>
		/// Подготавливает контент запроса на основе передаваемых параметров
		/// </summary>
		/// <param name="parameters">если параметры не указаны, то генерируется пустой список параметров</param>
		/// <returns></returns>
		private async Task<FormUrlEncodedContent> PrepereContent(Dictionary<string, string> parameters = null)
		{
			if (parameters == null)
			{
				parameters = new Dictionary<string, string>();
			}
			// Дополнительно во всех запросах должен находиться обязательный POST-параметр 
			// nonce с инкрементным числовым значением(> 0).Это значение не должно повторяться или уменьшаться.
			parameters.Add("nonce", Convert.ToString(GetTimestamp()));

			var userAndKeys = await GetExchangeUserKeysAsync();

			// шифруем строку на основе APIsecret, получаем ключ
			var sign = Sign(userAndKeys.ApiSecret, ToQuery(parameters));

			// Контейнер для значений отправляемых на сервер
			var content = new FormUrlEncodedContent(parameters);

			content.Headers.Add("Sign", sign);
			content.Headers.Add("Key", userAndKeys.Key);

			return content;
		}

		/// <summary>
		/// Формируем запрос из всех строк
		/// TODO комментарий не проясняят назначения метода, а его название слишком обобщенное
		/// </summary>
		/// <param name="parameters"></param>
		/// <returns></returns>
		private string ToQuery(IDictionary<string, string> parameters)
		{
			var queries = new List<string>();

			foreach(var parameter in parameters)
			{
				queries.Add(HttpUtility.UrlEncode(parameter.Key) + "=" + HttpUtility.UrlEncode(parameter.Value));
			}

			var query = string.Join("&", queries);

			return query;
		}


		/// <summary>
		/// Метод принимающий тип созданного ордера и возвращающий 
		/// строковый результат адаптированный под конректную биржу
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		private string OrderCreateTypeToString(OrderCreateTypes type)
		{
			/* buy - ордер на покупку
			   sell - ордер на продажу
			   market_buy - ордера на покупку по рынку
			   market_sell - ордер на продажу по рынку
			   market_buy_total - ордер на покупку по рынку на определенную сумму
			   market_sell_total - ордер на продажу по рынку на определенную сумму */
			string strType = "";
			switch (type)
			{
				case OrderCreateTypes.LimitBuy:
                    strType =  "buy";
                    break;

				case OrderCreateTypes.LimitSell:
                    strType = "sell";
                    break;

				case OrderCreateTypes.MarketBuy:
                    strType = "market_buy";
                    break;

				case OrderCreateTypes.MarketSell:
                    strType = "market_sell";
                    break;
                // TODO разобраться с остальными типами
            }
            return strType;
		}


        /// <summary>
        /// Получение валютной пары из строки. Например: "BTC_USD"
        /// </summary>
        /// <param name="stringPair"></param>
        /// <returns></returns>
        private PairDto GetPairFromString(string stringPair)
		{
		    var currencyNames = stringPair.Split(PairSplitter.ToCharArray());

		    var pair = new PairDto
		    {
                Currency1 = new CurrencyDto { BrifName = currencyNames[0] },
                Currency2 = new CurrencyDto { BrifName = currencyNames[1] }
		    };

		    return pair;
		}


		/// <summary>
		/// Проверяет успешность http запроса
		/// </summary>
		/// <param name="response"></param>
		private void CheckResponse(HttpResponseMessage response)
		{
			if (response.StatusCode != HttpStatusCode.OK)
			{
				throw new Exception($"Сервер биржи Exmo.me возвращает ошибку: {response.StatusCode}");
			}
		}


		/// <summary>
		/// Получить ключи пользователя на бирже
		/// </summary>
		/// <returns></returns>
		private async Task<UserAndKeys> GetExchangeUserKeysAsync()
		{
            //TODO исправить недочет  с идентификатором пользователя
            var keyPair = await ExchangeUserKeyProvider.GetKeyAsync(Exchange.Id);

            if (keyPair == null) throw new Exception("Пользователь с указанным параметрами не найден");

			var andKeys = new UserAndKeys {
				ApiSecret = keyPair.ApiSecretKey,
				Key = keyPair.ApiOpenKey,
                ExchangeUserId = keyPair.ExchangeUserId
            };

			return andKeys;
		}


		/// <summary>
		/// Получить дату и время в миллисекундах
		/// </summary>
		/// <returns></returns>
		private long GetTimestamp()
		{
			var d = (DateTime.Now - new DateTime(1970, 1, 1)).TotalMilliseconds;
			return (long)d;
		}


		/// <summary>
		/// Шифруем сообщение по ключу
		/// </summary>
		/// <param name="key">ключ шифрования</param>
		/// <param name="message">строка, которая будет зашифрована</param>
		/// <returns></returns>
		private string Sign(string key, string message)
		{
			using (HMACSHA512 hmac = new HMACSHA512(Encoding.UTF8.GetBytes(key)))
			{
				byte[] b = hmac.ComputeHash(Encoding.UTF8.GetBytes(message));

				return b.ConvertToString();
			}
		}
		
		/// <summary>
		/// Метод определяющий тип Order'а
		/// </summary>
		/// <param name="orderType"> строка содержащая тип Order'а</param>
		/// <returns>Возвращает тип Order'а</returns>
		private TradeTypes IdentifyOrderType(string orderType)
        {
            return orderType.ToLower().Contains("buy") ? TradeTypes.Bay : TradeTypes.Sell;
        }

#endregion
	}
	
	/// <summary>
	/// Структура сделки специфичная для этой биржи
	/// </summary>
    internal struct ExmoTrade
	{
		public string trade_id { get; set; }
		public string type { get; set; }
		public decimal price { get; set; }
		public decimal quantity { get; set; }
		public decimal amount { get; set; }
		/// <summary>
		/// Дата и время в формате UNIX
		/// https://ru.wikipedia.org/wiki/UNIX-%D0%B2%D1%80%D0%B5%D0%BC%D1%8F
		/// http://aione.ru/unix-timestamp-v-csharp-primeryi-konvertatsii/
		/// </summary>
		public long date { get; set; }
	}

    /// <summary>
    /// Структура заказа специфичная для этой биржи
    /// </summary>
    internal struct ExmoOrder
	{
		public decimal ask_quantity { get; set; }
		public decimal ask_amount { get; set; }
		public decimal ask_top { get; set; }
		public decimal bid_quantity { get; set; }
		public decimal bid_amount { get; set; }
		public decimal bid_top { get; set; }
		[JsonProperty]
		public List<List<decimal>> bid { get; set; }
		[JsonProperty]
		public List<List<decimal>> ask { get; set; }
	}

    /// <summary>
    /// Структура настройки валютных пар специфичная для этой биржи
    /// </summary>
    internal struct ExmoPairSettings
	{
		public decimal min_quantity { get; set; }
		public decimal max_quantity { get; set; }
		public decimal last_trade { get; set; }
		public decimal min_price { get; set; }
		public decimal max_price { get; set; }
		public decimal min_amount { get; set; }
		public decimal max_amount { get; set; }
	}

    /// <summary>
    /// Структура ticker специфичная для этой биржи
    /// </summary>
    internal struct ExmoTicker
	{
		public decimal buy_price { get; set; }
		public decimal sell_price { get; set; }
		public decimal last_trade { get; set; }
		public decimal high { get; set; }
		public decimal low { get; set; }
		public decimal avg { get; set; }
		public decimal vol { get; set; }
		public decimal vol_curr { get; set; }
		public long updated { get; set; }
	}

    /// <summary>
    /// Структура user специфичная для этой биржи
    /// </summary>
    internal struct ExmoUserInfo
	{
		public string uid { get; set; }
		public long server_date { get; set; }

		[JsonProperty]
		public IDictionary<string, decimal> balances { get; set; }

		[JsonProperty]
		public IDictionary<string, decimal> reserved { get; set; }
	}

    /// <summary>
    /// Структура OpenOrder специфичная для этой биржи
    /// </summary>
    internal struct ExmoOpenOrder
	{
		public decimal order_id { get; set; }
		public long created { get; set; }
		public string type { get; set; }
		public string pair { get; set; }
		public decimal price { get; set; }
		public decimal quantity { get; set; }
		public decimal amount { get; set; }
	}

    /// <summary>
    /// Стуруктура для хранения ключей (api) пользователя. Специфичная для этой биржи
    /// </summary>
    internal struct UserAndKeys
	{
		public Guid ExchangeUserId { get; set; }
		public string ApiSecret { get; set; }
		public string Key { get; set; }
	}

}
