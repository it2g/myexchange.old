﻿using Abp.Modules;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using MyExchange.Api.MyExchange;
using MyExchange.Base;

namespace MyExchange.ExchangeExmo
{
    [DependsOn(typeof(BaseModule))]
    public class ExmoModule : AbpModule
    {
        /// <summary>
        /// Предварительное подключение конвенций регистрации для контейнера,
        /// для автоматической регистрации контекстов данных, конфигураций и 
        /// компонентов системы (репозиториев и сервисов).
        /// </summary>
        public override void PreInitialize()
        {
            //IocManager.AddConventionalRegistrar(new ConfigurationConventionalRegistrar());
            //IocManager.AddConventionalRegistrar(new ComponentConventionalRegistrar());

            // включение поддержки внедрения коллекции зависимостей.
            // Эта настройка позволяет контейнеру внедрять зависимости в свойства вида T[], IList<T>, IEnumerable<T>, ICollection<T>,
            // пытаясь подставить в них коллекцию со всеми найденными реализациями типа T. 
            // Если ни одной зависимости типа T нет, контейнер внедрит пустую коллекцию
            IocManager.IocContainer.Kernel.Resolver.AddSubResolver(new CollectionResolver(IocManager.IocContainer.Kernel, true));

        }

        /// <summary>
        /// Регистрация компонентов текущей сборки и сборки модели справочников в контейнере
        /// </summary>
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ExmoModule).Assembly);
            IocManager.IocContainer.Register(Component.For<Exchange>().ImplementedBy<ExmoExchange>()
                .Named("750D352C-AF26-4ED5-9748-EE5EDA8B6659"));
        }

        /// <summary>
        /// Выполнение настроечной логики после завершения сборки контейнера
        /// </summary>
        public override void PostInitialize()
        {
        }


    }
}
