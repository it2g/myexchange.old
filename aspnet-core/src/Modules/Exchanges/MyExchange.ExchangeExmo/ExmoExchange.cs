﻿using System;
using MyExchange.Api.MyExchange;

namespace MyExchange.ExchangeExmo
{
    /// <summary>
    /// Криптовалютная биржа Exmo.me
    /// TODO попробовать использовать статический класс
    /// </summary>
    public class ExmoExchange: Exchange<IExmoExchangeDriver>
    {
        public override Guid Id { get; } = new Guid("750D352C-AF26-4ED5-9748-EE5EDA8B6659");

        public override string BriefName { get; } = "Exmo";

        public override string FullName { get; } = "Exmo";

        public override string Description { get; } = "";

        public ExmoExchange(IExmoExchangeDriver driver) : base(driver)
        {
        }
    }
}
