﻿using System.Linq;
using System.Threading.Tasks;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyExchange.Api.Data;
using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Dto.Order;
using MyExchange.Api.Dto.Pair;
using MyExchange.Api.Services.Bl;
using MyExchange.ExchangeExmo;


namespace MyExchange.Exchanges.Test
{
    /// <summary>
    /// Тестирование методов драйвера подключения к бирже Exmo
    /// </summary>
    [TestClass]
    public class ExmoDriverTests
    {
        [TestMethod]
        public void TradesForOneRairReturnSuccess()
        {
            var exchangeUserKeyProvider = A.Fake<IExchangeUserKeyProvider>();
            var driver = new ExmoExchangeDriver(exchangeUserKeyProvider);
            var exchange = new ExmoExchange(driver);

            var result = driver.TradesAsync(new PairDto
            {
                Currency1 = new CurrencyDto {BrifName = "BTC"},
                Currency2 = new CurrencyDto() {BrifName = "USD"}
            });

            Assert.IsNotNull(result);
            Assert.AreEqual(result.Result.Count(), 100);
        }

        [TestMethod]
        public void TradesForTwoRairReturnSuccess()
        {
            var exchangeUserKeyProvider = A.Fake<IExchangeUserKeyProvider>();
            var driver = new ExmoExchangeDriver(exchangeUserKeyProvider);
            var exchange = new ExmoExchange(driver);

            var result = driver.TradesAsync(new PairDto[]
            {
                new PairDto
                {
                    Currency1 = new CurrencyDto {BrifName = "BTC"},
                    Currency2 = new CurrencyDto {BrifName = "USD"}
                },
                new PairDto
                {
                    Currency1 = new CurrencyDto {BrifName = "XRP"},
                    Currency2 = new CurrencyDto {BrifName = "USD"}
                }

            });

            Assert.IsNotNull(result);
            Assert.AreEqual(result.Result.Count(), 2);
        }

        [TestMethod]
        public void TestMethodOrder()
        {
            var exchangeUserKeyProvider = A.Fake<IExchangeUserKeyProvider>();
            var driver = new ExmoExchangeDriver(exchangeUserKeyProvider);
            var exchange = new ExmoExchange(driver);

            var result = driver.OrdersAsync(new PairDto
            {
                Currency1 = new CurrencyDto {BrifName = "BTC"},
                Currency2 = new CurrencyDto { BrifName = "USD"}
            });

            Assert.IsNotNull(result);
            Assert.AreEqual(result.Result.Count(), 100);
        }

        [TestMethod]
        public void ReturnCurrenciesSuccess()
        {
            var exchangeUserKeyProvider = A.Fake<IExchangeUserKeyProvider>();
            var driver = new ExmoExchangeDriver(exchangeUserKeyProvider);
            var exchange = new ExmoExchange(driver);

            var result = driver.CurrenciesAsync();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(result.Result.Count(), 0);
        }

        [TestMethod]
        public void GetPairSettingsSuccessfuly()
        {
            var exchangeUserKeyProvider = A.Fake<IExchangeUserKeyProvider>();
            var driver = new ExmoExchangeDriver(exchangeUserKeyProvider);
            var exchange = new ExmoExchange(driver);

            var result = driver.PairSettingsAsync();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(result.Result.Count(), 0);
        }

        [TestMethod]
        public void GetUserInfoSuccessfuly()
        {
            var exchangeUserKeyProvider = A.Fake<IExchangeUserKeyProvider>();
            var driver = new ExmoExchangeDriver( exchangeUserKeyProvider);
            var exchange = new ExmoExchange(driver);

            A.CallTo(() => exchangeUserKeyProvider.GetKeyAsync(exchange.Id)).Returns(
                Task.FromResult(
                    new ExchangeUserKeyPair
                    {
                        ApiOpenKey = "K-ad17cc7e1758a1983c13c8755713688d2e15dff5",
                        ApiSecretKey = "S-77fae9fb950adc540a90b0969386a9e383f73061",
                        ExchangeUserId = exchange.Id
                    }
                ));

            var result = driver.UserInfoAsync();
           
            Assert.IsNotNull(result);
            Assert.AreNotEqual(result.Result.Balances.Count(), 0);
            Assert.AreNotEqual(result.Result.Reserved.Count(), 0);

        }

        [TestMethod]
        public void GetOpenOrdersSuccessfuly()
        {
            var exchangeUserKeyProvider = A.Fake<IExchangeUserKeyProvider>();
            var driver = new ExmoExchangeDriver(exchangeUserKeyProvider);
            var exchange = new ExmoExchange(driver);

            A.CallTo(() => exchangeUserKeyProvider.GetKeyAsync(exchange.Id)).Returns(
                Task.FromResult(
                    new ExchangeUserKeyPair
                    {
                        ApiOpenKey = "K-ad17cc7e1758a1983c13c8755713688d2e15dff5",
                        ApiSecretKey = "S-77fae9fb950adc540a90b0969386a9e383f73061",
                        ExchangeUserId = exchange.Id
                    }
                ));

            // нужно получить id'шники объектов
            var result = driver.OpenOrdersAsync();

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void CreateOrderSuccessfuly()
        {
            var exchangeUserKeyProvider = A.Fake<IExchangeUserKeyProvider>();
            var driver = new ExmoExchangeDriver(exchangeUserKeyProvider);
            var exchange = new ExmoExchange(driver);

            A.CallTo(() => exchangeUserKeyProvider.GetKeyAsync(exchange.Id)).Returns(
                Task.FromResult(
                    new ExchangeUserKeyPair
                    {
                        ApiOpenKey = "K-ad17cc7e1758a1983c13c8755713688d2e15dff5",
                        ApiSecretKey = "S-77fae9fb950adc540a90b0969386a9e383f73061",
                        ExchangeUserId = exchange.Id
                    }
                ));

            var order = new OrderDto
            {
                Price = 3,
                Quantity = 3
            };


            var t = driver.OrderCreateAsync(order, Api.Constants.OrderCreateTypes.LimitSell);
        }
    }


}
