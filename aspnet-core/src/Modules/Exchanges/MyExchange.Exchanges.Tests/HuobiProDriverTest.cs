﻿using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Dto.Pair;
using MyExchange.ExchangeHuobiPro;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyExchange.Api.Constants;
using MyExchange.Api.Data;
using MyExchange.Api.Dto.Order;
using MyExchange.Api.Dto.PairInfoOnExchange;
using MyExchange.Api.Dto.PairSettings;
using MyExchange.Api.Dto.Trade;
using MyExchange.Api.Services.Bl;

namespace MyExchange.Exchanges.Test
{
    [TestClass]
    public class HuobiProDriverTest
    {
        [TestMethod]
        public void TestTrades()
        {
            var exchangeUserKeyProvider = A.Fake<IExchangeUserKeyProvider>();
            var driver = new HuobiProExchangeDriver(exchangeUserKeyProvider);
            var exchange = new HuobiProExchange(driver);

            var result = driver.TradesAsync(new PairDto
            {
                Currency1 = new CurrencyDto { BrifName = "btc" },
                Currency2 = new CurrencyDto { BrifName = "usdt" }
            }).Result as List<TradeDto>;

            Assert.IsNotNull(result);
            Assert.AreEqual(100, result.Count);
        }

        [TestMethod]
        public void TestTradesList()
        {
            var exchangeUserKeyProvider = A.Fake<IExchangeUserKeyProvider>();
            var driver = new HuobiProExchangeDriver(exchangeUserKeyProvider);
            var exchange = new HuobiProExchange(driver);

            var result = driver.TradesAsync(new[]
                {
                    new PairDto
                    {
                        Currency1 = new CurrencyDto { BrifName = "btc" },
                        Currency2 = new CurrencyDto { BrifName = "usdt" }
                    },
                    new PairDto
                    {
                        Currency1 = new CurrencyDto { BrifName = "etc" },
                        Currency2 = new CurrencyDto { BrifName = "usdt" }
                    },

                });

            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Result.Count);
        }

        [TestMethod]
        public void TestOrders()
        {
            var exchangeUserKeyProvider = A.Fake<IExchangeUserKeyProvider>();
            var driver = new HuobiProExchangeDriver(exchangeUserKeyProvider);
            var exchange = new HuobiProExchange(driver);

            var result = driver.OrdersAsync(new PairDto
            {
                Currency1 = new CurrencyDto { BrifName = "btc" },
                Currency2 = new CurrencyDto { BrifName = "usdt" }
            },
            150).Result as List<OrderDto>;

            Assert.IsNotNull(result);
            Assert.AreEqual(150*2, result.Count);
        }

        [TestMethod]
        public void TestTicker()
        {
            var exchangeUserKeyProvider = A.Fake<IExchangeUserKeyProvider>();
            var driver = new HuobiProExchangeDriver(exchangeUserKeyProvider);
            var exchange = new HuobiProExchange(driver);

            var result = driver.TickerAsync().Result as List<PairInfoOnExchangeDto>;

            Assert.IsNotNull(result);
            Assert.AreNotEqual(0, result.Count);
        }

        [TestMethod]
        public void TestCurrencies()
        {
            var exchangeUserKeyProvider = A.Fake<IExchangeUserKeyProvider>();
            var driver = new HuobiProExchangeDriver(exchangeUserKeyProvider);
            var exchange = new HuobiProExchange(driver);

            var result = driver.CurrenciesAsync().Result as List<CurrencyDto>;

            Assert.IsNotNull(result);
            Assert.AreEqual(252, result.Count);
        }

        [TestMethod]
        public void TestPairSettings()
        {
            var exchangeUserKeyProvider = A.Fake<IExchangeUserKeyProvider>();
            var driver = new HuobiProExchangeDriver(exchangeUserKeyProvider);
            var exchange = new HuobiProExchange(driver);

            var result = driver.PairSettingsAsync().Result as List<PairSettings>;

            Assert.IsNotNull(result);
            Assert.AreNotEqual(0, result.Count);
        }

        [TestMethod]
        public void TestUserInfo()
        {
            var exchangeUserKeyProvider = A.Fake<IExchangeUserKeyProvider>();
            var driver = new HuobiProExchangeDriver(exchangeUserKeyProvider);
            var exchange = new HuobiProExchange(driver);

            A.CallTo(() => exchangeUserKeyProvider.GetKeyAsync(exchange.Id)).Returns(
                Task.FromResult(
                    new ExchangeUserKeyPair //AccountId = "122170228"
                    {
                        ApiOpenKey = "afwo04df3f-bc495821-0b2111f6-3e6c8",
                        ApiSecretKey = "c051c7a5-8b0c6343-a6389a2b-d86ac",
                        ExchangeUserId = exchange.Id
                    }
                ));

            var result = driver.UserInfoAsync();

            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Result.Balances);
        }

        [TestMethod]
        public void TestOpenOrders()
        {
            var exchangeUserKeyProvider = A.Fake<IExchangeUserKeyProvider>();
            var driver = new HuobiProExchangeDriver(exchangeUserKeyProvider);
            var exchange = new HuobiProExchange(driver);

            A.CallTo(() => exchangeUserKeyProvider.GetKeyAsync(exchange.Id)).Returns(
                Task.FromResult(
                    new ExchangeUserKeyPair //AccountId = "122170228"
                    {
                        ApiOpenKey = "afwo04df3f-bc495821-0b2111f6-3e6c8",
                        ApiSecretKey = "c051c7a5-8b0c6343-a6389a2b-d86ac",
                        ExchangeUserId = exchange.Id
                    }
                ));

            var result = driver.OpenOrdersAsync().Result as List<OrderDto>;

            Assert.IsNotNull(result);
            Assert.AreNotEqual(0, result.Count);
        }

        [TestMethod]
        public void TestOrderCreate()
        {
            var exchangeUserKeyProvider = A.Fake<IExchangeUserKeyProvider>();
            var driver = new HuobiProExchangeDriver(exchangeUserKeyProvider);
            var exchange = new HuobiProExchange(driver);

            A.CallTo(() => exchangeUserKeyProvider.GetKeyAsync(exchange.Id)).Returns(
                Task.FromResult(
                    new ExchangeUserKeyPair //AccountId = "122170228"
                    {
                        ApiOpenKey = "afwo04df3f-bc495821-0b2111f6-3e6c8",
                        ApiSecretKey = "c051c7a5-8b0c6343-a6389a2b-d86ac",
                        ExchangeUserId = exchange.Id
                    }
                ));

            var order = new OrderDto
            {
                Date = DateTime.Now,
                ExchangeId = exchange.Id,
                Id = new Guid(),
                OrderStatus = OrderStatuses.New,
                Price = 1000,
                Quantity = 1
            };
            var t = driver.OrderCreateAsync(order, OrderCreateTypes.MarketBuy);

            var result = driver.OpenOrdersAsync().Result as List<OrderDto>;

            var equal = new OrderDto();
            if (result != null)
                equal = result.Find(x => x.Equals(order));

            Assert.IsNotNull(equal);
        }
    }
}
