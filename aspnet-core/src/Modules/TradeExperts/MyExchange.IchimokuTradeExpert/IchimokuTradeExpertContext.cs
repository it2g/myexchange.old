﻿using System;
using System.Collections.Generic;
using MyExchange.Api.Data;
using MyExchange.Api.TradeExpert;

namespace MyExchange.IchimokuTradeExpert
{
    /// <summary>
    /// Контекст данных, используемый при работе эксперта Ишимоку
    /// </summary>
    public class IchimokuTradeExpertContext : TradeExpertContextBase
    {
        /// <summary>
        /// Параметр для расчета красной линии
        /// </summary>
        public uint ParamForRedLine { get; set; } = 9;

        public uint ParamForBlueLine { get; set; } = 26;

        public uint ParamForStopLoss { get; set; } = 70;

        /// <summary>
        /// Временной интервал свечей в размерности которых работает конкретный экземпляр эксперта
        /// </summary>
        public TimeSpan TimeFrame { get; set; }

        /// <summary>
        /// Зарегистрироавнные сигналы для прогона модульных тестов
        /// TODO после тестирования можно убрать
        /// </summary>
        public IList<Signal> Signals { get; set; }

    }
}
