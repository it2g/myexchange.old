﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using JetBrains.Annotations;
using MyExchange.Api.Constants;
using MyExchange.Api.Data;
using MyExchange.Api.Dto.Trade;
using MyExchange.Api.Services.Crud;
using MyExchange.Base.TradeExpert;

namespace MyExchange.IchimokuTradeExpert
{
    /// <summary>
    /// Эксперт на основе стратегии торговли Ишимоку
    /// </summary>
    public class IchimokuTradeExpert : TradeIndicatorBase<IchimokuTradeExpertContext>
    {
        private ITradeCrudAppService _tradeCrudService;

        public override Guid Id { get; protected set; } = new Guid("0C922D55-321D-4D6B-A3F9-B385581B5845");

        public IchimokuTradeExpert([NotNull] IchimokuTradeExpertContext context,
            [NotNull] ITradeCrudAppService tradeCrudAppService) : base(context)
        {
            _tradeCrudService = tradeCrudAppService ?? throw new ArgumentNullException(nameof(tradeCrudAppService));
            Context.Signals = new List<Signal>();
        }

        public override Action ExecuteIndicator => Monitoring;

        private  async void Monitoring()
        {
            //Значения точек красной и синей линий на предыдущем таймфрейме
            decimal redPointValuePrevose = 0;
            decimal bluePointValuePrevose = 0;
            decimal stopLossValue = 0;
            //Коэффициент пунктов для рассчета Take profit
            uint pointСoefficient = 3;

            Signal signal = null;

            do
            {
                var filter = new TradePagedResultRequestDto
                {
                    ExchangeId = Context.ExchangeId,
                    PairId = Context.PairId
                };

                var timeNow = DateTime.UtcNow;

                //расчет координаты красной линии
                filter.LastRequestTime = timeNow - Context.ParamForRedLine * Context.TimeFrame;

                var tradePrices = (await _tradeCrudService.GetAll(filter)).Items.Select(x => x.Price).ToList();

                var redPointValue = (tradePrices.Max() - tradePrices.Min()) / 2;

                //расчет значения точки для синей линии
                filter.LastRequestTime = timeNow - Context.ParamForBlueLine * Context.TimeFrame;

                tradePrices = (await _tradeCrudService.GetAll(filter)).Items.Select(x => x.Price).ToList();

                var bluePointValue = (tradePrices.Max() - tradePrices.Min()) / 2;

                //проверка на наличие пересечения
                if (redPointValue < bluePointValue && redPointValuePrevose > bluePointValuePrevose)
                {
                    //мертый крест - сигнал на продажу

                    //если пересечение обнаружено, то вычисляем стоплосс
                    filter.LastRequestTime = timeNow - Context.ParamForStopLoss * Context.TimeFrame;
                    stopLossValue = (await _tradeCrudService.GetAll(filter)).Items.Select(x => x.Price).Max();
                    var price = (await _tradeCrudService.GetAll(filter)).Items.Select(x => x.Price).Last();
                    //формируем сигнал, котрый будет записан в качестве события в шину
                    signal = new Signal
                    {
                        Price = price,
                        Stoploss = stopLossValue,
                        TakeProfit = price - stopLossValue * pointСoefficient
                    };
                }else if (redPointValue > bluePointValue && redPointValuePrevose < bluePointValuePrevose)
                {
                    //золотой крест - сигнал на покупку

                    //если пересечение обнаружено, то вычисляем стоплосс
                    filter.LastRequestTime = timeNow - Context.ParamForStopLoss * Context.TimeFrame;
                    stopLossValue = (await _tradeCrudService.GetAll(filter)).Items.Select(x => x.Price).Min();
                    var price = (await _tradeCrudService.GetAll(filter)).Items.Select(x => x.Price).Last();

                   //формируем сигнал, котрый будет записан в качестве события в шину
                    signal = new Signal
                    {
                        Price = price,
                        Stoploss = stopLossValue,
                        TakeProfit = price + stopLossValue * pointСoefficient
                    };
                }

                if (signal != null)
                {
                    //записываем сигнал в шину данных
                    //...
                    //А пока в целях тестирования складываем сигналы в контекст эксперта
                    if (Context.Signals.Count > 1000)
                    {
                        Context.Signals = new List<Signal>();
                    }
                    Context.Signals.Add(signal);
                        //TODO Сигналы должны записываться в БД для последующего анализа
                        //и формирования кармы эксперта

                    signal = null;
                }
                
                //Ведется контроль за состоянием бота и в случае необходимости
                //прекращает его работу
                //TODO Перед проверкой состояния следует обратиться в БД и проверить на изменилось ли 
                //состояние бота. 
                if (Context.Status == BotStatuses.Stoped || Context.Status == BotStatuses.OnPause)
                {
                    break;
                }

                Thread.Sleep(Context.TimeFrame);

            } while (true);
        }




        
    }
}
