﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using MyExchange.Api.Constants;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Crud;
using MyExchange.Api.TradeExpert;
using MyExchange.Base.TradeExpert;

namespace MyExchange.JapanCandleTradeExpert
{
    /// <summary>
    /// Эксперт на основе шаблона Японских свечей
    /// </summary>
    public class JapanCandleTradeExpert : TradeAdviserBase<TradeExpertContextBase>
    {
        private ITradeCrudAppService _tradeCrudService;


        public override Guid Id { get; protected set; } = new Guid("0C922D55-321D-4D6B-A3F9-B385581B7D2F");

        public JapanCandleTradeExpert([NotNull] TradeExpertContextBase context,
            [NotNull] ITradeCrudAppService tradeCrudAppService) : base(context)
        {
         
            _tradeCrudService = tradeCrudAppService ?? throw new ArgumentNullException(nameof(tradeCrudAppService));
            TradingTypeList.Add(Api.Constants.TradingTypes.MidleTime);
            TradingTypeList.Add(Api.Constants.TradingTypes.LongTime);
        }

        public override IList<TradingTypes> TradingTypeList { get; }

        public override async Task<TradeExpertAdvice> GetAdviceAsync()
        {
            //Context.BotTacticConext.Bot.
            //Получить последние значения курса валютной пары из БД
            //TODO Добавить критери по бирже
            var expression = (await _tradeCrudService.GetAll(null)).Items.ToList()
                .Where(x => x.PairId == Context.PairId);
            var tradeSell = expression.OrderBy(x => x.Date).Last(x => x.TradeTypes == TradeTypes.Sell);
            var tradeBay = expression.OrderBy(x => x.Date).Last(x => x.TradeTypes == TradeTypes.Bay);


            return new TradeExpertAdvice() {Advice = Advices.Growin, Possibility = 60.6};
        }


    }
}
