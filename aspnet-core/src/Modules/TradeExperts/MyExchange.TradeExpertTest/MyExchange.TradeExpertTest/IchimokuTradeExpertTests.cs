using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using FakeItEasy;
using JetBrains.Annotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyExchange.Api.Data;
using MyExchange.Api.Dto.Trade;
using MyExchange.Api.Services.Crud;
using MyExchange.IchimokuTradeExpert;

namespace MyExchange.TradeExpertTest
{
    [TestClass]
    public class IchimokuTradeExpertTests
    {
        
        [TestMethod]
        public void IchimokuGetSignals()
        {
            var context = new IchimokuTradeExpertContext
            {
               TimeFrame = TimeSpan.FromMinutes(1)
            };
            var _tradeCrudAppService = A.Fake<ITradeCrudAppService>();
            A.CallTo(() => _tradeCrudAppService.GetAll(A<TradePagedResultRequestDto>.Ignored))
                .Returns(
                    Task.FromResult(
                        new PagedResultDto<TradeDto>()
                        {
                            Items = GenerateFakeTrades(),
                            TotalCount = 20
                        }
                    )
                );

            var ichimokuTradeExpert = new FakeIchimokuTradeExpert(context, _tradeCrudAppService);


        }


        class FakeIchimokuTradeExpert : IchimokuTradeExpert.IchimokuTradeExpert
        {
            public FakeIchimokuTradeExpert([NotNull] IchimokuTradeExpertContext context, [NotNull] ITradeCrudAppService tradeCrudAppService) : base(context, tradeCrudAppService)
            {
            }
            /// <summary>
            /// ���� ����� ������ ��� ������ �������� �����
            /// ����� ������ �� ����������� �������� ������������  �������
            /// </summary>
            /// <returns></returns>
            public IList<Signal> GetSignals()
            {
                return Context.Signals;
            }
        }

        /// <summary>
        /// ������� �������� ������, ��������� �� � ���� json 
        /// � ��������� ���� � ��������� �� � �������� �������� ������.
        /// </summary>
        /// <returns></returns>
        private List<TradeDto> GenerateFakeTrades()
        {
            return new List<TradeDto>();
        }
    }
}
