﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services;
using Abp.Dependency;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Entity;

namespace MyExchange.TradeMonitoringTactic
{
    /// <summary>
    /// Создает тактику и передает ей контекст данных
    /// </summary>
    public interface ITradeMonitoringTacticBuilder : IApplicationService
    {
        TradeMonitoringTactic Create(TradeMonitoringTacticContext context);
    }

    public class TradeMonitoringTacticBuilder : ApplicationService, ITradeMonitoringTacticBuilder
    {
        public TradeMonitoringTactic Create(TradeMonitoringTacticContext context)
        {
            var botTactic = IocManager.Instance.Resolve<TradeMonitoringTactic>();

            botTactic.Context = context;

            return botTactic;
        }
    }
}
