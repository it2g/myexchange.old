﻿using System;
using Abp.Application.Services;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using JetBrains.Annotations;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Entity;
using MyExchange.Api.MyExchange;
using MyExchange.Api.Services.Bl;
using MyExchange.Api.Services.Crud;
using MyExchange.Base.BotTactic;
using MyExchange.Base.Services;
using MyExchange.TradeMonitoringTactic;
using MyExchange.TradeMonitoringTactic.Dal.Repositories;
using MyExchange.Web.ExchangeStratagy;

namespace MyExchange.Services.Bl
{
    public interface IMonitoringTacticManager : IBotManager<TradeMonitoringTactic.TradeMonitoringTactic, TradeMonitoringTacticContext>
    {
    }

    /// <summary>
    /// Менеджер, котрый управляет ботами мониторинга
    /// </summary>
    public class MonitoringTacticManager : BotManagerBase<TradeMonitoringTactic.TradeMonitoringTactic, TradeMonitoringTacticContext>
        , IMonitoringTacticManager
    {
        public MonitoringTacticManager( IBotTacticProvider botTacticProvider,
            IExchangeProvider exchangeProvider,
            IBackgroundJobManager backgroundJobManager,
            ITradeMonitoringTacticContextRepository contextRepository) 
                : base(botTacticProvider, exchangeProvider, backgroundJobManager, contextRepository)
        {
        }
    }
}
