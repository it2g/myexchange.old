﻿using System;

namespace MyExchange.TradeMonitoringTactic.Dto
{
    public class CreateTradeMonitoringTacticContextDto 
    {
        public Guid PairId { get; set; }

        public Guid ExchangeId { get; set; }

        ///Задержка между запросами к бирже 
        public int Pause { get; set; } = 3000;

        /// Временное приращение/подрезание паузы если появляется много
        /// повтороно загружаемых сделок, чтобы сократить количество избыточно-лишних запросов
        public int PauseDelta { get; set; } = 100;
    }
}
