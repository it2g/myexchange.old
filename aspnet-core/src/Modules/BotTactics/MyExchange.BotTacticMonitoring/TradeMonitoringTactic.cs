﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Abp.AutoMapper;
using JetBrains.Annotations;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Constants;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto.Pair;
using MyExchange.Api.Dto.Trade;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Bl;
using MyExchange.Api.Services.Crud;
using MyExchange.Base.BotTactic;

namespace MyExchange.TradeMonitoringTactic
{
    public interface ITradeMonitoringTactic : IBotTactic<TradeMonitoringTacticContext>
    {
    }

    /// <summary>
    /// Тактика мониторинга курсов валютных пар
    /// </summary>
    public class TradeMonitoringTactic : BotTacticBase<TradeMonitoringTacticContext>, ITradeMonitoringTactic
    {
        private readonly ITradeCrudAppService _tradeCrudService;
        private readonly IExchangeProvider _exchangeProvider;
        private readonly IPairRepository _pairRepository;

        public TradeMonitoringTactic(
            [NotNull] ITradeCrudAppService tradeCrudService,
            [NotNull] IExchangeProvider exchangeProvider,
            [NotNull] IPairRepository pairRepository)
        {
            _tradeCrudService = tradeCrudService ?? throw new ArgumentNullException(nameof(tradeCrudService));
            _exchangeProvider = exchangeProvider ?? throw new ArgumentNullException(nameof(exchangeProvider));
            _pairRepository = pairRepository ?? throw new ArgumentNullException(nameof(pairRepository));
        }
        public override Guid Id { get; } = new Guid("3cf4d36e-2de3-49eb-94a0-8053eea385b9");

        public override string Name { get; } = "Мониторинг сделок по валютной паре";

        public override Action ExecuteTactic => Monitoring;
        public override void Dispose() {}

        /// <summary>
        /// Собирает информацию о курсах валютных пар и сохраняет ее в БД для последующей аналитики
        /// </summary>
        //[UnitOfWork(isTransactional: false)]
        private void Monitoring()
        {
            //Сделки загруженные на предыдущем запросе
            var prevouseTrades = new List<TradeDto>();

            var exchange = _exchangeProvider.Get(Context.ExchangeId);

            var pair = _pairRepository.FirstOrDefault(Context.PairId);

            ///В цикле с установленной задержкой делать запросы на биржу по совершаемым
            /// сделкам отслеживаемой валютной пары.
            do
            {
                var trades = exchange.ExchangeDriver.TradesAsync(pair.MapTo<PairDto>()).Result.ToList();

                ///Оставляем только новые сделки, т.к. в последнем запросе естественно
                /// будут сделки, котрые были уже получены в предпоследнем запросе
                var lastNewTrades = GetOnlyNewTrades(trades, prevouseTrades);

                var tradesDto =  _tradeCrudService.AddList(lastNewTrades.Select(e=> ObjectMapper.Map<TradeDto>(e)).ToList()).Result;
                //Нужен для того, чтобы добавленные в контекст сделки закомитились в БД
                //CurrentUnitOfWork.SaveChanges();

                //Ведется контроль за выставленным состоянием бота и в случае необходимости
                //прекращает мониторинг
                //TODO Перед проверкой состояния следует обратиться в БД и проверить на изменилось ли 
                //состояние бота. 
                if (Context.Status == BotStatuses.Stoped || Context.Status == BotStatuses.OnPause)
                {
                    break;
                }
                var countNewTrades = lastNewTrades.Count;
                
                //Изменение задержки перед последующей загрузкой на основе анализа
                //повторно скаченных данных
                ChangePause(countNewTrades, trades.Count);

#if DEBUG
                Debug.WriteLine($"Количество новых данных: {countNewTrades} Задержка: {Context.Pause} милисек");
#endif

                //сохраняем последние сделки, чтобы сравнить с ними загружаемые сделки на 
                //следующем шаге
                prevouseTrades = trades;

                Thread.Sleep(Context.Pause);

            } while (true);
 
        }

        /// <summary>
        /// Вносит коррективы в величину паузы между двумя загрузками данных
        /// в зависимости от того, насколько быстро меняются данные.
        /// Настоящая реализация возможно несколько грубо отражает возможную динамику
        /// Этот  алгоритм можно уточнять и делать более диференцированый и более динамичный
        /// </summary>
        /// <param name="countNewTrades"></param>
        /// <param name="countTrades"></param>
        private void ChangePause(int countNewTrades, int countTrades)
        {
            if(countTrades==0) return;

            if (countNewTrades / countTrades < 0.4)
            {
                //Увеличиваем задержку в 2 раза
                Context.Pause *= 2;
            }
            else if (countNewTrades / countTrades < 0.6)
            {
                //Увеличиваем задержку на дельту
                Context.Pause += Context.PauseDelta;
            }
            else if (countNewTrades / countTrades > 0.8)//80% всех загруженных сделок - это новые сделки
            {
                //Уменьшаем задержку на дельту
                Context.Pause -= Context.PauseDelta;
            }
            else if (countNewTrades / countTrades > 0.9)//90% всех загруженных сделок - это новые сделки
            {
                //Уменьшаем задержку в 2 раза
                Context.Pause /= 2;
            }
        }

        /// <summary>
        /// Выбирает и возвращает только новые сделки по отношению к выборке 
        /// на предыдущей итеррации. Нет необходимости коммитить в БД одни и те же сделки
        /// </summary>
        /// <param name="trades"></param>
        /// <param name="prevouseTrades"></param>
        /// <returns></returns>
        private IList<TradeDto> GetOnlyNewTrades(IEnumerable<TradeDto> trades, IEnumerable<TradeDto> prevouseTrades)
        {
            var newTrades = trades.Except(prevouseTrades, new TradeComparer()).ToList();

            return newTrades;
        }
       
        /// <summary>
        /// Сравниватель двух сделок, загруженных из биржи
        /// </summary>
        class TradeComparer : IEqualityComparer<TradeDto>
        {
            public bool Equals(TradeDto x, TradeDto y)
            {
                return x.TradeId == y.TradeId;
            }

            public int GetHashCode(TradeDto obj)
            {
                return obj.TradeId.GetHashCode();
            }
        }

    }
}
