﻿using System;
using MyExchange.Api.Entity;

namespace MyExchange.TradeMonitoringTactic
{
    public class TradeMonitoringTacticContext : BotTacticContextBase
    {
        /// <summary>
        /// С этой пары нужно начинать торги,
        /// </summary>
        public Guid PairId { get; set; }

        public Guid ExchangeId { get; set; }

        ///Задержка между запросами к бирже 
        public int Pause { get; set; } = 3000;

        /// Временное приращение/подрезание паузы если появляется много
        /// повтороно загружаемых сделок, чтобы сократить количество избыточно-лишних запросов
        public int PauseDelta { get; set; }  = 100;
    }
}
