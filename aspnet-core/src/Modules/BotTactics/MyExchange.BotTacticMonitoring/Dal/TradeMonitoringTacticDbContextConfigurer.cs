using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyExchange.TradeMonitoringTactic.Dal
{
    public static class TradeMonitoringTacticDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<TradeMonitoringTacticDbContext> builder, string connectionString)
        {
            builder.UseNpgsql(connectionString,
                //https://stackoverflow.com/questions/38507861/entity-framework-core-change-schema-of-efmigrationshistory-table
                x => x.MigrationsHistoryTable(HistoryRepository.DefaultTableName, "bot_tactic"))
                .UseLazyLoadingProxies();
        }

        public static void Configure(DbContextOptionsBuilder<TradeMonitoringTacticDbContext> builder, DbConnection connection)
        {
            builder.UseNpgsql(connection, 
                //https://stackoverflow.com/questions/38507861/entity-framework-core-change-schema-of-efmigrationshistory-table
                x => x.MigrationsHistoryTable(HistoryRepository.DefaultTableName, "bot_tactic"))
                   .UseLazyLoadingProxies();
        }
    }
}
