﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using MyExchange.Configuration;
using MyExchange.Web;

namespace MyExchange.TradeMonitoringTactic.Dal
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class TradeMonitoringTacticDbContextFactory : IDesignTimeDbContextFactory<TradeMonitoringTacticDbContext>
    {
        public TradeMonitoringTacticDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<TradeMonitoringTacticDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            TradeMonitoringTacticDbContextConfigurer.Configure(builder, configuration.GetConnectionString(MyExchangeConsts.ConnectionStringName));

            return new TradeMonitoringTacticDbContext(builder.Options);
        }
    }
}
