﻿using System;
using Abp.EntityFrameworkCore;
using Abp.EntityFrameworkCore.Repositories;

namespace MyExchange.TradeMonitoringTactic.Dal.Repositories
{
    public class TradeMonitoringTacticContextRepository :
        EfCoreRepositoryBase<TradeMonitoringTacticDbContext, TradeMonitoringTacticContext, Guid>, 
        ITradeMonitoringTacticContextRepository
    {
        public TradeMonitoringTacticContextRepository(IDbContextProvider<TradeMonitoringTacticDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public override TradeMonitoringTacticContext Insert(TradeMonitoringTacticContext entity)
        {
            entity = base.Insert(entity);

            return entity;
        }
    }
}
