﻿using System;
using Abp.Domain.Repositories;

namespace MyExchange.TradeMonitoringTactic.Dal.Repositories
{
    public interface ITradeMonitoringTacticContextRepository : IRepository<TradeMonitoringTacticContext, Guid>
    {

    }
}
