﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.Dal.EfConfigurations;

namespace MyExchange.TradeMonitoringTactic.Dal.EfConfigurations
{
    public class TradeMonitoringTacticContextMap : BotTacticsContextBaseMap<TradeMonitoringTacticContext>
    {
        public override void Configure(EntityTypeBuilder<TradeMonitoringTacticContext> builder)
        {
            base.Configure(builder);

            builder.Property(u => u.PairId).IsRequired().HasColumnName("pair_id");
            builder.Property(u => u.Pause).IsRequired().HasColumnName("pause");
            builder.Property(u => u.PauseDelta).IsRequired().HasColumnName("pause_delta");
            builder.Property(x => x.ExchangeId).IsRequired().HasColumnName("exchange_id");

            builder.ToTable("trade_monitoring_bot_context", "monitoring");
        }
    }
}
