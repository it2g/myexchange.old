﻿using Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MyExchange.Api.Entity;
using MyExchange.Api.MyExchange;
using MyExchange.TradeMonitoringTactic.Dal.EfConfigurations;

namespace MyExchange.TradeMonitoringTactic.Dal
{
    public class TradeMonitoringTacticDbContext : AbpDbContext
    {
        
        public virtual DbSet<TradeMonitoringTacticContext> MonitoringBotTacticContexts { get; set; }

        public TradeMonitoringTacticDbContext(DbContextOptions<TradeMonitoringTacticDbContext> options)
            : base(options)
        {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<ApplicationLanguageText>()
            //    .Property(p => p.Value)
            //    .HasMaxLength(10485759); // any integer that is smaller than 10485760
            base.OnModelCreating(modelBuilder);
            //https://www.npgsql.org/efcore/value-generation.html - для автогенерации Id типа Guid
            modelBuilder.HasPostgresExtension("uuid-ossp");
            
            modelBuilder.ApplyConfiguration(new TradeMonitoringTacticContextMap());

            modelBuilder.Ignore<Pair>();
            modelBuilder.Ignore<Currency>();
            modelBuilder.Ignore<Exchange>();
        }
    }
}
