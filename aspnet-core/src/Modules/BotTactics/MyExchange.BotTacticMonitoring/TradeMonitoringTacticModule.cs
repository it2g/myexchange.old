﻿using Abp.AutoMapper;
using Abp.EntityFrameworkCore.Configuration;
using Abp.Modules;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using MyExchange.Api.BotTactic;
using MyExchange.Base;
using MyExchange.TradeMonitoringTactic.Dal;


namespace MyExchange.TradeMonitoringTactic
{
    [DependsOn(typeof(BaseModule))]
    public class TradeMonitoringTacticModule : AbpModule
    {
        public bool SkipDbContextRegistration { get; set; }

        /// <summary>
        /// Предварительное подключение конвенций регистрации для контейнера,
        /// для автоматической регистрации контекстов данных, конфигураций и 
        /// компонентов системы (репозиториев и сервисов).
        /// </summary>

        public override void PreInitialize()
        {
            //IocManager.AddConventionalRegistrar(new ConfigurationConventionalRegistrar());
            //IocManager.AddConventionalRegistrar(new ComponentConventionalRegistrar());
            if (!SkipDbContextRegistration)
            {
                Configuration.Modules.AbpEfCore().AddDbContext<TradeMonitoringTacticDbContext>(options =>
                {
                    if (options.ExistingConnection != null)
                    {
                        TradeMonitoringTacticDbContextConfigurer.Configure(options.DbContextOptions, options.ExistingConnection);
                    }
                    else
                    {
                        TradeMonitoringTacticDbContextConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
                    }
                });
            }
            //Настройка конфигурации Automapper
            Configuration.Modules.AbpAutoMapper().Configurators.Add(config =>
                config.AddProfiles(typeof(TradeMonitoringTacticModule).Assembly));

            // включение поддержки внедрения коллекции зависимостей.
            // Эта настройка позволяет контейнеру внедрять зависимости в свойства вида T[], IList<T>, IEnumerable<T>, ICollection<T>,
            // пытаясь подставить в них коллекцию со всеми зайденными реализациями типа T. 
            // Если ни одной зависимости типа T нет, контейнер внедрит пустую коллекцию
            IocManager.IocContainer.Kernel.Resolver.AddSubResolver(new CollectionResolver(IocManager.IocContainer.Kernel, true));
        }

        /// <summary>
        /// Регистрация компонентов текущей сборки и сборки модели справочников в контейнере
        /// </summary>
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TradeMonitoringTacticModule).Assembly);
            IocManager.IocContainer.Register(
                Component.For(typeof(IBotTactic))
                    //.ImplementedBy(typeof(TradeMonitoringTacticContextCrudAppService))
                    .Named("3cf4d36e-2de3-49eb-94a0-8053eea385b9"));
        }

        /// <summary>
        /// Выполнение настроечной логики после завершения сборки контейнера
        /// </summary>
        public override void PostInitialize()
        {
        }


    }
}
