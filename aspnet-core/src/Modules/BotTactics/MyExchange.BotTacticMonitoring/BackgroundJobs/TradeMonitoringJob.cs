﻿using System;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using JetBrains.Annotations;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Dal;
using MyExchange.Api.Services.Bl;
using MyExchange.Api.Services.Crud;
using MyExchange.Services.Bl;
using MyExchange.TradeMonitoringTactic.Dal.Repositories;


namespace MyExchange.TradeMonitoringTactic.BackgroundJobs
{
    /// <summary>
    /// Постановка фоновой задачи на мониторинг сделок по указанной валютной паре на бирже
    /// </summary>
    public class TradeMonitoringJob : BackgroundJob<(Guid exchangeId, Guid pairId)>, ITransientDependency
    {
        private readonly ITradeMonitoringTacticContextRepository  _monitoringTacticContextRepository;
        private readonly ITradeMonitoringTacticBuilder _monitoringTacticBuilder;
        private readonly IBotTacticBuilder _botTacticBuilder;
        private readonly IMonitoringTacticManager _monitoringTacticManager;

        public TradeMonitoringJob(
            [NotNull] ITradeMonitoringTacticContextRepository monitoringTacticContextRepository,
            [NotNull] ITradeMonitoringTacticBuilder monitoringTacticBuilder,
            [NotNull] IBotTacticBuilder botTacticBuilder,
            [NotNull] IMonitoringTacticManager monitoringTacticManager)
        {
            _monitoringTacticContextRepository = monitoringTacticContextRepository ?? throw new ArgumentNullException(nameof(monitoringTacticContextRepository));
            _monitoringTacticBuilder = monitoringTacticBuilder ?? throw new ArgumentNullException(nameof(monitoringTacticBuilder));
            _botTacticBuilder = botTacticBuilder ?? throw new ArgumentNullException(nameof(botTacticBuilder));
            _monitoringTacticManager = monitoringTacticManager ?? throw new ArgumentNullException(nameof(monitoringTacticManager));
        }

        public override void Execute(( Guid exchangeId , Guid pairId ) value)
        {
            //Проверить есть ли контекст в БД. Т.е. возможно уже ранее
            //это работа была запущена и завершилась аварийно
            TradeMonitoringTacticContext tradeTacticContext = _monitoringTacticContextRepository
                .FirstOrDefault(x=> x.ExchangeId == value.exchangeId && x.PairId == value.pairId);
            
            if (tradeTacticContext == null)
            {
                tradeTacticContext = new TradeMonitoringTacticContext
                {
                    Pause = 10000,
                    PauseDelta = 200,
                    PairId = value.pairId,
                    ExchangeId = value.exchangeId
                };
            }

            var botTactic = _monitoringTacticBuilder.Create(tradeTacticContext);

            _monitoringTacticManager.StartBot(botTactic);
            //botTactic.ExecuteTactic();

            //TODO Попробовать универсальный бьюлдер
            //var botTactic2 = _botTacticBuilder.Create(typeof(TradeMonitoringTactic), tradeTacticContext);

            //botTactic2.ExecuteTactic();
        }
    }
}
