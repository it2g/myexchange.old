﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyExchange.TradeMonitoringTactic.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "monitoring");

            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:PostgresExtension:uuid-ossp", ",,");

            migrationBuilder.CreateTable(
                name: "trade_monitoring_bot_context",
                schema: "monitoring",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    creation_time = table.Column<DateTime>(nullable: false),
                    bot_tactic_id = table.Column<Guid>(nullable: false),
                    status_id = table.Column<int>(nullable: false),
                    start_time = table.Column<DateTime>(nullable: true),
                    stop_time = table.Column<DateTime>(nullable: true),
                    pair_id = table.Column<Guid>(nullable: false),
                    exchange_id = table.Column<Guid>(nullable: false),
                    pause = table.Column<int>(nullable: false),
                    pause_delta = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_trade_monitoring_bot_context", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "trade_monitoring_bot_context",
                schema: "monitoring");
        }
    }
}
