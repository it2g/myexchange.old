﻿using MyExchange.TradeMonitoringTactic.Dto;

namespace MyExchange.TradeMonitoringTactic
{
    public class TradeMonitoringTacticContextAutoMapperProfile : AutoMapper.Profile
    {
        public TradeMonitoringTacticContextAutoMapperProfile()
        {
            CreateMap<TradeMonitoringTacticContextDto, TradeMonitoringTacticContext>().ReverseMap();
            CreateMap<CreateTradeMonitoringTacticContextDto, TradeMonitoringTacticContextDto>();
            CreateMap<UpdateTradeMonitoringTacticContextDto, TradeMonitoringTacticContextDto>();


        }
    }
}
