﻿using MyExchange.Api.Entity;
using MyExchange.Api.MyExchange;

namespace MyExchange.SimpleTradingBotTactic
{

    public class SimpleTradingBotTacticContext : TradingBotTacticContextBase
    {
        /// <summary>
        /// Стратегия, к которой прикреплен бот
        /// </summary>
        public ITradingStrategy TradingStrategy { get; set; }
    }
}
