﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using JetBrains.Annotations;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Constants;
using MyExchange.Api.Entity;
using MyExchange.Api.MyExchange;
using MyExchange.Api.Services.Crud;
using MyExchange.Api.TradeExpert;
using MyExchange.Base.BotTactic;

namespace MyExchange.SimpleTradingBotTactic
{
    /// <summary>
    /// Пример тактики, использующей специфичные для конкретной биржи функциональные
    /// возможности.
    /// </summary>
    public class SimpleTradingBotTactic : TradeBotTacticBase<SimpleTradingBotTacticContext>, ITradingBotTactic
    {
        private readonly IExpertProvider _expertProvider;

        private IOpenedOrderCrudAppService _openedOrderCrudService;

        public SimpleTradingBotTactic(IOpenedOrderCrudAppService openedOrderCrudService,
            [NotNull] IAccountant accountant,
            [NotNull] SimpleTradingBotTacticContext context,
            [NotNull] IExpertProvider expertProvider) : base(accountant)
        {
            _expertProvider = expertProvider ?? throw new ArgumentNullException(nameof(expertProvider));
            _openedOrderCrudService = openedOrderCrudService ?? throw new ArgumentNullException(nameof(openedOrderCrudService));
        }

        public override Guid Id { get; } = new Guid("4e1a238b-cd10-4b1a-a563-9f1d5b36a4e2");

        public override string Name => "Тактика торговли с простейшим алгоритмом";

        public override Action ExecuteTactic => AnyTacticMethod;

        /// <summary>
        /// Корректирует экстримальные значения в контексте тактики в процессе работы бота
        /// </summary>
        /// <param name="upperLevel"></param>
        /// <param name="lowerLevel"></param>
        public void CorrectExtpemeValues(decimal upperLevel, decimal lowerLevel)
        {}

        private async void AnyTacticMethod()
        {
            do
            {
                var parametrs = new Dictionary<string, object>();
                parametrs.Add("TradingType", TradingTypes.MidleTime);
                parametrs.Add("ValueMoney", Context.CurrencyValue);

                var experts = _expertProvider.GetExperts(parametrs);

                IList<TradeExpertAdvice> advices = new List<TradeExpertAdvice>();

                foreach (var expert in experts)
                {
                    advices.Add(await expert.GetAdviceAsync());
                }

                IDictionary<Advices, int> groupedAdvices = new Dictionary<Advices, int>();

                groupedAdvices.Add(Advices.Growin, advices.Count(a => a.Advice == Advices.Growin));
                groupedAdvices.Add(Advices.Falls, advices.Count(a => a.Advice == Advices.Falls));
                groupedAdvices.Add(Advices.Stable, advices.Count(a => a.Advice == Advices.Stable));

                var bestAdvice = groupedAdvices.OrderByDescending(p=> p.Value).First();

                if (bestAdvice.Key == Advices.Stable)
                {
                    return;
                }
                else if (bestAdvice.Key == Advices.Falls)
                {
                    //Что делаем здесь?
                }
                else if (bestAdvice.Key == Advices.Growin)
                {
                    //Что делаем здесь?
                }

                //Ведется контроль за статусом бота и в случае необходимости
                //прекращает работу
                if (Context.Status == BotStatuses.Stoped || Context.Status == BotStatuses.OnPause)
                {
                    break;
                }

                Thread.Sleep(1000);

            } while (true);
        }

        public override  void ChangeCurrencyValue(Currency currency, decimal value, string purpose)
        {
            throw new NotImplementedException();
        }

        public override string SetOrder(Pair pair, decimal value, decimal rate, TradeTypes tradeTypes)
        {
            throw new NotImplementedException();
        }

        public override bool CancelOrder(Guid orderId)
        {
            throw new NotImplementedException();
        }

        public override void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
