﻿using MyExchange.Api.Dto.Pair;
using MyExchange.Api.Entity;

namespace MyExchange.BotTacticInfo
{
    public class InfoBotTacticContext : BotTacticContextBase
    {
        /// <summary>
        /// С этой пары нужно начинать торги,
        /// </summary>
        public PairDto Pair { get; set; }

        public decimal UpperLevel {get; set; }

        public decimal LowerLevel {get; set; }

        public InfoBotTacticContext(decimal upperLevel, decimal lowerLevel)
        {
            UpperLevel = upperLevel;
            LowerLevel = lowerLevel;
        }
    }
}
