﻿using System;
using System.Linq;
using System.Threading;
using JetBrains.Annotations;
using MyExchange.Api.Constants;
using MyExchange.Api.Services.Crud;
using MyExchange.Base.BotTactic;

namespace MyExchange.BotTacticInfo
{
    /// <summary>
    /// Тактика, нацеленная на информаирование трейдера при достижении курса валютной
    /// пары до какого либо установленного для отслеживания значения.
    /// Трейдер устанавливает два значения при достижении которых уведомляется трейдер.
    /// В процессе работы бота с этой тактикой, трейдер должен иметь возможность
    /// корректировки критических значений
    /// </summary>
    public class InfoBotTactic : BotTacticBase<InfoBotTacticContext> //, IBotTactic<TContext> временно отключили, тобы не путаться с тактикой мониторинга
    {
        private readonly ITradeCrudAppService _tradeCrudService;

        public InfoBotTactic([NotNull] ITradeCrudAppService tradeCrudService)
        {
            _tradeCrudService = tradeCrudService ?? throw new ArgumentNullException(nameof(tradeCrudService));
        }

        public override Guid Id { get; } = new Guid("4e1a238b-cd10-4b1a-a563-9f1d5b36f3c1");

        public override string Name { get { return "Тактика для информирования трейдера"; } }

        public override Action ExecuteTactic => Informator;

        public override void Dispose() {}

        /// <summary>
        /// Корректирует экстримальные значения в контексте тактики в процессе работы бота
        /// </summary>
        /// <param name="upperLevel"></param>
        /// <param name="lowerLevel"></param>
        public void CorrectExtpemeValues(decimal upperLevel, decimal lowerLevel)
        {
            Context.UpperLevel = upperLevel;
            Context.LowerLevel = lowerLevel;
        }

        private void Informator()
        {
            //Проверить есть ли свежие данные в БД по интересующей валютной паре
            //Если нет, то нужно запустить бот мониторинга

            do
            {
                //Получить последние значения курса валютной пары из БД
                //TODO Добавить критери по бирже
                var expression = _tradeCrudService.GetAll(null).Result.Items
                    .Where(x => x.PairId == Context.Pair.Id);

                var tradeSell = expression.OrderBy(x=>x.Date).Last(x => x.TradeTypes == Api.Constants.TradeTypes.Sell);
                var tradeBay = expression.OrderBy(x => x.Date).Last(x => x.TradeTypes == Api.Constants.TradeTypes.Bay);

                //Сравнивать их с верним и нижним значением
                if (tradeSell.Price < Context.LowerLevel)
                {
                    //Отправляем уведомление о выгодной покупке
                }
                else if (tradeBay.Price > Context.UpperLevel)
                {
                    //Отправляем уведомление о выгодной продаже
                }

                //Ведется контроль за выставленным состоянием бота и в случае необходимости
                //прекращает работу
                if (Context.Status == BotStatuses.Stoped || Context.Status == BotStatuses.OnPause)
                {
                    break;
                }

                Thread.Sleep(1000);

            } while (true);
        }
    }
}
