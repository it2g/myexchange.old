﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using MyExchange.Api.Dto;
using MyExchange.Api.Dto.Currency;
using MyExchange.Services;
using MyExchange.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Dependency;
using JetBrains.Annotations;
using MyExchange.Api.MyExchange;
using MyExchange.BotTacticMonitoring.BackgroundJobs;
using MyExchange.Controllers;
using MyExchange.TradeMonitoringTactic.BackgroundJobs;

namespace MyExchange.Web
{
    [Route("api/[controller]")]
    public class BotTacticController : MyExchangeControllerBase
    {
        private IBackgroundJobManager _backgroundJobManager;

        public BotTacticController([NotNull] IBackgroundJobManager backgroundJobManager)
        {
            _backgroundJobManager = backgroundJobManager ?? throw new ArgumentNullException(nameof(backgroundJobManager));
        }

        /// <summary>
        /// Мониторинг сделок по определенной валютной паре
        /// на указанной бирже
        /// TODO Создан для тестирования. Убрать после настройки запуска фоновых задач при
        /// запуске системы.
        /// </summary>
        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> RunTradesMonitoring([FromBody] TradeMonitoringInput value )
        {
            await _backgroundJobManager.EnqueueAsync<TradeMonitoringJob, (Guid ,Guid) >((value.ExchangeId, value.PairId));

            return Ok();
        }

        public class TradeMonitoringInput
        {
            public  Guid ExchangeId { get; set; }

            public Guid PairId { get; set; }
        }

    }
}
