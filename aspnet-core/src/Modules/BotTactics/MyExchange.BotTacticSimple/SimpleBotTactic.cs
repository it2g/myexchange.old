﻿using System;
using JetBrains.Annotations;
using MyExchange.Api.BotTactic;
using MyExchange.Api.MyExchange;
using MyExchange.Api.Query;
using MyExchange.Web.BotTactic;

namespace MyExchange.BotTacticSimple
{
    public class SimpleBotTactic : TradeBotTacticBase<TradingTacticsContextBase>
    {
        private IQueryManager _queryManager;

        public SimpleBotTactic([NotNull] IAccountant accountant, [NotNull] TradingTacticsContextBase context,
            [NotNull] IQueryManager queryManager) : base(accountant, context)
        {
            _queryManager = queryManager ?? throw new ArgumentNullException(nameof(queryManager));
        }

        public override string Name { get; } = "Простейшая тактика торговли";

        public override Action ExecuteTactic => Implementation;

        private  void Implementation()
        {
            ///Пример постановки в очередь запроса на биржу
            _queryManager.Enqueue(() => ExchangeDriver.UserInfo());
        }


       
    }
}
