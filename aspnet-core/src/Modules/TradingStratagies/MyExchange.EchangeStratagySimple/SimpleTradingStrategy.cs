﻿using System;
using JetBrains.Annotations;
using MyExchange.Api.Services.Bl;
using MyExchange.Web.ExchangeStratagy;

namespace MyExchange.EchangeStratagySimple
{
    /// <summary>
    /// Простейшая стратегия автоматизированной торговли на биржах, применяемая системой
    /// </summary>
    public class SimpleTradingStrategy : TradingStrategyBase
    {
        //public SimpleTradingStrategy([NotNull] IBotManager botManager) : base(botManager)
        //{}

        public override Action ExecuteStratagy { get; protected set; }  = Implementation;
        
        private static void Implementation()
        {
            ///Логика управления ботами для достижения цели стратегии
            /// ...
            
        }
    }
}
