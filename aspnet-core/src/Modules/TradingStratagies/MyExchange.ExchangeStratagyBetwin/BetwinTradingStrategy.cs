﻿using System;
using JetBrains.Annotations;
using MyExchange.Api.Services.Bl;
using MyExchange.Web.ExchangeStratagy;

namespace MyExchange.ExchangeStratagyBetwin
{
    /// <summary>
    /// Стратегия анализа одинаковых валютных пар на разных биржах, чтобы можно было выполнять
    /// межберживые торговые операции
    /// https://www.youtube.com/watch?v=ai_9U1BCbp0&t=909s
    /// </summary>
    internal class BetwinTradingStrategy : TradingStrategyBase
    {
        //public BetwinTradingStrategy([NotNull] IBotManager botManager) : base(botManager)
        //{}

        public override Action ExecuteStratagy { get; protected set; }  = Implementation;
        
        private static void Implementation()
        {
            ///Логика управления ботами для достижения цели стратегии
            /// ...
            
        }
    }
}
