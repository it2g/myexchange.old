﻿using System;
using Abp.Hangfire.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using JetBrains.Annotations;
using MyExchange.Configuration;
using MyExchange.BotTacticMonitoring;
using MyExchange.BotTacticInfo;
using MyExchange.ExchangeExmo;
using MyExchange.EchangeStratagySimple;
using MyExchange.ExchangeHuobiPro;
using MyExchange.ExchangeStratagyBetwin;
using MyExchange.JapanCandleTradeExpert;
using MyExchange.SimpleTradingBotTactic;
using MyExchange.TradeMonitoringTactic;

namespace MyExchange.Web.Host.Startup
{
    [DependsOn(
        typeof(MyExchaneWebModule),
        typeof(MyExchaneBotTacticWebModule),
        typeof(MyExchangeWebCoreModule),
        typeof(MyExchangeMainModule),
        typeof(BotTacticInfoModule),
        typeof(TradeMonitoringTacticModule),
        typeof(SimpleTradingBotTacticModule),
        typeof(ExmoModule),
        typeof(HuobiProModule),
        typeof(SimpleTradingStratagyModule),
        typeof(BetwinTradingStratagyModule),
        typeof(JapanCandleTradeExpertModule))]
    public class MyExchangeWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;
        //private readonly IBackgroundJobsRunner _backgroundJobsRunner;

        public MyExchangeWebHostModule(IHostingEnvironment env
            //,[NotNull] IBackgroundJobsRunner backgroundJobsRunner
            )
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
            //_backgroundJobsRunner = backgroundJobsRunner ?? throw new ArgumentNullException(nameof(backgroundJobsRunner));

        }

        public override void PreInitialize()
        {
            Configuration.BackgroundJobs.UseHangfire();
            //Configuration.Settings.Providers.Add<ApplicationSettingProvider>();
#if DEBUG
            ///Временно отключена валидация параметров методов всех контролеров
            //Configuration.Modules.AbpAspNetCore().IsValidationEnabledForControllers = false;
#endif
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MyExchangeWebHostModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            //Запускаются фоновые задачи

            IocManager.Resolve<IBackgroundJobsRunner>().RunBackgroundJobs().Wait();
        }
    }
}
