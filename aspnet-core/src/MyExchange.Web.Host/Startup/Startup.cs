﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Castle.Facilities.Logging;
using Swashbuckle.AspNetCore.Swagger;
using Abp.AspNetCore;
using Abp.Castle.Logging.Log4Net;
using Abp.Extensions;
using MyExchange.Configuration;
using MyExchange.Identity;
using Abp.AspNetCore.SignalR.Hubs;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Hangfire;
using Hangfire.PostgreSql;
using MyExchange.Web.Host.BackgroundJobs;


namespace MyExchange.Web.Host.Startup
{
    public class Startup
    {
        private const string _defaultCorsPolicyName = "localhostPolicy";
        private const string _testingCorsPolicyName = "testingPolicy";

        private readonly IConfigurationRoot _appConfiguration;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Console.WriteLine($"Выбрана конфигурация {env.EnvironmentName}");

            _appConfiguration = builder.Build();
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // MVC
            services.AddMvc(
                options => options.Filters.Add(new CorsAuthorizationFilterFactory(_defaultCorsPolicyName))
            );

            IdentityRegistrar.Register(services);
            AuthConfigurer.Configure(services, _appConfiguration);

            services.AddSignalR();

            // Configure CORS for angular2 UI
            services.AddCors(options =>
            {
                options.AddPolicy(
                    _testingCorsPolicyName,
                    builder => builder
                        .AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                    );

                options.AddPolicy(
                    _defaultCorsPolicyName,
                    builder => builder
                        .WithOrigins(
                            // App:CorsOrigins in appsettings.json can contain more than one address separated by comma.
                            _appConfiguration["App:CorsOrigins"]
                                .Split(",", StringSplitOptions.RemoveEmptyEntries)
                                .Select(o => o.RemovePostFix("/"))
                                .ToArray()
                        )
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials()
                    );
            });



            // Swagger - Enable this line and the related lines in Configure method to enable swagger UI
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info { Title = "MyExchange API", Version = "v1" });
                options.DocInclusionPredicate((docName, description) => true);

                // Define the BearerAuth scheme that's in use
                options.AddSecurityDefinition("bearerAuth", new ApiKeyScheme()
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
            });

            services.AddHangfire(config =>
            {
                config.UsePostgreSqlStorage(_appConfiguration.GetConnectionString("Default"));
            });

            // Configure Abp and Dependency Injection
            return services.AddAbp<MyExchangeWebHostModule>(
                // Configure Log4Net logging
                options => options.IocManager.IocContainer.AddFacility<LoggingFacility>(
                    f => f.UseAbpLog4Net().WithConfig("log4net.config")
                )
            );
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseAbp(options => { options.UseAbpRequestLocalization = false; }); // Initializes ABP framework.

            app.UseCors(_testingCorsPolicyName); // Enable CORS!

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseAbpRequestLocalization();


            app.UseSignalR(routes =>
            {
                routes.MapHub<AbpCommonHub>("/signalr");
            });

            app.UseHangfireServer(new BackgroundJobServerOptions()
            {
                WorkerCount = 2,
                Queues = new[] { "default", "expert_queue", "monitoring_queue", "tradeTactic_queue" }
            });

            var defaultRetryFilter = GlobalJobFilters.Filters
                .FirstOrDefault(f => f.Instance is AutomaticRetryAttribute);

            if (defaultRetryFilter != null && defaultRetryFilter.Instance != null)
            {
                GlobalJobFilters.Filters.Remove(defaultRetryFilter.Instance);
            }
            GlobalJobFilters.Filters.Add(new HangfireUseCorrectQueueFilter{Order =1});

            app.UseHangfireDashboard("/hangfire");

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "defaultWithArea",
                    template: "{area}/{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

           
            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();
            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint(_appConfiguration["App:ServerRootAddress"].EnsureEndsWith('/') + "swagger/v1/swagger.json", "MyExchange API V1");
                options.IndexStream = () => Assembly.GetExecutingAssembly()
                    .GetManifestResourceStream("MyExchange.Web.Host.wwwroot.swagger.ui.index.html");
            }); // URL: /swagger

            //Публикует тестовые данные в БД 
            var ioCManager = app.ApplicationServices.GetService<IIocManager>();
            var bjm = ioCManager.Resolve<IBackgroundJobManager>();
           
        }
    }
}
