﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hangfire.Common;
using Hangfire.States;

namespace MyExchange.Web.Host.BackgroundJobs
{
    /// <summary>
    /// Фильтр для задач Hangfire которые восстанавливаются после сбоя
    /// и почему-то не возвращаются в прежнюю очередь
    /// https://habr.com/ru/post/434364/
    /// </summary>
    public class HangfireUseCorrectQueueFilter
        : JobFilterAttribute, IElectStateFilter
    {
        public void OnStateElection(ElectStateContext context)
        {
            if (context.CandidateState is EnqueuedState enqueuedState)
            {
                var queueName = context.GetJobParameter<string>("QueueName");

                if (string.IsNullOrWhiteSpace(queueName))
                {
                    context.SetJobParameter("QueueName", enqueuedState.Queue);
                }
                else
                {
                    enqueuedState.Queue = queueName;
                }
            }
        }
    }
}
