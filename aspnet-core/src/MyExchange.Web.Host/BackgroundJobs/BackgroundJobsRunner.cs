﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.BackgroundJobs;
using Hangfire;
using JetBrains.Annotations;
using MyExchange.Api.Dto.Pair;
using MyExchange.Api.Dto.PairInfoOnExchange;
using MyExchange.Api.Services.Bl;
using MyExchange.Api.Services.Crud;
using MyExchange.BackgroundJobs;
using MyExchange.BotTacticMonitoring.BackgroundJobs;
using MyExchange.TradeMonitoringTactic.BackgroundJobs;

namespace MyExchange.Web.Host.Startup
{
    /// <summary>
    /// Запускаются все фоновые процессы, котрые должны запускаться при старте системы
    /// </summary>
    public class BackgroundJobsRunner : ApplicationService, IBackgroundJobsRunner
    {
        private readonly IExchangeProvider _exchangeProvider;
        private readonly IPairInfoOnExchangeCrudAppService _pairInfoOnExchangeCrudAppService;
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly IBackgroundJobClient _backgroundJobClient;


        public BackgroundJobsRunner([NotNull] IExchangeProvider exchangeProvider,
            [NotNull] IPairInfoOnExchangeCrudAppService pairInfoOnExchangeCrudAppService,
            [NotNull] IBackgroundJobManager backgroundJobManager,
            [NotNull] IBackgroundJobClient backgroundJobClient)
        {
            _exchangeProvider = exchangeProvider ?? throw new ArgumentNullException(nameof(exchangeProvider));
            _pairInfoOnExchangeCrudAppService = pairInfoOnExchangeCrudAppService ?? throw new ArgumentNullException(nameof(pairInfoOnExchangeCrudAppService));
            _backgroundJobManager = backgroundJobManager ?? throw new ArgumentNullException(nameof(backgroundJobManager));
            _backgroundJobClient = backgroundJobClient ?? throw new ArgumentNullException(nameof(backgroundJobClient));
        }

        public async Task RunBackgroundJobs()
        {
            await StartMonitoringJobs();
            //await StartExperts();
        }

        private async Task StartMonitoringJobs()
        {
            var exchanges = _exchangeProvider.GetAll();

            foreach (var exchange in exchanges)
            {
                //TODO переделать на регламентный запуск задачи по расписанию
                _backgroundJobManager.Enqueue<ReadCurrenciesJob, Guid>(exchange.Id);
                _backgroundJobManager.Enqueue<ReadPairsJob, Guid>(exchange.Id);

                var pairInfoOnExchangeList =  _pairInfoOnExchangeCrudAppService.GetAll(
                    new PairInfoOnExchangePagedResultRequestDto { ExchangeId = exchange.Id }).Result;

                foreach (var pairId in pairInfoOnExchangeList.Items.Select(x => x.PairId))
                {//запускаем задачи для каждого объекта наблюдения: Trade, Order, Tiker

                    _backgroundJobManager
                         .Enqueue<TradeMonitoringJob, (Guid, Guid)>((exchange.Id, pairId));
                    //...
                }
            }

        }

        private async Task StartExperts()
        {

        }

    }


}
