﻿using System;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Hangfire;
using JetBrains.Annotations;
using MyExchange.Api.Dal;
using MyExchange.Api.Services.Bl;
using MyExchange.Api.Services.Crud;
using MyExchange.Web.ExchangeStratagy;
using MyExchange.Web.Host.Startup;

namespace MyExchange.BotTacticMonitoring.BackgroundJobs
{
    /// <summary>
    /// Запуск метода, котрый запускает фоновые процессы при старте системы
    /// </summary>
    public class StartBackgroundTasksJob : BackgroundJob<int>, ITransientDependency
    {
        private IBackgroundJobsRunner _backgroundJobsRunner;
 
        public StartBackgroundTasksJob([NotNull] IBackgroundJobsRunner backgroundJobsRunner)
        {
            _backgroundJobsRunner = backgroundJobsRunner ?? throw new ArgumentNullException(nameof(backgroundJobsRunner));
        }

        public override void Execute(int dummy)
        {
            _backgroundJobsRunner.RunBackgroundJobs();
        }
    }
}
