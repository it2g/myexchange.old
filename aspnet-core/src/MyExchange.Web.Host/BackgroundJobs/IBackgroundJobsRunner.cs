﻿using System.Threading.Tasks;
using Abp.Application.Services;

namespace MyExchange.Web.Host.Startup
{
    public interface IBackgroundJobsRunner : IApplicationService
    {
        Task RunBackgroundJobs();
    }
}